Bind testcase:

nxt-bad.c is the source with the unfixed version of the bug. If you search the
file for PUTSHORT or PUTLONG, you will find the regions that have been 
modified to take input from the user.

To compile from source, type "make" while in this directory.
This will create files "testcase" and "testcase.bc" in this directory.
They should be copied to the same directory as the main executable (src).

