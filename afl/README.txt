This is an excerpt of code from American Fuzzy Lop, which is used to insert 
instrumentation into executables on compilation.

Unlike AFL, which combines random compile-time constants to find transitions
but allows for collisions in the identifiers, this has been changed to insert
unique identifiers at each block. Instead of a two-dimensional hash map to 
store visit counts of transitions, it interprets available shared memory as
a single 1-D vector, with the UID being treated as an index into the vector.

Changes from the AFL source code are noted at the top of relevant files.

To build:
    1) From this directory, type "make afl-showmap".
    2) Navigate to the llvm_mode directory and run "make".

Note that underneath, this compiler uses clang. 
(Flags provided should be compatible with clang.)


