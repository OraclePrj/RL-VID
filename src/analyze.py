import sys
from Visualizer.LogFile import LogFile
from Analyzer.RunAnalyzer import RunAnalyzer
from Analyzer.TimeFile import TimeFile

def get_cli_args():
    """ Returns parameter_id and run_id if provided.
        Returns None, None if no args provided.
        Raises exception on invalid args.
    """
    args = sys.argv
    if len(args) == 1:
        # The 1 arg is the launched program name.
        return None, None
    elif len(args) == 3:
        #2 args provided.
        parameter_id, run_id = int(sys.argv[1]), int(sys.argv[2])
        return parameter_id, run_id
    else:
        raise Exception("Wrong number of arguments provided")

class RunSummarizer:
    def __init__(self, parameter_id=None, run_id=None):
        log_fmt, time_fmt = "log{}.log", "out{}.log"
        run_fmt = "param{}.data"
        parameter_id = "" if parameter_id is None else str(parameter_id)        

        # Calculate filenames for LogFile and TimeFile from id.
        log = LogFile(log_fmt.format(parameter_id))
        time = TimeFile(time_fmt.format(parameter_id))
 
        # Create RunAnalyzer, feeding it the loaded files.
        analyzer = RunAnalyzer()
        analyzer.analyze(log, time)

        # Save RunAnalyzer data to file.
        output_file = run_fmt.format(parameter_id)
        analyzer.save(output_file)

if __name__ == "__main__":
    parameter_id, run_id = get_cli_args()
    RunSummarizer(parameter_id, run_id)

