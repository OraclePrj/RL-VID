#Name of output file
OUTPUT = main

#Set compiler and flags
COMPILE = g++
CFLAGS = -g -std=c++11 -Wall $(shell llvm-config-6.0 --cxxflags) -fexceptions #-pg #for profiling

#Set up linking flags
LLVMPREFLAGS := $(shell llvm-config-6.0 --cxxflags --ldflags)
LLVMPOSTFLAGS := $(shell llvm-config-6.0 --libs all --system-libs)
LLVMFLAGS = $(LLVMPREFLAGS) $(LLVMPOSTFLAGS) -fexceptions
LFLAGS = $(LLVMFLAGS) -lboost_program_options -lboost_unit_test_framework

#Filesystem information 
DOWSER = AnalysisGroup DowserScores DataFlowGraph
QBASE = ActionSpace ActionSampler ProbabilityDistribution QBASE IntSpace
ROOT = CLIArgs Config Logger MCTS Policy ProgressBar Rewards
SIMULATOR = InputGenerator Random RangeMap Pipe Process Simulator TargetProgram
STATEGRAPH = Block Function Instruction Operand SharedMemory Representation
TREE = MCT_ActionNode MCT_Node MCT_StateNode MCT_Tree Path
UTILS = StrOps

#How to modify filenames to fit in filesystem structure
DOWSERPATHS = $(DOWSER:%=Dowser/%.cpp)
GRAPHPATHS = $(STATEGRAPH:%=StateGraph/%.cpp)
QBASEPATHS = $(QBASE:%=QBASE/%.cpp)
ROOTPATHS = $(ROOT:%=%.cpp)
SIMPATHS = $(SIMULATOR:%=Simulator/%.cpp)
TREEPATHS = $(TREE:%=Tree/%.cpp)
UTILPATHS = $(UTILS:%=Utils/%.cpp)

#All source files, objects, and header files.
SRC_FILES = $(DOWSERPATHS) $(GRAPHPATHS) $(ROOTPATHS) $(SIMPATHS) \
            $(QBASEPATHS) $(TREEPATHS) $(UTILPATHS)
OBJECTS = $(SRC_FILES:.cpp=.o) #Change all .cpp endings to .o
HEADERS = $(SRC_FILES:.cpp=.hpp) #Change all .cpp endings to .hpp

all: $(OBJECTS)
	$(COMPILE) Main.cpp -o $(OUTPUT) $(OBJECTS) $(CFLAGS) $(LFLAGS)
args: $(OBJECTS)
	$(COMPILE) CLIArgsTest.cpp -o $(OUTPUT) $(OBJECTS) $(CFLAGS) $(LFLAGS)
clean:
	./clean_all.sh
gdb: $(OBJECTS)
	$(COMPILE) GDB_ProgramTest.cpp -o $(OUTPUT) $(OBJECTS) $(CFLAGS)
simple: simple.c
	clang -g -Wall -O2 simple.c -o simple #.bc -S -emit-llvm
guard: guard.c
	clang -g -Wall -O2 guard.c -o guard #.bc -S -emit-llvm
tree: $(OBJECTS)
	$(COMPILE) Tree/MCT_Tree_Test.cpp -o $(OUTPUT) $(OBJECTS) $(CFLAGS)
process: $(OBJECTS)
	$(COMPILE) ProcessTest.cpp -o $(OUTPUT) $(OBJECTS) $(CFLAGS)
rewards: $(OBJECTS)
	$(COMPILE) RewardsTest.cpp -o $(OUTPUT) $(OBJECTS) $(CFLAGS) $(LFLAGS)

# COMPILING: SOURCE -> OBJECTS
# $@: LHS of :, i.e., the target.
# $<: Name of 1st item on RHS of :, i.e., 1st dependency.
%.o: %.cpp %.hpp
	$(COMPILE) $< -o $@ -c $(CFLAGS)
