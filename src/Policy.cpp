#include "Policy.hpp"

Policy::Policy(Config& config) : qbase(config.getActionSpace()) {
    mode = config.getPolicyMode();
    space = config.getActionSpace();
}

QBASE& Policy::getQBASE() {
    return qbase;
}

IntSpace& Policy::getSpace() {
    return space;
}

int Policy::greedy(MCT_StateNode* state) {
    //Greedy: Choose best action 
    return state->getBestAction();
}

void Policy::handle(CLIArgs& args) {
    if (mode == QBASE_MODE && args.exist()) {
        qbase.setBatchSize(args.getBatchSize());
        qbase.setBestSize(args.getBestSize());
        qbase.setUpdatePeriod(args.getUpdatePeriod());
    }
}

int Policy::rollout(int state) {
    Granularity granularity = space.getGranularity();
    if (granularity == DISCRETE) {
        return random.integer(space.getLower(), space.getUpper());
    } else { // if (granularity == CONTINUOUS) {
        //Only two actions regardless of action space.
        return random.integer(0, 1);
    }
}

int Policy::tree(shared_ptr<MCT_Node> node, int actionCount) {
    return tree(dynamic_cast<MCT_StateNode*>(&*node), actionCount);
}

int Policy::tree(MCT_Node* node, int actionCount) {
    return tree(dynamic_cast<MCT_StateNode*>(node), actionCount);
}

int Policy::tree(MCT_StateNode* state, int actionCount) {
    if (mode == UCB_MODE) {
        if (state == nullptr) return -1;
        return ucb(state, actionCount);
    } else { //if (mode == QBASE_MODE) {
        return qbase.sample(state);
    }
}

int Policy::ucb(MCT_StateNode* state, int actionCount) {
    if (actionCount == 1) return 0;
    int bestAction = -1;
    double bestValue = -numeric_limits<double>::infinity();
    double numerator = log(state->getVisits());

    /*
        Guidance to choose c:
        UCB1 = Q_this + c * sqrt(lg(n_all)/n_this)

        Remember, sqrt(x) has following property:
        If 0 < x < 1, makes it bigger.
        If x == 1, stays the same.
        If x > 1, makes it smaller.

        In this case: 
        0 < x < 1: action visited < lg(n) times: makes term bigger.
        x == 1: action visited lg(n) times: no effect.
        x > 1: action visited > lg(n) times: makes term smaller.

        So, exploration term seeks to each action being selected >= lg(n) times.
        So at lg(n) we want term to represent some average value.

        Suggestion: use c = -Q_worst.
        At n_this = lg(n_all), UCB1 = Q_this + c.
        If c = - Q_worst, UCB1 = 0 at worst.
    */
    double c = 10000;

    unsigned childCount = state->getChildren().size();
    for (auto& entry : state->getChildren()) {
        MCT_Node* current = entry.second;
        if (childCount == 1) {
            //Choose unexplored action.
            bestAction = 1 - current->getId();
        } else {
            double exploration = sqrt(numerator/current->getVisits());
            double thisValue = current->value() + c * exploration;
            if (thisValue > bestValue) {
                bestValue = thisValue;
                bestAction = entry.first;
            }
        }
    }
    if (bestAction == -1) {
        bestAction = random.integer(0, 1);
    }

    return bestAction;
}
