#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE QBASE
#include <boost/test/unit_test.hpp>

#include <vector>
#include "CLIArgs.hpp"

char* getArg(string& fromString) {
    unsigned newLength = fromString.size() + 1;
    char* result = new char[newLength];
    strcpy(result, fromString.c_str());
    result[newLength - 1] = '\0';
    return result;
}

int getArgC(vector<string>& arglist) {
    return arglist.size();
}

char** getArgV(vector<string>& arglist) {
    unsigned argc = arglist.size();
    char** result = new char*[argc];
    for (unsigned index = 0; index < argc; index++) {
        result[index] = getArg(arglist[index]);
    }
    return result;
}

BOOST_AUTO_TEST_CASE( CLIArgs_00 ) {
    cout << "Testing no arguments" << endl;
    vector<string> arglist = {"test"};
    int argc = getArgC(arglist);
    char** argv = getArgV(arglist);

    CLIArgs args(argc, argv);
    BOOST_CHECK_EQUAL(args.exist(), false);
    delete[] argv;
}


BOOST_AUTO_TEST_CASE( CLIArgs_01 ) {
    cout << "Testing all arguments" << endl;
    vector<string> arglist = {"test", 
                              "--batch", "10",
                              "--best", "4", 
                              "--id", "42",
                              "--update", "13"};
    int argc = getArgC(arglist);
    char** argv = getArgV(arglist);

    CLIArgs args(argc, argv);
    BOOST_CHECK_EQUAL(args.exist(), true);

    BOOST_CHECK_EQUAL(args.getBatchSize(), 10);
    BOOST_CHECK_EQUAL(args.getBestSize(), 4);
    BOOST_CHECK_EQUAL(args.getParameterId(), 42);
    BOOST_CHECK_EQUAL(args.getUpdatePeriod(), 13);

    delete[] argv;
}

