#ifndef _MCTS_HPP_
    #define _MCTS_HPP_
    #include <fstream>
    #include <map>
    #include <memory>
    #include <vector>
    #include "CLIArgs.hpp"
    #include "Config.hpp"
    #include "Simulator/Simulator.hpp"
    #include "Tree/MCT_Node.hpp"
    #include "Tree/MCT_StateNode.hpp"
    #include "Tree/MCT_Tree.hpp"
    #include "Policy.hpp"
    
    using namespace std;

    class MCTS {
        public:
            MCTS(Config& config);
            Policy& getPolicy();
            Simulator& getSimulator();
            MCT_Tree& getTree();
            void handle(CLIArgs& args);
            void reset();
            void step();
            void write_to_log();
            void update();
        private:
            MCT_Tree tree;
            Config config;
            Policy policy;
            Simulator sim;
            vector<MCT_Node*> path;
            
            MCT_StateNode* select(int firstState);
            MCT_StateNode* expand(MCT_StateNode* node);
            void backpropagate(double value, MCT_StateNode* node);
    };

#endif
