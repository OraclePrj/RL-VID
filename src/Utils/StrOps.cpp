#include "StrOps.hpp"

//TODO: Use a template here.
ostream& operator<<(ostream& os, const vector<double>& list) {
    unsigned length = list.size();
    os << '[';
    for (unsigned index = 0; index < length; index++) {
        if (index > 0) os << ", ";
        os << list[index];
    }
    os << ']';
    return os;
}

ostream& operator<<(ostream& os, const vector<int>& list) {
    unsigned length = list.size();
    os << '[';
    for (unsigned index = 0; index < length; index++) {
        if (index > 0) os << ", ";
        os << list[index];
    }
    os << ']';
    return os;
}

ostream& operator<<(ostream& os, const vector<string>& list) {
    unsigned length = list.size();
    os << '[';
    for (unsigned index = 0; index < length; index++) {
        if (index > 0) os << ", ";
        os << '"' << list[index] << '"';
    }
    os << ']';
    return os;
}

bool contains(const string& pattern, const string& target) {
    return target.find(pattern) != string::npos;
}

bool endsWith(const string& pattern, const string& target) {
    unsigned patternLength = pattern.length();
    unsigned targetLength = target.length();
    if (targetLength < patternLength) return false;
    int startIndex = targetLength - patternLength;
    for (unsigned index = startIndex; index < targetLength; index++) {
        if (pattern[index - startIndex] != target[index]) {
            return false;
        }
    }
    return true;
}

vector<string> split(const string& target, const string& delim) {
    vector<string> results;
    if (target.empty()) {
        results.push_back("");
        return results;
    }

    string copy = target;
    size_t index;
    while ((index = copy.find(delim)) != string::npos) {
        results.push_back(copy.substr(0, index));
        copy = copy.substr(index + delim.length());
    }
    if (copy.length() > 0) {
        results.push_back(copy);
    }
    return results;
}

bool startsWith(const string& pattern, const string& target) {
    unsigned patternLength = pattern.length();
    if (target.length() < patternLength) return false;
    for (unsigned index = 0; index < patternLength; index++) {
        if (pattern[index] != target[index]) {
            return false;
        }
    }
    return true;
}

void trim(vector<string>& strings) {
    for (string& token : strings) {
        token = trim(token);
    }
}

string trim(string& toTrim) {
    string whitespace = " \t";
    return trim(toTrim, whitespace);
}

string trim(const string& toTrim, const string& badChars) {
    //Should remove badChars from either end.
    size_t lengthToTrim = toTrim.length();
    //For remaining string, make fwd pass to find last bad char at front
    size_t start = 0;
    while (start < lengthToTrim) {
        char firstCharLeft = toTrim[start];
        if (badChars.find(firstCharLeft) != string::npos) {
            //Char is bad. Trim.
            start++;
        } else {
            //First non-bad. Stop.
            break;
        }
    }

    size_t end = lengthToTrim;
    while (start < end - 1) {
        char lastCharLeft = toTrim[end - 1];
        if (badChars.find(lastCharLeft) != string::npos) {
            //Char is bad. Trim.
            end--;
        } else {
            //First non-bad. Stop.
            break;
        }
    }

    return toTrim.substr(start, end - start);
}
