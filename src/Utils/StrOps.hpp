#ifndef _STROPS_HPP_
    #define _STROPS_HPP_
    #include <iostream>
    #include <string>
    #include <vector>

    using namespace std;

    bool contains(const string& pattern, const string& target);
    bool endsWith(const string& pattern, const string& target);
    bool startsWith(const string& pattern, const string& target);
    vector<string> split(const string& target, const string& delim);
    string trim(string& toTrim);
    string trim(const string& toTrim, const string& badChars);
    void trim(vector<string>& strings);

    ostream& operator<<(ostream& os, const vector<double>& list);
    ostream& operator<<(ostream& os, const vector<int>& list);
    ostream& operator<<(ostream& os, const vector<string>& list);
    
#endif
