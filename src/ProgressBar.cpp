#include "ProgressBar.hpp"

ProgressBar::ProgressBar(int max) {
    this->current = 0;
    this->max = max;
}

bool ProgressBar::done() const {
    return current >= max;
}

bool ProgressBar::next() {
    if (!done()) {
        current++;
        return true;
    } else {
        return false;
    }
}

void ProgressBar::show() {
    double ratio = ((double)current / max);
    int counters = ratio / 0.05;
    ostringstream output;
    output << "\rProgress: [";
    for (int count = 0; count < 20; count++) {
        if (count < counters) {
            output << '|';
        } else {
            output << ' ';
        }
    }
    output << "] (";
    int percent = 100 * ratio;
    output << percent << "%)";
    cout << output.str();
    flush(cout);
}

/*ostream& operator<<(ostream& os, const ProgressBar& bar) {
    return os << bar.toString();
}*/
