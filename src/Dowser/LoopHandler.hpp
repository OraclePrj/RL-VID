#ifndef _LOOPHANDLER_CPP_
    #define _LOOPHANDLER_CPP_
    #include <iostream>
    #include <vector>
    #include "llvm/ADT/SmallVector.h"
    #include "llvm/Analysis/LoopInfo.h"
    #include "llvm/IR/Dominators.h"
    #include "../StateGraph/Function.hpp"
    #include "Loop.hpp"

    using namespace std;
    using namespace PRGM;

    class LoopHandler {
        public:
            vector<Loop> getLoops(Function& function);
        private:
            //Nothing for now.
    };

#endif
