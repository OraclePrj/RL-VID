#ifndef _POSTDOMINATORTREE_HPP_
    #define _POSTDOMINATORTREE_HPP_
    #include <iostream>
    #include "llvm/Analysis/PostDominators.h"
    #include "../StateGraph/Block.hpp"
    #include "../StateGraph/Function.hpp"

    using namespace std;
    using namespace PRGM;

    class PostDominatorTree {
        public:
            void load(Function& function);
            bool postdom(const Block& postdominator, 
                         const Block& postdominatee) const;
        private:
            llvm::PostDominatorTree tree;    
    };

#endif
