#ifndef _ANALYSISGROUP_HPP_
    #define _ANALYSISGROUP_HPP_
    #include <set>
    #include <sstream>
    #include <string>
    #include <vector>
    #include "../StateGraph/Instruction.hpp"
    #include "../Utils/StrOps.hpp"

    using namespace PRGM; 
    using namespace std;

    class AnalysisGroup {
        public:
            AnalysisGroup();
            void add(unsigned bbid, Instruction& instr);
            void filterBBIDsTo(set<unsigned> blockIds);
            vector<unsigned> getBlockIds();
            vector<Instruction> getGetElemPtrs(unsigned bbid, bool local);
            vector<Instruction> getNonPointerNonInlineFnCalls(unsigned bbid);
            vector<Instruction> getOtherArithmetic(unsigned bbid);
            vector<Instruction> getPointerCasts(unsigned bbid);
            vector<Instruction> getSimpleArithmetic(unsigned bbid, bool local);
            vector<double> getUniqueConstantValues(unsigned bbid);
            vector<Instruction> getVariablesOutsideLoops(unsigned bbid);
            bool hasCycle();
            void setCycle(bool hasCycle);
            unsigned size() const;
            string toString() const;
        private:
            bool cycle;
            bool debug;
            set<Instruction> outside;
            map<unsigned, vector<Instruction>> blocks;
            vector<Instruction> getInstructions();
    };

    ostream& operator<<(ostream& output, const AnalysisGroup& group);
    ostream& operator<<(ostream& output, const vector<AnalysisGroup>& group);

#endif
