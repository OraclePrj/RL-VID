#include "DowserFeatures.hpp"

DowserFeatures::DowserFeatures() {
    debug = true;
}

AnalysisGroup DowserFeatures::getAnalysisGroup(Instruction& instruction) {
    if (debug) {
        cout << "Seed instruction: " << instruction << endl;
    }

    //Give inst to FunctionAnalyzer and ask for vector<Inst> dependencies.
    vector<Instruction> dependencies = analyzer.getDependencies(instruction);

    AnalysisGroup group;
    for (Instruction dependency : dependencies) {
        //Query FunctionAnalyzer for bbid of Instruction and add to group.
        if (!dependency.isAlloc()) {
            Block block = analyzer.getBlock(dependency);
            unsigned bbid = analyzer.getBBID(block);
            group.add(bbid, dependency);
        }
    }

    //Query FunctionAnalyzer to see if it contained cycles.
    if (analyzer.sawCycle()) {
        group.setCycle(true);
    }    

    return group;
}

vector<AnalysisGroup> DowserFeatures::getAnalysisGroups(Representation& repr) {
    unsigned blockOffset = 0;
    vector<AnalysisGroup> groups;
    for (auto currentFunction = repr.begin();
              currentFunction != repr.end(); ++currentFunction) {
        Function function(currentFunction);
        if (debug) {
            cout << "Visiting function: " << function.getName() << endl;
        }

        analyzer.initialize(function, blockOffset);

        unsigned loopCounter = 0;
        for (Loop& loop : analyzer.getLoops()) {
            if (debug) {
                cout << "Loop " << loopCounter++ << "; ";
            }

            analyzer.setLoop(loop);
            vector<AnalysisGroup> loopGroups = getGroups(loop);
            if (debug) {
                cout << "Groups found: " << loopGroups.size() << endl;
            }
            for (AnalysisGroup group : loopGroups) {
                groups.emplace_back(group);
            }
        }
    
        //Maintain starting basic block id of each function
        blockOffset += function.getBlockCount();
    }
    return groups;
}

vector<AnalysisGroup> DowserFeatures::getGroups(Loop& loop) {
    vector<AnalysisGroup> groups;
    set<Instruction> seen;
    queue<Instruction> groupSeeds;
    for (Instruction instruction : loop.getPointerAccesses()) {
        //Assumption: ptr accesses are all either loads or stores.
        unsigned index = instruction.isLoad() ? 0 : 1;
        Operand address = instruction.getOperands()[index];
        if (!address.isLocalVar()) {
            //Only add accesses where address can be changed
            groupSeeds.emplace(instruction);
        }
    }
    while (groupSeeds.size() > 0) {
        Instruction instruction = groupSeeds.front();
        groupSeeds.pop();
        seen.insert(instruction);

        //Get AG for this instruction and save it.
        AnalysisGroup group = getAnalysisGroup(instruction);
        group.filterBBIDsTo(getLoopBBIDs(loop));
        groups.push_back(group);

        //Dedupe BBIDs in the group and convert to Blocks
        set<Block> blocks;
        for (unsigned bbid : group.getBlockIds()) {
            Block block = analyzer.getBlock(bbid);
            blocks.insert(block);
        }

        //Find guards for each block in group (if any) and put in queue.
        for (Block block : blocks) {
            for (Instruction guard : analyzer.getGuards(block, loop)) {
                if (seen.count(guard) == 0) {
                    seen.insert(guard);

                    if (debug) cout << "Guard: " << guard << endl;

                    groupSeeds.emplace(guard);
                }
            }
        }
    }
    return groups;
}

set<unsigned> DowserFeatures::getLoopBBIDs(Loop& loop) {
    set<unsigned> result;
    for (Block block : loop.getBlocks()) {
        result.insert(analyzer.getBBID(block));
    }
    return result;
}


/*
vector<AnalysisGroup> DowserFeatures::getAnalysisGroups(Representation& repr) {
    unsigned blockOffset = 0;
    vector<AnalysisGroup> result;
    for (auto currentFunction = repr.begin();
              currentFunction != repr.end(); ++currentFunction) {
        Function function(currentFunction);
        if (debug) {
            cout << "Visiting function: " << function.getName();
        }
        //For each function, create dependency graph
        DataFlowGraph data;
        data.load(function, blockOffset);

        //Extract analysis groups from graph
        unsigned groupCounter = 0;
        vector<AnalysisGroup> groups = data.getAnalysisGroups();
        if (debug) {
            cout << ", # of AGs: " << groups.size() << endl;
        }
        for (AnalysisGroup& group : groups) {
            if (debug) {
                cout << endl << "AG #" << groupCounter++ << ": ";
            }

            //Save each group
            result.push_back(group);
        }
    
        //Maintain starting basic block id of each function
        blockOffset += function.getBlockCount();
    }
    return result;
}
*/
