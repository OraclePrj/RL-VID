#ifndef _DOWSERFEATURES_HPP_
    #define _DOWSERFEATURES_HPP_
    #include <iostream>    
    #include <queue>
    #include <set>
    #include <vector>
    #include "AnalysisGroup.hpp"
    #include "DataFlowGraph2.hpp"
    #include "FunctionAnalyzer.hpp"
    #include "Loop.hpp"
    #include "../StateGraph/Function.hpp"
    #include "../StateGraph/Instruction.hpp"
    #include "../StateGraph/Representation.hpp"

    using namespace std;
    using namespace PRGM;

    class DowserFeatures {
        public:
            DowserFeatures();
            vector<AnalysisGroup> getAnalysisGroups(Representation& repr);
        private:
            bool debug;
            FunctionAnalyzer analyzer;
            AnalysisGroup getAnalysisGroup(Instruction& instruction);
            vector<AnalysisGroup> getGroups(Loop& loop);
            set<unsigned> getLoopBBIDs(Loop& loop);
    };

#endif
