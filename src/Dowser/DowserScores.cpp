#include "DowserScores.hpp"

DowserScores::DowserScores(Representation& repr) {
    debug = false;    

    unsigned blockCount = repr.getBlockCount();
    for (unsigned block = 0; block < blockCount; block++) {
        blockScores.push_back(0);
    }

    unsigned blockOffset = 0;
    for (auto currentFunction = repr.begin();
              currentFunction != repr.end(); ++currentFunction) {
        Function function(currentFunction);
        if (debug) {
            cout << "Visiting function: " << function.getName();
        }
        //For each function, create dependency graph
        DataFlowGraph data(function, blockOffset);

        //Extract analysis groups from graph
        unsigned groupCounter = 0;
        vector<AnalysisGroup> groups = data.getAnalysisGroups();
        if (debug) {
            cout << ", # of AGs: " << groups.size() << endl;
        }
        for (AnalysisGroup& group : groups) {
            if (debug) {
                cout << endl << "AG #" << groupCounter++ << ": ";
            }

            //Assign score for each group
            scoreAnalysisGroup(group);
        }
    
        //Maintain starting basic block id of each function
        blockOffset += function.getBlockCount();
    }

    if (debug) {
        cout << "Scores: [";
        for (unsigned block = 0; block < blockCount; block++) {
            if (block > 0) cout << ", ";
            cout << blockScores[block];
        }
        cout << "]" << endl << endl;
    }
}

vector<double> DowserScores::getBlockScores() {
    return blockScores;
}

void DowserScores::scoreAnalysisGroup(AnalysisGroup& group) {
    //Need to assign scores to internal block scoring vector based on features
    if (debug) {
        cout << group << endl;
    }
    vector<unsigned> bbids = group.getBlockIds();
    for (unsigned bbid : bbids) {
        if (debug) {
            cout << "Visiting bbid " << bbid << "..." << endl;
        }

        unsigned score = 0;
        //Check for index addition/subtraction local to iteration: 1
        unsigned count = group.getSimpleArithmetic(bbid, true).size();
        score += count;

        //Check for index addition/subtraction not local to iteration: 5
        count = group.getSimpleArithmetic(bbid, false).size();
        score += 5 * count;

        //Check for other index arithmetic operation (e.g., shift, xor): 10
        count = group.getOtherArithmetic(bbid).size();
        score += 10 * count;

        //Check for # of unique constants: 10 per value
        count = group.getUniqueConstantValues(bbid).size();
        score += 10 * count;

        //Check for values determined outside loop: 30
        count = group.getVariablesOutsideLoops(bbid).size();
        score += 30 * count;

        //Check for non inlined functions returning non-pointer values: 500
        count = group.getNonPointerNonInlineFnCalls(bbid).size();
        score += 500 * count;

        //Check for GetElemPtr local to iteration: 1
        count = group.getGetElemPtrs(bbid, true).size();
        score += count;

        //Check for GetElemPtr not local to iteration: 5
        count = group.getGetElemPtrs(bbid, false).size();
        score += 5 * count;

        //Check for pointer casts: 100
        count = group.getPointerCasts(bbid).size();
        score += 100 * count;

        if (debug) {
            cout << "Score (subtotal): " << score << endl;
        }

        //Save result
        blockScores[bbid] = (double)max(score, (unsigned)blockScores[bbid]);
        //blockScores[bbid] += score;
        if (debug) {
            cout << "Cumulative score: " << blockScores[bbid] << endl;
        }
    }
}
