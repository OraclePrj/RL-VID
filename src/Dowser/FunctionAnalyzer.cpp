#include "FunctionAnalyzer.hpp"

FunctionAnalyzer::FunctionAnalyzer() : function(NULL) {}

unsigned FunctionAnalyzer::getBBID(Block& block) {
    return bbids[block];
}

Block FunctionAnalyzer::getBlock(unsigned bbid) {
    if (bbid < blockOffset) {
        cerr << "FunctionAnalyzer: invalid bbid in getBlock" << endl;
        exit(1);
    }
    return functionBlocks[bbid - blockOffset];
}

Block FunctionAnalyzer::getBlock(Instruction& instruction) {
    Block block(instruction.getBlock());
    return block;
}

vector<Instruction> FunctionAnalyzer::getDependencies(Instruction& inst) {
    return dependencies.getDependencies(inst);
}

set<Instruction> FunctionAnalyzer::getGuards(Block& block, Loop& loop) {
    set<Block> seen;
    cout << "DEBUG: Checking block for guards: " << block << endl;
    return recursiveGuards(block, loop, seen);
}

vector<Loop> FunctionAnalyzer::getLoops() {
    return loops.getLoops(function);
}

void FunctionAnalyzer::initialize(Function& targetFn, unsigned offset) {
    function.function = targetFn.function;
    blockOffset = offset;

    dependencies.load(targetFn);
    postdominators.load(targetFn);

    for (auto blockPtr = targetFn.begin(); 
              blockPtr != targetFn.end(); ++blockPtr) {
        Block block(blockPtr);
        //Save block and remember its BBID.
        bbids[block] = offset;
        functionBlocks.emplace_back(block);
        offset++;
    }
}

bool FunctionAnalyzer::postdom(Block& leftOperand, Block& rightOperand) {
    return postdominators.postdom(leftOperand, rightOperand);
}

set<Instruction> FunctionAnalyzer::recursiveGuards(Block& child, Loop& loop, 
                                                          set<Block>& seen) {
    set<Instruction> guards;
    if (!loop.isIn(child)) {
        return guards;
    }

    seen.insert(child);
    for (Block& parent : child.getParents()) {
        if ((seen.count(parent) == 1) || !loop.isIn(parent)) {
            //Cycle or out of loop. Base case.
            continue;
        }

        if (!postdominators.postdom(child, parent)) {
            //There is a way around child in loop: parent guards child.
            Instruction guard = parent.getConditionVariable();
            guards.insert(guard);
            seen.insert(parent);
            //Note no recursion, since this is another base case.
        } else {
            //Parent doesn't guard, but anything guarding parent guards child
            for (Instruction guard : recursiveGuards(parent, loop, seen)) {
                guards.insert(guard);
            }
        }
    }

    return guards;
}

bool FunctionAnalyzer::sawCycle() {
    return dependencies.sawCycle();
}

void FunctionAnalyzer::setLoop(Loop& loop) {
    dependencies.setLoop(loop);
}
