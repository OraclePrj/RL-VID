#ifndef _FUNCTIONANALYZER_HPP_
    #define _FUNCTIONANALYZER_HPP_

    #include <iostream>
    #include <map>
    #include <set>
    #include <vector>
    #include "../StateGraph/Block.hpp"
    #include "../StateGraph/Function.hpp"
    #include "../StateGraph/Instruction.hpp"
    #include "DataFlowGraph2.hpp"
    #include "Loop.hpp"
    #include "LoopHandler.hpp"
    #include "PostDominatorTree.hpp"

    using namespace std;
    using namespace PRGM;

    class FunctionAnalyzer {
        public:
            FunctionAnalyzer();
            unsigned getBBID(Block& block);
            Block getBlock(unsigned bbid);
            Block getBlock(Instruction& instruction);
            vector<Instruction> getDependencies(Instruction& instruction);
            set<Instruction> getGuards(Block& block, Loop& loop);
            vector<Loop> getLoops();
            void initialize(Function& function, unsigned offset);
            bool postdom(Block& leftOperand, Block& rightOperand);
            bool sawCycle();
            void setLoop(Loop& loop);
        private:
            Function function;
            unsigned blockOffset;
            DataFlowGraph dependencies;
            LoopHandler loops;
            PostDominatorTree postdominators;
            vector<Block> functionBlocks;
            map<Block, unsigned> bbids;
            set<Instruction> recursiveGuards(Block& block, Loop& loop, 
                                                     set<Block>& seen);
    };

#endif
