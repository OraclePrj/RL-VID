#include "LoopHandler.hpp"

vector<Loop> LoopHandler::getLoops(Function& function) {
    vector<Loop> loops;
    if (function.getBlockCount() == 0) return loops;

    //Process function to determine dominators
    llvm::DominatorTree dominators(*function.function);
    //dominators.recalculate(*function.function);

    //Extract loop information
    llvm::LoopInfo loopMaker(dominators);
    //loopMaker.releaseMemory(); //May not be needed?
    //loopMaker.analyze(dominators);

    //Construct loops from extracted info.
    llvm::SmallVector<llvm::Loop*, 4> preorder = loopMaker.getLoopsInPreorder();
    for (llvm::Loop* loopPointer : preorder) {
        loops.push_back(Loop(loopPointer));
    }
    return loops;
}
