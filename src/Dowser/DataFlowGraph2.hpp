#ifndef __DATAFLOWGRAPH_HPP_
    #define __DATAFLOWGRAPH_HPP_

    #include <iostream>
    #include <map>
    #include <set>
    #include <string>
    #include <vector>

    #include "../StateGraph/Block.hpp"
    #include "../StateGraph/Function.hpp"
    #include "../StateGraph/Instruction.hpp"
    #include "../StateGraph/Operand.hpp"
    #include "../StateGraph/Representation.hpp"
    #include "../Utils/StrOps.hpp"
    #include "AnalysisGroup.hpp"
    #include "Loop.hpp"

    using namespace std;

    class DataFlowGraph {
        public:
            DataFlowGraph();
            vector<Instruction> getDependencies(Instruction& instruction);
            void load(Function& function);
            bool sawCycle();
            void setLoop(Loop& loop);
        private:
            Loop* loop;
            vector<Instruction> cycles;
            map<Instruction, vector<Instruction>> dependencies;
            set<Instruction> done, seen;
            map<string, vector<Instruction>> stores;

            void clearHistory();
            void getDependencies(Instruction& ins, vector<Instruction>& result);
            map<string, Instruction> label(Function& function);
    };

#endif
