#include "Loop.hpp"
Loop::Loop(llvm::Loop* pointer) {
    loop = pointer;

    //For each block in the loop:
    for (auto& blockPtr : loop->blocks()) {
        Block block(blockPtr);

        //Add block to set for isIn checks
        blocks.insert(block); //TODO: *

        //For each instruction in the block:
        for (auto instPtr = block.begin(); 
                  instPtr != block.end(); ++instPtr) {
            Instruction instruction(instPtr);

            //Add instruction to set for isIn checks
            instructions.insert(instruction); //TODO: *

            //If instruction is pointerAccess, add it to accesses
            if (instruction.isDereference()) {
                accesses.emplace_back(instruction);
            }
        }
    }

    //TODO: * Make sure data structures stored correctly in set.
}

set<Block> Loop::getBlocks() {
    return blocks;
}

vector<Instruction> Loop::getPointerAccesses() {
    return accesses;
}

/*
    Remember these methods exist on llvm::Loop
    bool .contains(llvm::BasicBlock*)
    bool .contains(llvm::Instruction*)
*/
bool Loop::isIn(Block& block) {
    bool result = (blocks.count(block) == 1);
    return result;
}

bool Loop::isIn(Instruction& instruction) {
    //TODO: Maybe find parent block and call isIn(Block&)?
    //That way don't have to store all the Instructions.
    return (instructions.count(instruction) == 1);
}
