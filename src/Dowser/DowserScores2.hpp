#ifndef _DOWSERSCORES_HPP_
    #define _DOWSERSCORES_HPP_

    #include <vector>
    #include "AnalysisGroup.hpp"
    #include "DowserFeatures.hpp"
    #include "../StateGraph/Representation.hpp"
    using namespace std;

    class DowserScores {
        public:
            DowserScores(Representation& repr);
            vector<double> getBlockScores();
        private:
            vector<double> blockScores;
            DowserFeatures features;
            bool debug;

            void scoreAnalysisGroup(AnalysisGroup& group);
    };

#endif
