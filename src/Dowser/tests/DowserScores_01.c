#include <stdbool.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
    int array[20];
    for (int index = 0; index < 20; index++) {
        bool guard = (index % 2 == 0);
        bool notGuard = (index % 3 == 0);
        if (guard) {
            array[index] = 0;
        }
        if (notGuard) {
            notGuard ^= 1;
        }
    }
    return 0;
}
