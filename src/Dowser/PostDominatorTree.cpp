#include "PostDominatorTree.hpp"

void PostDominatorTree::load(Function& function) {
    tree.recalculate(*function.function);
}

bool PostDominatorTree::postdom(const Block& postdominator, 
                                const Block& postdominatee) const {
    //return false;
    return tree.dominates(postdominator.block, postdominatee.block);
}
