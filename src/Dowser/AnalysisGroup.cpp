#include "AnalysisGroup.hpp"

AnalysisGroup::AnalysisGroup() {
    cycle = false;
    debug = false;
}

void AnalysisGroup::add(unsigned bbid, Instruction& instruction) {
    blocks[bbid].emplace_back(instruction);
}

void AnalysisGroup::filterBBIDsTo(set<unsigned> bbids) {
    //Any entry that is not in bbids should be erased.
    set<unsigned> toErase;
    for (auto& entry : blocks) {
        unsigned bbid = entry.first;
        if (bbids.count(bbid) == 0) {
            toErase.insert(bbid);
        }
    }
    for (unsigned bbid : toErase) {
        //Also want to save any Instructions outside the loop
        //TODO: This needs to be thought out better.
        //Should only be saving first variable outside loop, not its depends.
        //May not want to do this for every variable (e.g., pointers)
        for (Instruction instruction : blocks[bbid]) {
            outside.insert(instruction);
        }        
        blocks.erase(bbid);
    }
}

vector<unsigned> AnalysisGroup::getBlockIds() {
    vector<unsigned> result;
    for (auto& entry : blocks) {
        unsigned bbid = entry.first;
        result.push_back(bbid);
    }
    return result;
}

vector<Instruction> AnalysisGroup::getGetElemPtrs(unsigned bbid, bool local) {
    //# of GetElemPtrs in AG. local: whether contained to local iteration.
    vector<Instruction> result;
    if ((local && cycle) || (!local && !cycle)) return result;

    vector<Instruction>& instructions = blocks[bbid];
    for (Instruction& instruction : instructions) {
        if (instruction.isGetElemPtr()) {
            if (debug) {
                cout << "GetElemPtr Instruction: " << instruction << endl;
            }
            result.emplace_back(instruction);
        }
    }    
    return result;
}

vector<Instruction> AnalysisGroup::getInstructions() {
    vector<Instruction> result;
    return result;
}

vector<Instruction> AnalysisGroup::getNonPointerNonInlineFnCalls(unsigned bbid) {
    vector<Instruction>& instructions = blocks[bbid];
    vector<Instruction> result;
    for (Instruction& instruction : instructions) {
        //Skip non-function calls (inline functions are not fn calls either)
        if (!instruction.isFnCall()) continue; 

        //Add any function calls not returning pointers to result
        if (!instruction.evaluatesToPointer()) {
            string fnName = instruction.getCalledFnName();
            //Filter out internal LLVM functions
            if (startsWith("llvm.", fnName)) continue;
            if (debug) {
                cout << "Non ptr return value fn call: ";
                cout << instruction << endl;
            }
            result.emplace_back(instruction);
        }
    }
    return result;
}

vector<Instruction> AnalysisGroup::getOtherArithmetic(unsigned bbid) {
    //# of other index instructions (division, shift, xor...)
    vector<Instruction>& instructions = blocks[bbid];
    vector<Instruction> result;
    for (Instruction& instruction : instructions) {
        if (!instruction.isArithmetic()) continue;
        if (!instruction.isAdd() && !instruction.isSubtract()) {
            if (debug) {
                cout << "Arithmetic instruction: " << instruction << endl;
            }
            result.emplace_back(instruction);
        }
    } 
    return result;
}

vector<Instruction> AnalysisGroup::getPointerCasts(unsigned bbid) {
    vector<Instruction>& instructions = blocks[bbid];
    vector<Instruction> result;
    for (Instruction& instruction : instructions) {
        if (instruction.isCast() && instruction.evaluatesToPointer()) {
            if (debug) {
                cout << "Pointer cast instruction: " << instruction << endl;
            }
            result.emplace_back(instruction);
        }
    }
    return result;
}

vector<Instruction> AnalysisGroup::getSimpleArithmetic(unsigned bbid, 
                                                           bool local) {
    //local: Whether contained to local iteration. 
    vector<Instruction> result;
    if ((local && cycle) || (!local && !cycle)) return result;

    vector<Instruction>& instructions = blocks[bbid];
    for (Instruction& instruction : instructions) {
        if (instruction.isAdd() || instruction.isSubtract()) {
            if (debug) {
                cout << "Add/Sub Instr: " << instruction << endl;
            }
            result.emplace_back(instruction);
        }
    } 
    return result;
}

vector<double> AnalysisGroup::getUniqueConstantValues(unsigned bbid) {
    //Need to look at all operands of instructions and see if constants.
    set<double> seen;
    vector<Instruction>& instructions = blocks[bbid];
    for (Instruction& instruction : instructions) {
        if (instruction.isAlloc()) continue; //Skip allocations
        vector<Operand> operands = instruction.getOperands();
        for (Operand& operand : operands) {
            if (operand.isConstant()) {
                double value = operand.getConstantValue();
                if (debug) {
                    cout << "Instruction: " << instruction << endl;
                    cout << "Constant: " << operand << ", value: "; 
                    cout << value << endl;
                }
                seen.insert(value);
            }
        }
    }

    vector<double> result;
    for (double value : seen) {
        result.push_back(value);
    }
    return result;
}

vector<Instruction> AnalysisGroup::getVariablesOutsideLoops(unsigned bbid) {
    vector<Instruction> result;
    for (Instruction instruction : outside) {
        result.emplace_back(instruction);
    }
    return result;
}

bool AnalysisGroup::hasCycle() {
    return false;
}

void AnalysisGroup::setCycle(bool hasCycle) {
    cycle = hasCycle;
}

unsigned AnalysisGroup::size() const {
    unsigned result = 0;
    for (auto& entry : blocks) {
        //Add each block's instruction count to total
        const vector<Instruction>& instructionList = entry.second;
        unsigned instructionCount = instructionList.size();
        result += instructionCount;
    }
    return result + outside.size();
}

string AnalysisGroup::toString() const {
    ostringstream builder;
    builder << "AnalysisGroup of size " << size();
    builder << " with ";
    if (!cycle) builder << "no ";
    builder << "cycles: {";
    unsigned blockCount = 0;
    for (auto& block : blocks) {
        if (blockCount++ > 0) builder << "; ";
        const unsigned& bbid = block.first;
        builder << "<" << bbid << ">" << ": ";
        unsigned instCount = 0;
        const vector<Instruction>& instructions = block.second;
        for (const Instruction& instruction : instructions) {
            if (instCount++ > 0) builder << ", ";
            builder << instruction;
        }
        //TODO: Remove this. NEW
        builder << "; outside:";
        instCount = 0;
        for (const Instruction& instruction : outside) {
            if (instCount++ > 0) builder << ", ";
            builder << instruction;
        }
        //TODO: End new.
    }
    builder << "}";
    return builder.str();
}

ostream& operator<<(ostream& output, const AnalysisGroup& group) {
    output << group.toString();
    return output;
}


ostream& operator<<(ostream& output, const vector<AnalysisGroup>& groups) {
    output << "[";
    unsigned count = 0;
    for (const AnalysisGroup& group : groups) {
        if (count++ > 0) output << ", ";
        output << group;
    }
    output << "]";
    return output;
}
