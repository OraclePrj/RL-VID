#ifndef _DOWSERSCORES_HPP_
    #define _DOWSERSCORES_HPP_

    #include <vector>
    #include "AnalysisGroup.hpp"
    #include "DataFlowGraph.hpp"
    #include "../StateGraph/Representation.hpp"
    using namespace std;

    class DowserScores {
        public:
            DowserScores(Representation& repr);
            vector<double> getBlockScores();
            void scoreAnalysisGroup(AnalysisGroup& group); //TODO: Make priv
        private:
            vector<double> blockScores;
            bool debug;

    };

#endif
