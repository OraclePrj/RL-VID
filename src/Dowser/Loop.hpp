#ifndef _LOOP_HPP_
    #define _LOOP_HPP_
    #include <iostream>
    #include <set>
    #include <vector>
    #include "llvm/Analysis/LoopInfo.h"
    #include "../StateGraph/Block.hpp"
    #include "../StateGraph/Instruction.hpp"

    using namespace std;
    using namespace PRGM;
    class Loop {
        public:
            Loop(llvm::Loop* pointer);
            set<Block> getBlocks();
            vector<Instruction> getPointerAccesses();
            bool isIn(Block& block);
            bool isIn(Instruction& instruction);
        private:
            llvm::Loop* loop;
            set<Block> blocks;
            set<Instruction> instructions; //TODO: Can avoid storing?
            vector<Instruction> accesses;
    };

#endif
