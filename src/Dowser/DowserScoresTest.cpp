
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE DowserScores
#include <boost/test/unit_test.hpp>

#include <vector>
#include "DowserScores2.hpp"
#include "../StateGraph/Representation.hpp"

/*
BOOST_AUTO_TEST_CASE( DowserScores_00 )
{
    //Reminder: These scores will change when considering other AGs/features.
    //Manually recalculate by hand.
    Representation repr("tests/DowserScores_00.bc");
    DowserScores scores(repr);
    //vector<double> expected = {0, 10, 30, 15, 0};
    vector<double> expected = {0, 40, 60, 45, 0};
 
    vector<double> actual = scores.getBlockScores();
    BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(), actual.end(),
                                  expected.begin(), expected.end());
}
*/

BOOST_AUTO_TEST_CASE( DowserScores_01 )
{
    //Reminder: These scores will change when considering other AGs/features.
    //Manually recalculate by hand.
    Representation repr("tests/DowserScores_01.bc");
    DowserScores scores(repr);
    //vector<double> expected = {0, 10, 30, 15, 0, 0, 0, 15, 0};
    vector<double> expected = {0, 40, 60, 45, 0, 0, 0, 45, 0};
 
    vector<double> actual = scores.getBlockScores();
    BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(), actual.end(),
                                  expected.begin(), expected.end());
}
