#include "AnalysisGroup.hpp"
#include "DowserScores.hpp"
#include "../StateGraph/Representation.hpp"
#include "../Rewards.hpp"
#include "../Utils/StrOps.hpp"

using namespace PRGM;

int main(int argc, char* argv[]) {
    Representation repr("./example.bc");
    DowserScores dowser(repr);
    cout << dowser.getBlockScores() << endl;

    //Rewards rewards(repr);
    return 0;
}
