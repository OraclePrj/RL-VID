#ifndef __DATAFLOWGRAPH_HPP_
    #define __DATAFLOWGRAPH_HPP_

    #include <iostream>
    #include <map>
    #include <set>
    #include <string>
    #include <vector>

    #include "../StateGraph/Block.hpp"
    #include "../StateGraph/Function.hpp"
    #include "../StateGraph/Instruction.hpp"
    #include "../StateGraph/Operand.hpp"
    #include "../StateGraph/Representation.hpp"
    #include "../Utils/StrOps.hpp"
    #include "AnalysisGroup.hpp"

    using namespace std;

    class DataFlowGraph {
        public:
            DataFlowGraph(Function& function, unsigned offset);
            vector<AnalysisGroup> getAnalysisGroups();
        private:
            map<Instruction, unsigned> blocks;
            map<Instruction, vector<Instruction>> dependencies;
            vector<Instruction> pointerDerefs;
            set<Instruction> seen;

            void buildAnalysisGroup(Instruction& ins, AnalysisGroup& group);
            AnalysisGroup getAnalysisGroup(Instruction& instr);
            vector<Instruction> getPointerDereferences();
            map<string, Instruction> label(Function& function);
    };

#endif
