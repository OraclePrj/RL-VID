#include "DataFlowGraph.hpp"

DataFlowGraph::DataFlowGraph(Function& function, unsigned offset) {
    map<string, Instruction> names = label(function);
    unsigned bbid = offset;

    //For each block in function...
    for (auto currentBlock = function.begin();
              currentBlock != function.end(); ++currentBlock) {
        Block block(currentBlock);
        //For each instruction in the basic block...
        for (auto currentInstr = block.begin();
                  currentInstr != block.end(); ++currentInstr) {
            //Set up data structures
            Instruction instruction(currentInstr);
            vector<Instruction> dependencies;
            
            //Get Instruction's dependencies and find matching instructions
            for (Operand& operand : instruction.getOperands()) {
                if (names.count(operand.getLabel()) == 1) {
		    auto& entry = *names.find(operand.getLabel());
		    Instruction dependency(entry.second);
		    dependencies.emplace_back(dependency);
                }
            }

            //Save results
            this->dependencies[instruction] = dependencies;
            
            //If instruction is pointer access, save it
            if (instruction.isDereference()) {
                pointerDerefs.push_back(instruction);
            }

            //Save block id of instruction.
            blocks[instruction] = bbid;
            //cout << bbid << ": " << instruction << endl;
        }
        
        //Advance id to next block's
        bbid++;
    }
}

vector<AnalysisGroup> DataFlowGraph::getAnalysisGroups() {
    vector<AnalysisGroup> groups;
    for (Instruction& instruction : getPointerDereferences()) {
        groups.push_back(getAnalysisGroup(instruction));
    }
    return groups;
}

AnalysisGroup DataFlowGraph::getAnalysisGroup(Instruction& instr) {
    //Perform DFS in graph starting from this instruction, + detect cycles.
    seen.clear();
    AnalysisGroup group;
    buildAnalysisGroup(instr, group);
    return group;
}

void DataFlowGraph::buildAnalysisGroup(Instruction& ins, AnalysisGroup& group) {
    //Recursively build Analysis group using DFS from given instruction.
    if (seen.count(ins) == 1) {
        //Already seen instruction in DFS. Cycle exists. Ignore instruction.
        group.setCycle(true);
    } else {
        //Unseen instruction. Save in access group and record as seen.
        group.add(blocks[ins], ins);
	seen.insert(ins);
        
        //Recursively include all its dependencies
        for (Instruction& other : dependencies[ins]) {
            buildAnalysisGroup(other, group);
        }
    }
}

vector<Instruction> DataFlowGraph::getPointerDereferences() {
    return pointerDerefs;
}

map<string, Instruction> DataFlowGraph::label(Function& function) {
    map<string, Instruction> names;
    unsigned uid = 0;
    //For each block in function...
    for (auto currentBlock = function.begin();
              currentBlock != function.end(); ++currentBlock) {
        Block block(currentBlock);
        //For each instruction in the basic block...
        for (auto currentInstr = block.begin();
                  currentInstr != block.end(); ++currentInstr) {
            Instruction instruction(currentInstr);
            //Get Instruction's name, creating one if necessary.
            string name = instruction.getLabel();
            if (name.empty()) {
                //Assign UID to instruction
                ostringstream nameBuilder;
                nameBuilder << uid++;
                name = nameBuilder.str();
                instruction.setLabel(name);
            }

            //Save instruction to result to look up later.
            names.insert(pair<string, Instruction>(name, instruction));
        }
    }
    return names;
}
