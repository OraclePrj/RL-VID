#include "DataFlowGraph2.hpp"

DataFlowGraph::DataFlowGraph() {
    loop = NULL;
}

void DataFlowGraph::clearHistory() {
    cycles.clear();
    done.clear();
    seen.clear();
}

vector<Instruction> DataFlowGraph::getDependencies(Instruction& instruction) {
    //Finds ALL dependencies of given instruction recursively
    clearHistory();
    vector<Instruction> result;
    getDependencies(instruction, result);
    return result;
}

void DataFlowGraph::getDependencies(Instruction& instruction,
			            vector<Instruction>& result) {
    //Recursively build all dependencies using DFS from given instruction.
    if (seen.count(instruction) == 1) {
        //Already seen instruction in DFS, so not recursing.
        if (done.count(instruction) == 0) {
            //Cycle exists.
            cycles.emplace_back(instruction);
        }
    } else {
        //Unseen instruction. Save in access group and record as seen.
        result.emplace_back(instruction);
	seen.insert(instruction);
        
        //Recursively include all its dependencies
        for (Instruction& other : dependencies[instruction]) {
            if (loop == NULL) {
                //No restrictions.
                getDependencies(other, result);
            } else if (loop->isIn(instruction)) {
                //Always get a dependency if you are in the loop.
                getDependencies(other, result);
            } else if (loop->isIn(other)) {
                //Always get a dependency if it is in the loop.
                getDependencies(other, result);
            }
        }

        //Finished with this instruction
        done.insert(instruction);
    }
}

void DataFlowGraph::load(Function& function) {
    loop = NULL;
    clearHistory();

    map<string, Instruction> names = label(function);

    //For each block in function...
    for (auto currentBlock = function.begin();
              currentBlock != function.end(); ++currentBlock) {
        Block block(currentBlock);
        //For each instruction in the basic block...
        for (auto currentInstr = block.begin();
                  currentInstr != block.end(); ++currentInstr) {
            //Set up data structures
            Instruction instruction(currentInstr);
            vector<Instruction> dependencies;
            
            //Get Instruction's dependencies and find matching instructions
            for (Operand& operand : instruction.getOperands()) {
                string name = operand.getLabel();
                if (names.count(name) == 1) {
		    auto& entry = *names.find(name);
		    Instruction dependency(entry.second);
		    dependencies.emplace_back(dependency);
                }
            }

            if (instruction.isLoad()) {
                //Add any stores into loaded address as dependencies.
                Operand address = instruction.getOperands()[0];
                string name = address.getLabel();
                if (stores.count(name) == 1) {
                   auto& entry = *stores.find(name);
                   for (Instruction dependency : entry.second) {
                       dependencies.emplace_back(dependency);
                   }
                }
            }

/*
            if (instruction.isStore()) {
                //If both operands are variables:
                cout << "Store instruction: " << instruction << endl;
                vector<Instruction> variables;
                for (Operand & operand : instruction.getOperands()) {
                    if (names.count(operand.getLabel()) == 1) {
		        auto& entry = *names.find(operand.getLabel());
		        Instruction variable(entry.second);
		        variables.emplace_back(variable);
                    }
                }
                if (variables.size() == 2) {
                    //Storing a variable into a variable. Add store dependency.
                    vector<Instruction> dependency;
                    Instruction value = variables[0];
                    Instruction destination = variables[1];
                    dependency.emplace_back(value);
                    this->dependencies[destination] = dependency;
                }
            }
*/
            //Save results
            this->dependencies[instruction] = dependencies;
        }
    }
}

map<string, Instruction> DataFlowGraph::label(Function& function) {
    map<string, Instruction> names;
    unsigned uid = 0;
    //For each block in function...
    for (auto currentBlock = function.begin();
              currentBlock != function.end(); ++currentBlock) {
        Block block(currentBlock);
        //For each instruction in the basic block...
        for (auto currentInstr = block.begin();
                  currentInstr != block.end(); ++currentInstr) {
            Instruction instruction(currentInstr);
            //Get Instruction's name, creating one if necessary.
            string name = instruction.getLabel();
            if (name.empty()) {
                //Assign UID to instruction
                ostringstream nameBuilder;
                nameBuilder << uid++;
                name = nameBuilder.str();
                instruction.setLabel(name);
            }

            //Save instruction to result to look up later.
            names.insert(pair<string, Instruction>(name, instruction));

            //If instruction is a store, save it for later too.
            if (instruction.isStore()) {
                //Instruction: store value -> address. Need address.
                Operand address = instruction.getOperands()[1];
                if (address.hasLabel()) {
                    name = address.getLabel();
                    if (stores.count(name) == 0) {
                        vector<Instruction> list;
                        stores[name] = list;
                    }
                    stores[name].push_back(instruction);
                }
            }
        }
    }
    return names;
}

bool DataFlowGraph::sawCycle() {
    return (cycles.size() > 0);
}

void DataFlowGraph::setLoop(Loop& loop) {
    this->loop = &loop;
}
