#ifndef _PROGRESSBAR_HPP_
    #define _PROGRESSBAR_HPP_

    #include <iostream>
    #include <sstream>
    #include <string>

    using namespace std;

    class ProgressBar {
        public:
            ProgressBar(int max);
            bool done() const;
            bool next();
            void show();
            //string toString() const;
        private:
            int current, max;
    };

    //ostream& operator<<(ostream& os, const ProgressBar& bar);

#endif
