#ifndef _CONFIG_HPP_
    #define _CONFIG_HPP_

    #include <iostream>
    #include <string>
    #include "QBASE/IntSpace.hpp"

    using namespace std;

    enum PolicyMode {UCB_MODE, QBASE_MODE};

    class Config {
        public:
            Config();
            int getActionCount();
            IntSpace& getActionSpace();
            string getIRFilename();
            PolicyMode getPolicyMode();
            string getProgramFilename();
        private:
            int actionCount;
            IntSpace actionSpace;
            string irFilename;
            string prgmFilename;
            PolicyMode policyMode;
    };

#endif
