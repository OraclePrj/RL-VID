#ifndef _REWARDS_HPP_
    #define _REWARDS_HPP_
    #include "Dowser/DowserScores.hpp"
    #include "StateGraph/Block.hpp"
    #include "StateGraph/Function.hpp"
    #include "StateGraph/Instruction.hpp"
    #include "StateGraph/Representation.hpp"
    #include "Utils/StrOps.hpp"

    #include <cmath>
    #include <iostream>
    #include <string>
    #include <vector>

    using namespace PRGM;
    using namespace std;
    
    class Rewards {
        public:
            Rewards(Representation& repr);
            void analyze(Block& block);
            bool isTerminal(int state);
            double getStateReward(int state);
            string toString() const;
            double value(vector<int>& visits);
        private:
            vector<double> discounts;
            vector<double> rewards;
    };

#endif 
