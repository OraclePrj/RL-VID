#ifndef _CLIARGS_HPP_
    #define _CLIARGS_HPP_
    #include <boost/program_options.hpp>
    #include <iostream>
    #include <limits>
    #include <string>

    using namespace std;
    namespace po = boost::program_options;

    class CLIArgs {
        public:
            CLIArgs(int argc, char** argv);
            bool exist();
            unsigned getBatchSize();
            unsigned getBestSize();
            unsigned getParameterId();
            unsigned getUpdatePeriod();
        private:
            bool success;
            unsigned batchSize, bestSize, parameterId, updatePeriod;
    };

#endif
