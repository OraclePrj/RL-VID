#include "MCTS.hpp"

MCTS::MCTS(Config& config) : policy(config), sim(config) {
    sim.setMaxDepth(5);
    sim.setPolicy(policy);
}

Policy& MCTS::getPolicy() {
    return policy;
}

Simulator& MCTS::getSimulator() {
    return sim;
}

MCT_Tree& MCTS::getTree() {
    return tree;
}

void MCTS::handle(CLIArgs& args) {
    policy.handle(args);
}

void MCTS::reset() {
   tree.clear();
}

void MCTS::step() {
    sim.reset();
    int firstState = sim.startup();
    MCT_StateNode* leaf = MCTS::select(firstState);
    MCT_StateNode* expanded = expand(leaf);
    double value = sim.simulate(expanded, policy);
    backpropagate(value, expanded);
}

void MCTS::update() {
    //Find best actions and pass to Simulator to update.
    vector<Path> bestPaths = tree.getBestPaths();
    sim.update(bestPaths); 
}

MCT_StateNode* MCTS::select(int firstState) {
    MCT_Node* current = tree.getRoot();

    //NEW for UCB
    path.push_back(current);
    current = current->getChild(0); //action 0
    path.push_back(current);
    current = current->getChild(firstState);
    path.push_back(current);

    while (!current->isLeaf()) {
        //Choose action
	int action = policy.tree(current, 2);
        current = current->getChild(action);
        path.push_back(current);

        //Perform action and see result
        int state = sim.perform(action);
        current = current->getChild(state);
        path.push_back(current);
    }

    return dynamic_cast<MCT_StateNode*>(current);
}

MCT_StateNode* MCTS::expand(MCT_StateNode* node) {
    MCT_StateNode* root = dynamic_cast<MCT_StateNode*>(tree.getRoot()); 
    if (!node->isTerminal() && (node->getVisits() > 0 || node == root)) {
        //Use TreePolicy (same as in select) to choose new action
        int action = policy.tree(node, 2); //TODO: Remove actionCount
        MCT_Node* actionNode = node->getChild(action);
        path.push_back(actionNode);

        //Perform action to see resulting state and rewards
        int state = sim.perform(action);

        //Get that child from tree (i.e., create it) & return ptr to it.
        MCT_Node* child = actionNode->getChild(state);
        path.push_back(child);
        return dynamic_cast<MCT_StateNode*>(child);
    }
    return node;
}

void MCTS::backpropagate(double value, MCT_StateNode* expanded) {
    MCT_StateNode* state = NULL;
    MCT_ActionNode* action = NULL;
    vector<double> rewards = sim.getRewards();
    while (path.size() > 0) {
       //Update state
       state = dynamic_cast<MCT_StateNode*>(path.back());
       path.pop_back();

       //Make sure knows value of taken action (if any) was changed.
       if (action != NULL) {
           state->update(action);
       } else if (state->getVisits() == 0) {
           //First time visited.
           state->assign(value);
       } else {
           //Must be terminal node.
           state->recordVisit();
       }

       //This will run all but last time.
       if (path.size() > 0) {
           //Update action
           action = dynamic_cast<MCT_ActionNode*>(path.back());
           path.pop_back();
           action->update(rewards.back(), state);
           rewards.pop_back();
       }
    }
}
