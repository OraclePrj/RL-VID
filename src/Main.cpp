#include <chrono>
#include "Config.hpp"
#include "CLIArgs.hpp"
#include "Logger.hpp"
#include "MCTS.hpp"
#include "ProgressBar.hpp"

int main(int argc, char** argv) {
    int updates = 6;
    int trials = 1000;
    ProgressBar progress(updates);
    Config config;
    MCTS mcts(config);
    string logFileName = "log.log";
    CLIArgs args(argc, argv);
    if (args.exist()) {
        /*
        //Set requested settings on QBASE.
        QBASE& qbase = mcts.getPolicy().getQBASE();
        qbase.setBatchSize(args.getBatchSize());
        qbase.setBestSize(args.getBestSize());
        qbase.setUpdatePeriod(args.getUpdatePeriod());
        */
        mcts.handle(args);

        //Also change logFile name to include ID.
        ostringstream builder;
        builder << "log" << args.getParameterId() << ".log";
        logFileName = builder.str();

    }

    Simulator& simulator = mcts.getSimulator();
    Logger logger(logFileName, simulator);
    cout << "Starting trials..." << endl;
    chrono::steady_clock::time_point start = chrono::steady_clock::now();
    chrono::steady_clock::time_point wayPt;
    progress.show();
    bool hasCrashed = false;
    //TEMP-DISABLE: if (config.getPolicyMode() == QBASE_MODE) mcts.reset();
    for (int update = 0; update < updates; update++) {
        mcts.reset(); //TEMP-DISABLE: if (config.getPolicyMode() == UCB_MODE) mcts.reset();
        for (int trial = 0; trial < trials; trial++) {
            mcts.step();
            if (!hasCrashed) {
                bool crashed = (simulator.current() == 1); //TODO: Refactor
                if (crashed) {
                    wayPt = chrono::steady_clock::now();
                    chrono::duration<double> crashTime = wayPt - start;
                    cout << "\rFirst crash at: " << crashTime.count() << " sec";
                    cout << "                                        " << endl;
                    progress.show();
                    hasCrashed = true;
                }
            }
        }
        logger.log(mcts.getTree()); //TEMP-DISABLE: if (config.getPolicyMode() == UCB_MODE) logger.log(mcts.getTree());
        progress.next();
        progress.show();
        mcts.update();
    }
    //TEMP-DISABLE if (config.getPolicyMode() == QBASE_MODE) logger.log(mcts.getTree());
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    progress.show();
    chrono::duration<double> elapsed = end - start;
    cout << endl << "Time elapsed: " << elapsed.count() << " sec" << endl;
}
