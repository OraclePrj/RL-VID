#include <stdio.h>

int main() {
   int length = 42;
   int array[length];
   int index = 1;
   for (int ii = 0; ii < length; ii++) {
       if (ii % 7 == 0) {
           index--;
       }
       array[index++] = ii;
   }
   /*
   printf("[");
   for (int jj = 0; jj < length; jj++) {
       if (jj > 0) printf(", ");
       printf("%d", array[jj]);
   }
   printf("]\n");
   */
   return array[0];
}
