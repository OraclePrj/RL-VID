class TimeFile:
    def __init__(self, filename):
        self.crash_time = None
        with open(filename) as openFile:
            for line in openFile:
                if "crash" in line:
                    self.crash_time = self._extract_time(line)
                elif "elapsed" in line:
                    self.runtime = self._extract_time(line)

    def _extract_time(self, line):
        line = line.strip()
        tokens = line.split()
        return float(tokens[-2])
