import os

class ParameterSummary:
    def __init__(self, filename):
        self.filename = filename

    def exists(self):
        return os.path.exists('./' + self.filename)

    def load(self):
        if not self.exists():
            raise Exception("ParameterRunData file doesn't exist")
        # Else: load data
        print("ParameterSummary.load still not written") # TODO

    def save(self, analyzer):
        # Open file for appending
        with open(self.filename, "a+") as openFile:
            # Append away
            columns = (analyzer.time_to_crash, analyzer.total_runtime,
                       analyzer.total_crashes, analyzer.total_samples)
            columns = (str(column) for column in columns)
            row = ",".join(columns) + "\n"
            openFile.write(row)

