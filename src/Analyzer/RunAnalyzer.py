from Visualizer.StateNode import StateNode
from .ParameterSummary import ParameterSummary

#TODO: Shouldn't know these.
def is_crash(state_no):
    return state_no == 1

def is_terminal(state_no):
    return state_no == 1 or state_no == 2

class RunAnalyzer:
    def __init__(self):
        self.time_to_crash = None
        self.total_runtime = None
        self.total_crashes = 0
        self.total_samples = 0

    def analyze(self, log, time):
        #Time to crash and total runtime
        self.time_to_crash = time.crash_time
        self.total_runtime = time.runtime

        #Total crashes and total samples
        last_timestep = log.steps[-1]
        first_node = True
        for node in last_timestep.tree:
            if isinstance(node, StateNode):
                state = node.id.split(",")[-1]
                state_no = int(state[1:])
                if first_node:
                    self.total_samples = node.visits
                    first_node = False
                if is_crash(state_no):
                    self.total_crashes += node.visits

    def save(self, filename):
        summary = ParameterSummary(filename)
        summary.save(self)
