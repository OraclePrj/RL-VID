#include "Process.hpp"

Process::Process(string filename) : pipe(BIDIRECTIONAL) {
    //cout << "Loading Process" << endl;
    this->childPid = -1;
    this->debugger = -1;
    this->filename = filename;
    this->streamMode = UNSET;
    //cout << "Process loading" << endl;
}

Process::~Process() {
    if (isRunning()) {
        stop();
    }
}

void Process::allowDebugger(int pid) {
    debugger = pid;
}
            
vector<int> Process::getChildPids() {
    ostringstream command;
    command << "pgrep -P" << childPid;
    //TODO: If needed, call fflush(stdout) here.
    FILE* reply = popen(command.str().c_str(), "r");
    int bufferLength = 65535;
    vector<int> result;
    char buffer[bufferLength];
    memset(buffer, '\0', bufferLength - 1);
    char* stringRead = fgets(buffer, bufferLength - 1, reply); 
    while (stringRead != NULL) {
        result.push_back(stoi(string(buffer)));
        memset(buffer, '\0', bufferLength - 1);
        stringRead = fgets(buffer, bufferLength - 1, reply);
    }
    pclose(reply);
    return result;
}

//BE AWARE: WEXITSTATUS() only returns last 8 bits of exit status.
int Process::getReturnValue() {
    if (isRunning()) {
        int result;
        waitpid(childPid, &result, 0);
        if (WIFEXITED(result)) {
            int returnValue = WEXITSTATUS(result);
            return returnValue;
        }
    }
    //Either not running or crashed. TODO: Handle better
    return -1;
}

bool Process::getCrashStatus() {
    bool crashed = true;

    if (isRunning()) {
        //Wait on child to read its exit status (and reap zombie process).
        int result;
        waitpid(childPid, &result, 0);

        //Check for if exited normally or signaled
        if (WIFEXITED(result)) {
            //Exited normally
            WEXITSTATUS(result);
            crashed = (result != 0);
        } else if (WIFSIGNALED(result)) {
            //Exited abnormally, flag is false by default
            WTERMSIG(result);
        }

        //Reset childPid.
        childPid = -1;

        //Clean up pipes (otherwise will leak open fds!)
        pipe.stop();
    }

    return crashed;
}

int  Process::getPid() {
    return childPid;
}

void Process::interrupt() {
    interrupt(childPid);
}

void Process::interrupt(int pid) {
    kill(pid, SIGINT);
}

bool Process::isRunning() {
    //Returns whether or not the program is currently running.
    return childPid > 0;
}

void Process::launch() {
    vector<string> args;
    launch(args);
}

void Process::launch(vector<string>& args) {
    //Launches filename with given args. Exits with error if invalid.
    pipe.start(BIDIRECTIONAL);
    childPid = fork();
    if (childPid < 0) {
        int error = errno;
        cerr << "Error forking " << filename << ", errno = " << error << endl;
        exit(1);
    } else if (childPid == 0) {
        //Child. Close write end of toChildPipe, read end of fromChildPipe
        pipe.setOwner(CHILD);

        //Attach pipes to stdin/stdout/stderr
        if (streamMode == STDIO) {
            pipe.connectToStdIO();
        } else if (streamMode == STDSTREAMS) {
            pipe.connectToStdStreams();
        }

        //Allow debugger if applicable
        if (debugger != -1) {
            //Allows this pid to ptrace new process
            prctl(PR_SET_PTRACER, debugger);
        }

        //Extract arguments from vector into array
        char* arguments[1 + args.size() + 1];

        //First argument is always program name.
        arguments[0] = strdup(filename.c_str());

        //Other arguments are as appropriate
        for (unsigned ii = 0; ii < args.size(); ii++) {
            arguments[ii + 1] = strdup(args[ii].c_str());
        }

        //Terminate the argument vector.
        arguments[1 + args.size()] = (char *)NULL;

        //Synchronize with parent and exec.
        synchronize();
        execvp(filename.c_str(), arguments);

        //Shouldn't still be here!
        int errorCode = errno;
        cerr << "Error while exec'ing: " << errorCode << endl;
        cerr << "Filename: ( " + filename + " ) " << endl;
        for (unsigned ii = 0; ii < args.size() + 2; ii++) {
            if (arguments[ii] != (char*) NULL) {
                cerr << "Arg " << ii << ": ( ";
                cerr << arguments[ii] << " )" << endl;
            }
        }

        _exit(1);
    } else {
        //Parent.
        pipe.setOwner(PARENT);
        synchronize();
    }
}

void Process::sendInput(string input) {
    stringstream stringMaker;
    stringMaker << input << endl;
    string msg = stringMaker.str();
    cerr << "============SENDING INPUT: " << msg << "===========" << endl;
    pipe.writeLine(msg);
}

void Process::setStreamMode(StreamMode mode) {
    this->streamMode = mode;
}

void Process::stop() {
    kill(childPid, SIGKILL);
    getCrashStatus();
}

void Process::synchronize() {
    //Forces parent to wait until child is about to exec before continuing.
    if (childPid == 0) {
        pipe.writeChar('*'); //NEW
        //printf("*");
        //fflush(stdout);
    } else if (childPid > 0) {
        string reply = pipe.readChars(1);
        if (reply != "*") {
            throw invalid_argument("Parent failed to sync with child.");
        }
    } else {
        //Called at inappropriate time
        throw invalid_argument("Synchronize called before successful fork");
    }
}
