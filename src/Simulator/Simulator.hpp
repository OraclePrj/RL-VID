#ifndef _SIMULATOR_HPP_
    #define _SIMULATOR_HPP_
    #include <vector>
    #include "../Config.hpp"
    #include "InputGenerator.hpp"
    #include "../QBASE/IntSpace.hpp"
    #include "../Policy.hpp"
    #include "Random.hpp"
    #include "TargetProgram.hpp"
    #include "../Rewards.hpp"
    #include "../StateGraph/Representation.hpp"
    #include "../Tree/MCT_StateNode.hpp"
    #include "../Tree/Path.hpp"
    #include "../Utils/StrOps.hpp"

    using namespace PRGM;
    using namespace std;

    class Simulator {
        public:
            Simulator(Config& config);
            int current();
            double getPercentOutofRange(double limit, int action);
            TargetProgram& getProgram();
            vector<double> getRewards();
            int perform(int action);
            string printAction(vector<int> states, int action);
            void reset();
            void setDebugger(int pid);
            void setMaxDepth(int depth);
            void setPolicy(Policy& policy);
            double simulate(MCT_StateNode* state, Policy& policy);
            int startup();
            void update(vector<Path>& bestPaths);
        private:
            Random random;
	    Representation repr;
            InputGenerator actions;
            unsigned maxDepth;
            Rewards rewards;
            TargetProgram program;
            vector<double> collected;
            int state;
            vector<int> states;
    };

#endif
