#include "Random.hpp"

Random :: Random() {
    unsigned seed = getTimeBasedSeed();
    reseed(seed);
}

Random :: Random(unsigned seed) {
    reseed(seed);
}

const string Random :: DIGITS = "1234567890";
const string Random :: UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const string Random :: LOWER = "abcdefghijklmnopqrstuvwxyz";
const string Random :: SPECIAL_NO_SPACE = "~`!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?";
const string Random :: SPECIAL = SPECIAL_NO_SPACE + ' ';
const string Random :: ALPHANUMERIC = DIGITS + UPPER + LOWER;
const string Random :: PRINTABLE = DIGITS + UPPER + LOWER + SPECIAL;

int Random :: integer() {
    //No arguments = entire integer range
    int low = numeric_limits<int>::min();
    int high = numeric_limits<int>::max();
    
    return integer(low, high);
}

int Random :: integer(int low, int high) {
    uniform_int_distribution<int> distro(low, high);
    
    return distro(generator);
}

string Random :: nonNumericString(int len) {
    return nonNumericString(len, PRINTABLE);
}

string Random :: nonNumericString(int len, string charset) {
    uniform_int_distribution<int> distro(0, charset.size());
    string result = "";
    for (int ii = 0; ii < len; ii++) {
       int index = distro(generator);
       char letter = charset[index];
       result += letter;
    }
    return result;
}
string Random :: numericString(int threshold, bool greaterEq) {
    // Returns a random string picked in relation to the given threshold
    //  If greaterEq, chooses a string >= threshold
    //  Otherwise,    chooses a string strictly < threshold
    int high = threshold - 1, low = threshold;
    if (greaterEq) {
        high = numeric_limits<int>::max();  // int max
    } else {
        low = numeric_limits<int>::min();   // int low
    }
   
    //Actually pick number string will represent 
    int number = integer(low, high);

    //Convert value to string.
    ostringstream builder;
    builder << number;
    return builder.str();
}

string Random :: numericString(int start, int end) {
    ostringstream builder;
    builder << integer(start, end);
    return builder.str();
}

double Random :: real() {
    //No arguments = entire double (finite) range
    double low  = numeric_limits<double>::min();
    double high = numeric_limits<double>::max();
    
    return real(low, high);
}

double Random :: real(double low, double high) {
    uniform_real_distribution<double> distro(low, high);
    
    return distro(generator);
}

void Random :: reseed(unsigned seed) {
    generator.seed(seed);
}

//Private
unsigned Random :: getTimeBasedSeed() {
    return chrono::system_clock::now().time_since_epoch().count();
}
