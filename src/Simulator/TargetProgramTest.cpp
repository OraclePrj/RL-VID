#include "TargetProgram.hpp"


int main() {
   TargetProgram program("/home/scott/research/bop/src/Simulator/testcase");
   for (int action = 0; action < 11; action++) {
       for (int iteration = 0; iteration < 3; iteration++) {
           cout << "Action: " << action << ", ";
           program.doAction(action);
           cout << "Observation: " <<  program.getObservation() << endl;
       }
   }
}
