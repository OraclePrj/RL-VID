#include "RangeMap.hpp"

RangeMap::RangeMap() {
    actionCount = 0;
    space = NULL;
}

int RangeMap::getActionCount(vector<int>& states) {
    if (inputMap.count(states) == 1) {
        vector<int>& range = inputMap[states];
        if (range[0] == range[1]) return 1;
    }
    return 2; 
}

//TODO: Refactor this method into parts.
vector<int> RangeMap::getRange(vector<int>& states, int action) {
    if (action < 0 || actionCount <= action) {
        cerr << "Fatal error! Action out of range ";
        cerr << "(action:" << action << ")." << endl;
        exit(1);
    }
    if (inputMap.count(states) == 0) {
        //Initialize unseen state ranges to max possible
        vector<int> maxRange = {space->getLower(), space->getUpper()};
        inputMap[states] = maxRange;
    }
    vector<int>& totalInterval = inputMap[states];
    int absMin = totalInterval[0], absMax = totalInterval[1];
    int64_t overallLength = (int64_t)absMax - absMin + 1;

    bool tooSmall = overallLength < (int64_t)actionCount;
    int64_t numerator = tooSmall ? actionCount   : overallLength;
    int denominator   = tooSmall ? overallLength : actionCount;
    int binCount = denominator;

    int64_t bins[binCount] = {0};
    int64_t perBin = numerator / denominator;
    int leftover   = numerator % denominator;

    for (int binNo = 0; binNo < binCount; binNo++) {
        bins[binNo] = perBin;
        if (leftover > 0) {
            bins[binNo]++;
            leftover--;
        }
    }

    int minBound = absMin;
    int actionsSoFar = 0;
    for (int binNo = 0; binNo < binCount; binNo++) {
        if (!tooSmall && binNo < action) {
            minBound += bins[binNo];
        } else if (tooSmall && actionsSoFar <= action) {
            actionsSoFar += bins[binNo];
            if (actionsSoFar <= action) {
                minBound++;
            }
        } else {
            break;
        }
    }

    int length = tooSmall ? 1 : bins[action];
    int maxBound = minBound + length - 1;

    vector<int> range = {minBound, maxBound};
    return range;
}

void RangeMap::setActionCount(int actions) {
    actionCount = actions;
}

void RangeMap::setSpace(IntSpace& space) {
    this->space = &space;
}

void RangeMap::update(vector<Path>& bestPaths) {
    set<Path> paths = Path::getSubPaths(bestPaths);
    map<vector<int>, vector<int>> newMapping; 
    for (auto& path : paths) {
        Path copy = path;
        int action = copy.pop();
        vector<int> parents = copy.getStatePath();
        //if (parents.size() == 1) continue; //Root (launch) exempt TODO
        newMapping[parents] = getRange(parents, action);
    }

    inputMap.clear();
    for (auto& entry : newMapping) {
        inputMap[entry.first] = entry.second;
    }
}
