#ifndef _RANDOM_H_
    #define _RANDOM_H_

    #include <chrono>  
    #include <iostream> 
    #include <limits>  
    #include <random>   
    #include <sstream> 

    using namespace std;

    class Random {
        public:
            Random();
            Random(unsigned seed);
                
            static const string DIGITS, UPPER, LOWER, SPECIAL_NO_SPACE;
            static const string SPECIAL, ALPHANUMERIC, PRINTABLE;

            int integer();
            int integer(int low, int high);

            double real();
            double real(double low, double high);

            string nonNumericString(int len);
            string nonNumericString(int len, string charset);
            string numericString(int threshold, bool greaterEq);
            string numericString(int start, int end);
            void reseed(unsigned seed);
        private:
            mt19937 generator;

            unsigned getTimeBasedSeed();
    };

#endif
