#ifndef _PROCESS_HPP_
    #define _PROCESS_HPP_

    #include <iostream>
    #include <string>
    #include <sys/prctl.h>
    #include <vector>

    #include "Pipe.hpp"

    using namespace std;

    enum StreamMode { UNSET, STDIO, STDSTREAMS };

    class Process {
        public:
            Process(string filename);
           ~Process();
            void allowDebugger(int pid);
            vector<int> getChildPids();
            bool getCrashStatus();
            int getReturnValue();
            int getPid();
            void interrupt();
            bool isRunning();
            void launch();
            void launch(vector<string>& args);
            void sendInput(string input);
            void setStreamMode(StreamMode mode);
            void stop();
        protected:
            Pipe pipe;

            void interrupt(int pid);
        private:
            int childPid;
            int debugger;
            StreamMode streamMode;
            string filename;

            void synchronize();
    };

#endif
