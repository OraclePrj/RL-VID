#include "Simulator.hpp"
#include <iostream>

int main() {
    Simulator sim;
    for (int action = 0; action < 2; action++) {
        for (int iteration = 0; iteration < 20; iteration++) {
            cout << "Iteration " << iteration << ", Action " << action << endl;
            sim.perform(action);
            cout << "Current state: " << sim.current() << endl;
            if (sim.current() == 1 || sim.current() == 2) {
                sim.reset();
                cout << "Resetting sim." << endl;
            }
        }
    }    
}
