#include "Simulator.hpp"
Simulator :: Simulator(Config& config) : repr(config.getIRFilename()), 
                                         rewards(repr), 
                           program(config.getProgramFilename(), repr) {
    state = 0;
    states.push_back(state);
    actions.setActionCount(config.getActionCount());
    maxDepth = -1; 
}

int Simulator :: current() {
    return state;
}

/*
double Simulator :: getPercentOutofRange(double limit, int action) {
    //limit is the stack size. Find overlap of range with [-limit, limit].
    vector<int> range = actions.getInputRange(states, action);
    double minimum = range[0], maximum = range[1];
    bool lower = (-limit <= minimum) && (minimum <= limit);
    bool upper = (-limit <= maximum) && (maximum <= limit);

    if (upper && lower) {
        // [-limit, limit] contains entire range.
        return 0.0;
    } else if (!upper && !lower) {
        // Range is entirely outside of [-limit, limit].
        return 1.0;
    } else if (lower) { //Lower end of range is truncated.
        minimum = limit;
    } else { //Upper end of range is truncated.
        maximum = -limit;
    }
    return (maximum - minimum + 1) / (range[1] - range[0] + 1);
}
*/

TargetProgram& Simulator::getProgram() {
    return program;
}

vector<double> Simulator :: getRewards() {
    return collected;
}

int Simulator :: perform(int action) {
    string input;
    if (state == 0) {
        //If in state 0, program unlaunched, so launch with arguments.
        vector<string> args; // TEMP-DISABLE: actions.getLaunchArgs(action);
        //vector<string> args = actions.getLaunchArgs(action);
        program.launch(args);
        //program.sendInput("2+2"); //NEW (To ensure doesn't block for input)
    } else if (states.size() <= maxDepth) {
        //Otherwise must be at an input
        cerr << "Action: " << action << endl;
        input = actions.getInput(states, action);
        program.sendInput(input);   
    }

    state = (states.size() > maxDepth) ? 2 : program.getObservation();
    vector<int> visits = program.getBlockVisits();
    collected.push_back(rewards.value(visits));
    states.push_back(state);
    return state;
}

string Simulator :: printAction(vector<int> states, int action) {
    return actions.printAction(states, action);     
}

void Simulator :: setDebugger(int pid) {
    program.allowDebugger(pid);
}

void Simulator :: setMaxDepth(int maxDepth) {
    this->maxDepth = maxDepth;
}

void Simulator :: setPolicy(Policy& policy) {
    actions.setSpace(policy.getSpace());
}

double Simulator :: simulate(MCT_StateNode* node, Policy& policy) {
    int state = node->getId();
    bool firstState = true;
    double result = 0.0;
    double discount = 1.0;
    double blockVisitsReward = collected[collected.size() - 1];
    do {
        if (!firstState) {        
            int action = policy.rollout(state);
            state = perform(action);
            blockVisitsReward = collected[collected.size() - 1];
            collected.pop_back();
            states.pop_back();
        } else {
            //For first state, already loaded blockVisitsReward.
            firstState = false;
        }

        double subtotal = rewards.getStateReward(state);
        if (!rewards.isTerminal(state)) {
            //Ignore block visits for terminal states.
            subtotal += blockVisitsReward;
        }
    
        result += discount * subtotal;
        discount *= 0.95;
    } while (!rewards.isTerminal(state));

    return result;
}


/*
double Simulator :: simulate(MCT_StateNode* node) {
    int state = node->getId();
    double stateReward = rewards.getStateReward(state);
    if (rewards.isTerminal(state)) {
        //If terminal, we know actual value: ignore simulated value.
        return stateReward;
    } else {
        unsigned lastIndex = collected.size() - 1;
        //Simulated value is state reward + last immediate reward rec'v'd.
        return collected[lastIndex] + stateReward;
    }
}
*/

int Simulator :: startup() {
    //Launches program and confirms program running. If not, raises exception
    int state = perform(0);
    if (rewards.isTerminal(state)) {
        cerr << "Target Program ended on launch" << endl;
        exit(1);
    }
    return state;
}

void Simulator :: update(vector<Path>& bestPaths) {
    actions.update(bestPaths);
}

void Simulator :: reset() {
    collected.clear();
    states.clear();
    state = 0;
    states.push_back(state);
    program.reset();
}
