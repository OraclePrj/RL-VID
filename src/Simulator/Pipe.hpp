#ifndef _PIPE_HPP_
    #define _PIPE_HPP_
    #include <cstring>
    #include <fcntl.h>
    #include <iostream>
    #include <sstream>
    #include <string>
    #include <sys/wait.h>
    #include <unistd.h>

    using namespace std;

    enum PipeOwner {UNKNOWN, PARENT, CHILD, CLOSED };
    enum PipeMode { TO_CHILD, FROM_CHILD, BIDIRECTIONAL };

    class Pipe {
        public:
            Pipe(PipeMode mode);
           ~Pipe();

            void connectToStdIO();
            void connectToStdStreams();
            //void flushWritten();
            string readChars(int length);
            string readLine();
            //string readUntilBlock();
            void reset();
            void setOwner(PipeOwner owner);
            void start(PipeMode mode);
            void stop();
            void writeChar(char letter);
            void writeLine(string line);
        private:
            PipeOwner owner;
            PipeMode mode;
            FILE* readEnd;
            FILE* writeEnd;
            int toChildPipe[2];
            int fromChildPipe[2];

            void setReadBlocking(bool shouldBlock);
            void setWriteBlocking(bool shouldBlock);
    };

#endif
