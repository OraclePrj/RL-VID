#include "Pipe.hpp"

Pipe::Pipe(PipeMode mode) {
    //cout << "Loading Pipe" << endl;
    owner = UNKNOWN;
    this->start(mode);
    //cout << "Pipe loaded" << endl;
}

Pipe::~Pipe() {
    if (owner != CLOSED) {
        this->stop();
    }
}

void Pipe::connectToStdIO() {
    if (this->owner != CHILD) {
        throw invalid_argument("connectToStdIO attempted as non-child");
    }
    
    //Hook up stdin to parent read, and stdout to child write.
    dup2(fileno(readEnd), STDIN_FILENO);
    dup2(fileno(writeEnd), STDOUT_FILENO);
}

void Pipe::connectToStdStreams() {
    if (this->owner != CHILD) {
        throw invalid_argument("connectToStdIO attempted as non-child");
    }
    
    //Hook up stdin to parent read, and stdout/stderr to child write.
    dup2(fileno(readEnd), STDIN_FILENO);
    dup2(fileno(writeEnd), STDOUT_FILENO);
    dup2(fileno(writeEnd), STDERR_FILENO);
}

//void Pipe::flushWritten() {
//    fflush(writeEnd);
//}

string Pipe::readChars(int length) {
    if (length <= 0) {
        throw invalid_argument("readChars called with invalid length");
    }
    char buffer[length + 1];
    memset(buffer, '\0', length + 1);
    int bytesRead = read(fileno(readEnd), buffer, length);
    if (bytesRead >= 0) {
        return string(buffer);
    } else {
        //Error occurred.
        throw invalid_argument("An error occurred during fd read");
    }
}

string Pipe::readLine() {
    if (owner != PARENT && owner != CHILD) {
        throw invalid_argument("readLine called in incorrect mode");
    }
    ostringstream result;
    while (true) {
        int letter = fgetc(readEnd);
        if (letter == EOF || letter == '\n') {
            break;
        }
        result << (char)letter;
    }
    return result.str();
}

/*
string Pipe::readUntilBlock() {
    setReadBlocking(false); //TODO: have accept fileno as arg, so only called 1x
    int fd = fileno(readEnd);
    int bufferLength = 80;
    char buffer[bufferLength + 1];
    ostringstream result;
    while (true) {
        memset(buffer, '\0', bufferLength + 1);
        ssize_t bytesRead = read(fd, buffer, bufferLength);
        if (bytesRead == -1) {
            //An error occurred, OR would have blocked.
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                break; //Stop condition
            } else {
                throw invalid_argument("Error reading from file descriptor");
            }
        } else if (bytesRead == 0) {
            break; //End of file.
        }
        result << string(buffer);
    }
    setReadBlocking(true);
    return result.str();
}
*/

void Pipe::setOwner(PipeOwner owner) {
    if (this->owner != UNKNOWN || owner == UNKNOWN) {
        throw invalid_argument("invalid setOwner call");
    }

    //TODO: Implement non-bidirectional modes.
    if (owner == PARENT) {
        //Parent setup. Close ends that child will use
        close(toChildPipe[0]);
        close(fromChildPipe[1]);
        
        //Configure own read/write ends.
        readEnd = fdopen(fromChildPipe[0], "r");
        writeEnd = fdopen(toChildPipe[1], "w");
    } else if (owner == CHILD) {
        //Child setup. Close ends that parent will use
        close(toChildPipe[1]);
        close(fromChildPipe[0]);

        //Configure own read/write ends.
        readEnd = fdopen(toChildPipe[0], "r");
        writeEnd = fdopen(fromChildPipe[1], "w");
    }

    this->owner = owner;
}

void Pipe::setReadBlocking(bool shouldBlock) {
    int fd = fileno(readEnd);
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1) {
        throw invalid_argument("Can't read file descriptor flags");
    }

    if (shouldBlock) {
        //Clear nonblocking bit, leaving old flags unchanged.
        flags &= ~O_NONBLOCK;
    } else {
        //Set nonblocking bit, leaving old flags unchanged.
        flags |= O_NONBLOCK;
    }
    
    if (fcntl(fileno(readEnd), F_SETFL, flags) == -1) {
        throw invalid_argument("Can't write file descriptor flags");
    }
}

void Pipe::setWriteBlocking(bool shouldBlock) {
    //TODO: Write later.
}

void Pipe::start(PipeMode mode) {
    if (owner == PARENT || owner == CHILD) {
        this->stop();
    }
    this->owner = UNKNOWN;
    this->mode = mode;
    readEnd = writeEnd = NULL;

    //Create pipes, sensing for errors.
    if (pipe(toChildPipe) == -1) {
        int errorCode = errno;
        cerr << "Could not create pipe, errno = " << errorCode << endl;
        exit(1);
    }

    if (pipe(fromChildPipe) == -1) {
        int errorCode = errno;
        cerr << "Could not create pipe, errno = " << errorCode << endl;
        exit(1);
    }
}

void Pipe::stop() {
    if (owner == PARENT || owner == CHILD) {
        fclose(readEnd);
        fclose(writeEnd);
    }

    this->owner = CLOSED;
}

void Pipe::writeChar(char letter) {
    fputc(letter, writeEnd);
    fflush(writeEnd);
}

void Pipe::writeLine(string line) {
    if (owner != PARENT && owner != CHILD) {
        throw invalid_argument("writeLine called in incorrect mode");
    }
    fputs(line.c_str(), writeEnd);
    //fputc('\n', writeEnd);
    fflush(writeEnd);
}
