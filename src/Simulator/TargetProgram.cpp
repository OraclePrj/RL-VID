#include "TargetProgram.hpp"
 
TargetProgram :: TargetProgram(string filename, Representation& repr) 
                                                         : Process(filename),
                                 shmem("__AFL_SHM_ID", repr.getBlockCount()) {
    //cout << "Loading TargetProgram" << endl;
    this->state = 0;
    this->setStreamMode(STDIO);
    //cout << "TargetProgram loaded" << endl;
}

vector<int> TargetProgram :: getBlockVisits() {
    vector<int> result = shmem.asVector();
    shmem.clear();
    return result;
}

int TargetProgram :: getObservation() {
    if (state == 1 || state == 2) {
        //Child unstarted (Waiting on input)
        state = 0;
    } else {
        string currentState = pipe.readChars(65535);
        if (currentState.empty()) {
            //Child terminated in state 1 or 2
            bool crashed = getCrashStatus();
        
            //Debug output
            if (crashed) {
                cerr << "Crashed." << endl;
            } else {
                cerr << "Exited normally." << endl;
            }
            
            state = crashed ? 1 : 2;
        } else {
            cerr << "Received: |" << currentState << "|" << endl; //NEW
            state = (state == 0) ? 3 : (state + 1); //OLD: stoi(currentState);
        }
    }
    return state;
}

void TargetProgram :: reset() {
    state = 0;
}
