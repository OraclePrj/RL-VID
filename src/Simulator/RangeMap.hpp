#ifndef _RANGEMAP_HPP_
    #define _RANGEMAP_HPP_
    #include <map>
    #include <set>
    #include <sstream>
    #include <stdexcept>
    #include <string>
    #include <vector>

    #include "../Tree/Path.hpp"
    #include "../QBASE/IntSpace.hpp"

    using namespace std;

    class RangeMap {
        public:
            RangeMap();
            int getActionCount(vector<int>& states);
            vector<int> getRange(vector<int>& states, int action);
            void setActionCount(int actions);
            void setSpace(IntSpace& space);
            void setInputRange(int state, vector<int>& range);
            void update(vector<Path>& bestPaths);
        private:
            IntSpace* space;
            int actionCount;
            map<vector<int>, vector<int>> inputMap;
    };
#endif
