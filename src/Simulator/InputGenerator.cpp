#include "InputGenerator.hpp"

InputGenerator::InputGenerator() {
    actionCount = 0;
    space = NULL;
}


int InputGenerator::getActionCount(vector<int>& states) {
    return ranges.getActionCount(states);
}

string InputGenerator::printAction(vector<int>& states, int action) {
    ostringstream output;
    if (space->getGranularity() == DISCRETE) {
        output << "[" << action << ", " << action << "]";
    } else { // if (space->getGranularity() == CONTINUOUS) {
        output << ranges.getRange(states, action);
    }
    return output.str();
}

string InputGenerator::getInput(vector<int>& states, int action) {
    ostringstream stringMaker;
    if (space->getGranularity() == DISCRETE) {
        stringMaker << action;
    } else { // if (space->getGranularity() == CONTINUOUS) {
        vector<int> range = ranges.getRange(states, action);
        cerr << range << endl;
        stringMaker << random.integer(range[0], range[1]);
    }
    /*
        stringMaker << "define doom(";
        for (int count = 0; count < chosenInput; count++) {
            if (count > 0) stringMaker << ",";
            stringMaker << "x" << count;
        }
        stringMaker << ") {\n  return (0)\n}\n2+2\n";
    */
    return stringMaker.str();
}

void InputGenerator::setActionCount(int actions) {
    actionCount = actions;
    ranges.setActionCount(actions);
}

void InputGenerator::setSpace(IntSpace& space) {
    this->space = &space;
    if (space.getGranularity() == CONTINUOUS) {
        ranges.setSpace(space);
    }
}

void InputGenerator::update(vector<Path>& bestPaths) {
    if (space->getGranularity() == CONTINUOUS) {
        ranges.update(bestPaths);
    }
}
