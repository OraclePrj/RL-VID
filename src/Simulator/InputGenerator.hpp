#ifndef _INPUTGENERATOR_HPP_
    #define _INPUTGENERATOR_HPP_
    #include <map>
    #include <set>
    #include <sstream>
    #include <stdexcept>
    #include <string>
    #include <vector>

    #include "../QBASE/IntSpace.hpp"
    #include "../Tree/Path.hpp"
    #include "Random.hpp"
    #include "RangeMap.hpp"
    #include "../Utils/StrOps.hpp"

    using namespace std;

    class InputGenerator {
            public:
                InputGenerator();
                int getActionCount(vector<int>& states);
                string getInput(vector<int>& states, int action);
                //vector<int> getInputRange(vector<int> states, int action);
                string printAction(vector<int>& states, int action);
                void setActionCount(int actions);
                void setSpace(IntSpace& space);
                //void setInputRange(int state, vector<int>& range);
                void update(vector<Path>& bestPaths);
            private:
                IntSpace* space;
                int actionCount;
                Random random; 
                RangeMap ranges;
    };
#endif
