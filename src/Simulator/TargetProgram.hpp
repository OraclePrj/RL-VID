#ifndef _TARGETPROGRAM_H_
    #define _TARGETPROGRAM_H_

    #include <iostream>
    #include <string>
    #include <signal.h>   
    #include <sstream>
    #include <string.h> 
    #include <sys/wait.h> 
    #include <unistd.h> 
    #include <vector>

    #include "Process.hpp"
    #include "../StateGraph/Representation.hpp"
    #include "../StateGraph/SharedMemory.hpp"

    using namespace PRGM;
    using namespace std;

    class TargetProgram : public Process {
        public:
            TargetProgram(string filename, Representation& repr);

            vector<int> getBlockVisits();
            int getObservation();
            void reset();
        private:
            SharedMemory shmem;
            int state;

    };
#endif
