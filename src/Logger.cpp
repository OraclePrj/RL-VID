#include "Logger.hpp"

Logger :: Logger(string filename, Simulator& sim) {
    //Create new blank file and open for writing
    logFile.open(filename, fstream::out|fstream::trunc);
    step = 0;
    this->sim = &sim;
}

Logger :: ~Logger() {
    logFile.close();
}

void Logger :: log(MCT_Tree& tree) {
    logFile << "Step " << step++ << endl;
    Path emptyPath;
    //TODO: This should be MCT_Tree.toString()...
    recursive_write(emptyPath, tree.getRoot());

    logFile << "Best Paths:" << endl;
    vector<Path> bestPaths = tree.getBestPaths();
    for (Path& path : bestPaths) {
        logFile << path.toString() << endl;
    }
}

void Logger::recursive_write(Path parent, MCT_Node* current) {
    //Assemble path to node (comma separated)
    Path path = parent;
    path.append(current);

    //Print parent path
    logFile << path.toString() << ';';
    
    //Print range if action
    MCT_ActionNode* actionNode = dynamic_cast<MCT_ActionNode*>(&*current);
    int action = current->getId();
    if (actionNode != nullptr) {
        vector<int> statePath = path.getStatePath();
        logFile << sim->printAction(statePath, action) << ';';
    }

    //Print statistics
    logFile << current->getVisits() << ';';
    logFile << current->value() << endl;

    //Repeat for children with this node prepended
    for (auto& entry : current->getChildren()) {
        MCT_Node* next = entry.second;
        recursive_write(path, next);
    }
}
