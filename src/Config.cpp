#include "Config.hpp"

//Always pair QBASE/DISCRETE and UCB/CONTINUOUS for now.
//Other combinations not currently supported.
Config::Config() : actionSpace(DISCRETE, 0, 65535) {
//Config::Config() : actionSpace(CONTINUOUS, 0, 65535) {
    policyMode = QBASE_MODE; //UCB_MODE; //QBASE_MODE
    actionCount = 2;

    irFilename = "./testcase.bc";
    prgmFilename = "./testcase";
}

int Config::getActionCount() {
    return actionCount;
}

IntSpace& Config::getActionSpace() {
    return actionSpace;
}

string Config::getIRFilename() {
    return irFilename;
}

PolicyMode Config::getPolicyMode() {
    return policyMode;
}

string Config::getProgramFilename() {
    return prgmFilename;
}
