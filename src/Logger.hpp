#ifndef _LOGGER_HPP_
    #define _LOGGER_HPP_
    #include <fstream>
    #include <iostream>
    #include <memory>
    #include <string>
    #include "Simulator/Simulator.hpp"
    #include "Tree/MCT_Node.hpp"
    #include "Tree/MCT_Tree.hpp"
    #include "Tree/Path.hpp"

    using namespace std;

    class Logger {
        public:
            Logger(string filenamei, Simulator& sim);
           ~Logger();
            void log(MCT_Tree& tree);
        private:
            Simulator *sim;
            fstream logFile;
            int step;
            void recursive_write(Path path, MCT_Node* current);
    };

#endif
