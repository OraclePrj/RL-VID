MODE = "graphical"
#MODE = "text"

if MODE == "graphical":
    from Visualizer.GraphVisualizer import GraphVisualizer, NoStepException
    visualizer = GraphVisualizer("log.log")
    first, last = visualizer.get_range()
    while True:
        step_no = input("Enter step to view ({}-{}): ".format(first, last))
        if step_no == "":
            break    
        try:
            visualizer.view(int(step_no))
        except ValueError:
            print("Non-integer step number: " + step_no)
        except NoStepException:
            print("Step number not in log file: " + step_no)

elif MODE == "text":
    from Visualizer.Model import Model
    from Visualizer.View import View
    from Visualizer.Controller import Controller
    
    model = Model()
    view = View(model)
    controller = Controller(model, view)
