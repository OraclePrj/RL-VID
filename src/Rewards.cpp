#include "Rewards.hpp"

Rewards :: Rewards(Representation& repr) {
    DowserScores dowser(repr);
    unsigned blockCount = repr.getBlockCount();
    rewards.resize(blockCount);
    discounts.resize(blockCount);
    vector<double> dowserScores = dowser.getBlockScores();
    if (dowserScores.size() != blockCount) {
        cerr << "Mismatch between DowserScores and Representation." << endl;
        cerr << "Dowser scores: " << dowserScores.size() << endl;
        cerr << dowserScores << endl;
        cerr << "Representation: " << blockCount;
        exit(1);
    }
    for (unsigned index = 0; index < blockCount; index++) {
        rewards[index] = dowserScores[index];
        discounts[index] = 1.0;
    }

/*
    //Iterate over representation in order, analyzing basic blocks as you go.
    //cout << "Loading Rewards" << endl;
    for (auto currFn = repr.begin(); currFn != repr.end(); ++currFn) {
        Function function(currFn);
        for (auto currB = function.begin(); currB != function.end(); ++currB) {
            Block block(currB);
            analyze(block);
        }
    }  
    //cout << "Rewards loaded" << endl;
*/
}

void Rewards :: analyze(Block& block) {
    //Use heuristic on block to assign a value.
    double value = 0.0;
    for (auto inst = block.begin(); inst != block.end(); ++inst) {
        Instruction instruction(inst);
        //cout << instruction << endl;
        if (instruction.isDangerous()) {
            value += 1;
            //cout << instruction << endl;
        } else if (instruction.isFnCall()) {
            //cout << instruction << endl;
        }
    }

    //Store value in order for this block.
    rewards.push_back(value);
}

bool Rewards::isTerminal(int state) {
    return (state == 1) || (state == 2);
}

double Rewards::getStateReward(int state) {
    if (state == 1) {
        return 10000;
    } else if (state == 2) {
        return -10000;
    } else {
        return 0;
    }
}

double Rewards::value(vector<int>& visits) {
    double value = 0.0;
    unsigned blockCount = visits.size();
    for (unsigned index = 0; index < blockCount; index++) {
        if (visits[index] > 0) {
            double discount = 0 * discounts[index]; //TODO: Remove 0 *
            value += discount * rewards[index];
            discounts[index] = 0.95 * discount; //discount / 2.0;
        }
    }
    //cout << value << endl;
    return value;
}

string Rewards::toString() const {
    ostringstream buffer;
    buffer << rewards;
    return buffer.str();
}
