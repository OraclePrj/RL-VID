#include "MCT_Tree.hpp"

//TODO: Need toString() that captures action ranges as well

MCT_Tree::MCT_Tree() : root(new MCT_StateNode(0)) {}

void MCT_Tree::clear() {
    root = shared_ptr<MCT_Node>(new MCT_StateNode(0));
}

shared_ptr<MCT_Node> MCT_Tree::getRoot() {
    return root;
}

vector<Path> MCT_Tree::getBestPaths() {
    return getBestPaths(root);
}

vector<MCT_Node*> MCT_Tree::getNodes(Path& path) {
    vector<MCT_Node*> result;
    shared_ptr<MCT_Node> current = root;
    for (unsigned index = 0; index < path.size(); index++) {
        if (index > 0) {
            current = current->getChild(path[index]);
        }
        result.push_back(&*current);
    }
    return result;
}

vector<Path> MCT_Tree::getBestPaths(shared_ptr<MCT_Node> node) {
    vector<Path> paths;
    MCT_StateNode* state = dynamic_cast<MCT_StateNode*>(&*node);
    shared_ptr<MCT_Node> bestAction = state->getChild(state->getBestAction());
    int action = bestAction->getId();
    for (auto& entry : bestAction->getChildren()) {
        shared_ptr<MCT_Node> child = entry.second;
        int childState = child->getId();
        if (childState != 1 && childState != 2) { //TODO: Generalize
            for (auto& childPath : getBestPaths(child)) {
                Path path;
                path.appendState(state->getId());
                path.appendAction(action);
                path.extend(childPath);
                paths.push_back(path);
            }
        }
    }
    if (paths.size() == 0) {
        Path path;
        path.appendState(state->getId());
        path.appendAction(action);
        paths.push_back(path);
    }

    return paths;
}

/*  Maybe incorp later? Used to be in MCTS.
void MCTS::analyze(map<string, MCT_Node*>& actions) {
    probs.clear();
    for (auto& element : actions) {
        //Find probability of crashing for this mapping.
        MCT_Node* action = element.second;
        map<int, shared_ptr<MCT_Node>>& children = action->getChildren();
        double prob = 0;
        if (children.count(1) == 1) {
            //At least one crash TODO: Make this more general
            shared_ptr<MCT_Node> child = action->getChild(1);
            prob += child->getVisits();
            prob /= action->getVisits();
        }
        probs[element.first] = prob;
    }    
}

map<string, double>& MCTS::getProbs() {
    map<string, MCT_Node*> actions;
    tree.getTerminalActions(actions);
    analyze(actions);
    return probs;
}
*/

set<Path> MCT_Tree::getTerminalActions() {
    Path parent;
    return getTerminalActions(root, parent);
}

set<Path> MCT_Tree::getTerminalActions(shared_ptr<MCT_Node>& start, 
                                                          Path parent) {
    set<Path> result;
    //Do DFS to identify terminal nodes, and when identified, append parent
    parent.append(start->getParent());
    int thisId = start->getId();
    if (start->isLeaf() && (thisId == 1 || thisId == 2)) {
        //Terminal. Append parent to list and stop. TODO: Make general
        result.insert(parent);
    } else {
        //Non-terminal: recursively call on children.
        for (auto& entry : start->getChildren()) {
            shared_ptr<MCT_Node> next = entry.second;
            for (Path path : getTerminalActions(next, parent)) {
                Path copy = parent;
                copy.extend(path);
                result.insert(copy);
            }
        }
    }
    return result;
} 
