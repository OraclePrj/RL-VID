#include "MCT_Node.hpp"

#include <iostream>
using namespace std;

int main() {
    MCT_Node node1(1);
    MCT_Node node2(node1, 2);
    cout << node1.toString() << endl;
    cout << node2.toString() << endl;
}
