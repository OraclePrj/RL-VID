#ifndef _MCT_ACTIONNODE_HPP_
    #define _MCT_ACTIONNODE_HPP_
    #include <iostream>
    #include <limits>
    #include <memory>
    #include "MCT_Node.hpp"
    #include "MCT_StateNode.hpp"

    using namespace std;

    class MCT_ActionNode : public MCT_Node {
        public:
            MCT_ActionNode(int id);
            MCT_ActionNode(MCT_Node* parent, int id);
            void addChild(int id);
            string getLabel();
            MCT_NodeType getType();
            string toString();
            void update(MCT_Node* child, double value);
            double value();
        private:
            double cumValue;
    };

#endif
