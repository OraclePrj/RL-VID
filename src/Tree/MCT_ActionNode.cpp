#include "MCT_ActionNode.hpp"

MCT_ActionNode::MCT_ActionNode(int id) : MCT_Node(id) {
    cumValue = 0;
}

MCT_ActionNode::MCT_ActionNode(MCT_Node* parent, int id): MCT_Node(parent, id) {
    cumValue = 0;
}

MCT_ActionNode::~MCT_ActionNode() {
    for (auto& entry : children) {
        delete entry.second; //Delete the children
    }
}

void MCT_ActionNode::addChild(int id) {
    children[id] = new MCT_StateNode(this, id);
}

string MCT_ActionNode::getLabel() { 
    ostringstream result;
    result << 'a' << id;
    return result.str();
}

MCT_NodeType MCT_ActionNode::getType() {
    return MCT_NodeType::ACTION;
}

string MCT_ActionNode :: toString() {
    ostringstream result;
    result << "ActionNode (Visits: " << visits << ")" << endl;
    result << "Path: " << getPath() << endl;
    result << "CumValue: " << cumValue << ", Average: " << value();
    return result.str();
}

void MCT_ActionNode :: update(double reward, MCT_Node* child) {
    //Use for Bellman Backup
    cumValue += reward;

    //Use Monte Carlo Backup TODO: take out hard-coded discount
    //cumValue += reward + 0.95 * child->value();

    //Always use
    visits += 1;
}

double MCT_ActionNode :: value() {
    //Use for Bellman Backup
    //Keeps State value estimates optimal with time.
    double subtotal = 0;
    double totalWeight = 0;
    for (auto& entry : children) {
        MCT_Node* child = entry.second;
        int weight = child->getVisits();
        subtotal += weight * child->value();
        totalWeight += weight;
    }
    double immediateReward = cumValue / visits;
    double longTerm = 0.95 * (subtotal/totalWeight);
    //TODO: Take out hard-coded discount factor

    return immediateReward + longTerm;    

    //Use for Monte Carlo Backup
    //return cumValue / visits;
}
