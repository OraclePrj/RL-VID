#ifndef _PATH_HPP_
    #define _PATH_HPP_

    #include <memory>
    #include <set>
    #include <vector>

    #include "MCT_Node.hpp"
    #include "MCT_StateNode.hpp"
    #include "MCT_ActionNode.hpp"

    using namespace std;

    class Path {
        public:
            void append(shared_ptr<MCT_Node> node);
            void append(MCT_Node* node);
            void append(MCT_StateNode* state);
            void append(MCT_ActionNode* action);
            void appendAction(int id);
            void appendState(int id);
            vector<int> asList() const;
            void extend(const Path& other);
            friend bool operator<(const Path& p1, const Path& p2);
	    int operator[](int index) const;
            static set<Path> getSubPaths(vector<Path>& paths);
            vector<int> getStatePath();
            bool isAction(int index) const;
            bool isState(int index) const;
            int pop();
            unsigned size() const;
            string toString() const;
        private:
            vector<int> ids;
            vector<int> state;
    };

#endif
