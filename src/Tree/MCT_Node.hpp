#ifndef _MCT_NODE_HPP_
#define _MCT_NODE_HPP_
    #include <map>
    #include <sstream>
    #include <string>

    using namespace std;

    enum MCT_NodeType {STATE, ACTION};

    class MCT_Node {
        public:
            MCT_Node(int id);
            MCT_Node(MCT_Node* parent, int id);
            virtual ~MCT_Node();

            MCT_Node* getChild(int id);
            MCT_Node* getParent();
            map<int, MCT_Node*>& getChildren();
            int getId() const;
            virtual string getLabel() = 0;
            string getPath();
            virtual MCT_NodeType getType() = 0;
            int getVisits();
            bool isLeaf();
            void recordVisit();
            virtual string toString() = 0;
            virtual double value() = 0;
        protected:
            int id, visits;
            MCT_Node* parent;
            map<int, MCT_Node*> children;

            void initialize(MCT_Node* parent, int id);

            virtual void addChild(int id) = 0;
            string getPath(bool highlight);
    };

#endif
