#include "MCT_ActionNode.hpp"

MCT_ActionNode::MCT_ActionNode(int id) : MCT_Node(id) {
    cumValue = 0;
}

MCT_ActionNode::MCT_ActionNode(MCT_Node* parent, int id): MCT_Node(parent, id) {
    cumValue = 0;
}

void MCT_ActionNode::addChild(int id) {
    shared_ptr<MCT_Node> child(new MCT_StateNode(this, id));
    children[id] = child;
}

string MCT_ActionNode::getLabel() { 
    ostringstream result;
    result << 'a' << id;
    return result.str();
}

MCT_NodeType MCT_ActionNode::getType() {
    return MCT_NodeType::ACTION;
}

string MCT_ActionNode :: toString() {
    ostringstream result;
    result << "ActionNode (Visits: " << visits << ")" << endl;
    result << "Path: " << getPath() << endl;
    result << "CumValue: " << cumValue << ", Average: " << value();
    return result.str();
}

void MCT_ActionNode :: update(MCT_Node* child, double value) {
    cumValue += value;
    visits += 1;
    if (parent) {
        parent->update(this, this->value());
    }
}

double MCT_ActionNode :: value() {
    return cumValue / visits;
}
