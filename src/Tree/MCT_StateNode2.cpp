#include "MCT_StateNode.hpp"

MCT_StateNode::MCT_StateNode(int id) : MCT_Node(id) {
    visits = 0;
    reset_bests();
}

MCT_StateNode::MCT_StateNode(MCT_Node* parent, int id) : MCT_Node(parent, id) {
    reset_bests();
}

void MCT_StateNode::addChild(int id) {
    shared_ptr<MCT_Node> child(new MCT_ActionNode(this, id));
    children[id] = child;
}

void MCT_StateNode::assign(double value) {
    visits += 1;
    reset_bests();
    if (parent) {
        parent->update(this, value);
    }
}

int MCT_StateNode :: getBestAction() {
    return (bestChild == nullptr) ? -1 : bestChild->getId();
}

string MCT_StateNode :: getLabel() { 
    ostringstream result;
    result << 's' << id;
    return result.str();
}

MCT_NodeType MCT_StateNode::getType() {
    return MCT_NodeType::STATE;
}

bool MCT_StateNode :: isTerminal() {
    return id == 1 || id == 2;
}

void MCT_StateNode :: reset_bests() {
    bestChild = nullptr;
    bestValue = -numeric_limits<double>::infinity();
}

string MCT_StateNode :: toString() {
    ostringstream result;
    result << "StateNode (Visits: " << visits << ")" << endl;
    result << "Path: " << getPath() << endl;
    result << "Action values: ";
    int bestIndex = -1, count = 0;
    for (auto& entry : children) {
        if (count > 0) result << ", ";
        shared_ptr<MCT_Node> child = entry.second;
        result << child->value();
        if (bestChild == &(*child)) {
            bestIndex = count;
        }
        count++;
    }
    if (count == 0) result << "(None)";
    result << endl;
    result <<  "BestAction: " << bestIndex; 
    result << ", BestValue: " << bestValue;
    return result.str();
}

void MCT_StateNode :: update(MCT_Node* child, double value) {
    if (value > bestValue) {
        //If newValue is better than bestValue, replace both.
        bestValue = value;
        bestChild = child;
    } else if (bestChild == child && value < bestValue) {
        //bestAction just got worse, need to recheck for new best.
        reset_bests();
        for (auto& entry : children) {
            shared_ptr<MCT_Node> action = entry.second;
            double childValue = action->value();
            if (childValue > bestValue) {
                bestValue = childValue;
                bestChild = &(*action);
            }
        }
    }

    visits += 1;
    
    if (parent) {
        parent->update(this, this->value());
    }
}

double MCT_StateNode :: value() {
    if (bestChild) {
        return bestValue;
    } else if (id == 1 || id == 2) {
        return (id == 1) ? 1000 : -1000;
    } else {
        return 0;
    }
}
