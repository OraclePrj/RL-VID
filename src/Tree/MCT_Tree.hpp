#ifndef _MCT_TREE_HPP_
    #define _MCT_TREE_HPP_

    #include <map>
    #include <string>
    #include <vector>
    #include "MCT_Node.hpp"
    #include "MCT_StateNode.hpp"
    #include "MCT_ActionNode.hpp"
    #include "Path.hpp"

    using namespace std;

    class MCT_Tree {
        public:
            MCT_Tree();
            void clear();
            MCT_Node* getRoot();
            vector<Path> getBestPaths();
            vector<Path> getBestPaths(MCT_Node* node);
            vector<MCT_Node*> getNodes(Path& path);
            set<Path> getTerminalActions();
            set<Path> getTerminalActions(MCT_Node* start, Path parent);
        private:
            MCT_Node* root;
    };


#endif
