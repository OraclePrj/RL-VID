#include "MCT_StateNode.hpp"

#include <iostream>
#include <memory>
using namespace std;

int main() {
    MCT_StateNode node1(1);
    shared_ptr<MCT_Node> node2 = node1.getChild(2);
    shared_ptr<MCT_Node> node3 = node2->getChild(3);
    cout << node1.toString() << endl << endl;
    cout << node2->toString() << endl << endl;
    cout << node3->toString() << endl << endl;
    cout << "=======================" << endl;

    MCT_StateNode* stateNode3 = dynamic_cast<MCT_StateNode*>(&*node3);
    if (stateNode3) {
        stateNode3->assign(42);
    } else {
        cout << "Dynamic cast failed!" << endl;
    }
    cout << node1.toString() << endl << endl;
    cout << node2->toString() << endl << endl;
    cout << node3->toString() << endl;
}
