#include "MCT_ActionNode.hpp"

#include <iostream>
#include <memory>
using namespace std;

int main() {
    MCT_ActionNode node1(1);
    shared_ptr<MCT_Node> node2 = node1.getChild(2);
    shared_ptr<MCT_Node> node3 = node2->getChild(3);
    shared_ptr<MCT_Node> node4 = node3->getChild(4);
    cout << node1.toString() << endl << endl;
    cout << node2->toString() << endl << endl;
    cout << node3->toString() << endl << endl;
    cout << node4->toString() << endl << endl;

    cout << "======================" << endl;
    MCT_StateNode* state = dynamic_cast<MCT_StateNode*>(&*node4);
    if (state) {
        state->assign(42);
    } else {
        cout << "Failed cast!" << endl;
    }
    
    cout << node1.toString() << endl << endl;
    cout << node2->toString() << endl << endl;
    cout << node3->toString() << endl << endl;
    cout << node4->toString() << endl << endl;

}
