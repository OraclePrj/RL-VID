#include "MCT_Node.hpp"

MCT_Node :: MCT_Node(int id) { initialize(nullptr, id); }

MCT_Node :: MCT_Node(MCT_Node* parent, int id) { initialize(parent, id); }

MCT_Node :: ~MCT_Node() {
    //Zero all children's parent field so no dangling ptrs
    for (auto& entry : children) {
        shared_ptr<MCT_Node> child = entry.second;
        child->parent = nullptr;
    }
}

shared_ptr<MCT_Node> MCT_Node :: getChild(int id) {
    if (children.count(id) == 0) {
        this->addChild(id);
    }

    return children[id];
}

MCT_Node* MCT_Node :: getParent() {
    return parent;
}

map<int, shared_ptr<MCT_Node>>& MCT_Node :: getChildren() {
    return children;
}

int MCT_Node :: getId() {
    return id;
}

string MCT_Node :: getPath() {
    return getPath(true);
}

string MCT_Node :: getPath(bool highlight) {
    ostringstream result;
    if (parent) {
        result << parent->getPath(false) << '-';
    }

    if (highlight) {
        result << '[';
    }

    result << getLabel();

    if (highlight) {
        result << ']';
    }

    return result.str();
}

int MCT_Node :: getVisits() {
    return visits;
}

void MCT_Node :: initialize(MCT_Node* parent, int id) {
    this->parent = parent;
    this->id = id;
    this->visits = 0;
}

bool MCT_Node :: isLeaf() {
    return children.size() == 0;
}
