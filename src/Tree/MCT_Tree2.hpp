#ifndef _MCT_TREE_HPP_
    #define _MCT_TREE_HPP_

    #include <map>
    #include <memory>
    #include <string>
    #include <vector>
    #include "MCT_Node.hpp"
    #include "MCT_StateNode.hpp"
    #include "MCT_ActionNode.hpp"
    #include "Path.hpp"

    using namespace std;

    class MCT_Tree {
        public:
            MCT_Tree();
            void clear();
            shared_ptr<MCT_Node> getRoot();
            vector<Path> getBestPaths();
            vector<Path> getBestPaths(shared_ptr<MCT_Node> node);
            //vector<vector<int>> getBestPaths();
            //vector<vector<int>> getBestPaths(shared_ptr<MCT_Node> node);
            vector<MCT_Node*> getNodes(Path& path);
            set<Path> getTerminalActions();
            set<Path> getTerminalActions(shared_ptr<MCT_Node>& start, 
                                                          Path parent);
//            void getTerminalActions(map<string, MCT_Node*>& result);
//            void getTerminalActions(shared_ptr<MCT_Node>& start,
//                                    map<string, MCT_Node*>& result);
        private:
            shared_ptr<MCT_Node> root;
    };


#endif
