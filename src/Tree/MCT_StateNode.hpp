#ifndef _MCT_STATENODE_HPP_
    #define _MCT_STATENODE_HPP_
    #include <iostream>
    #include <limits>
    #include <utility>
    #include "MCT_Node.hpp"
    #include "MCT_ActionNode.hpp"

    using namespace std;

    class MCT_StateNode : public MCT_Node {
        public:
            MCT_StateNode(int id);
            MCT_StateNode(MCT_Node* parent, int id);
           ~MCT_StateNode();            
            string getLabel();
            void addChild(int id);
            void assign(double value);
            int getBestAction();
            MCT_NodeType getType();
            bool isTerminal();
            string toString();
            void update(MCT_Node* action);
            double value();
        private:
            double bestValue;
            MCT_Node* bestChild;

            void reset_bests();
    };

    bool operator<(const MCT_StateNode& s1, const MCT_StateNode& s2);

#endif
