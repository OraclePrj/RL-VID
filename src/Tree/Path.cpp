#include "Path.hpp"

void Path::append(shared_ptr<MCT_Node> node) {
    append(&*node);
}

void Path::append(MCT_Node* node) {
    MCT_NodeType nodeType = node->getType();
    if (nodeType == MCT_NodeType::STATE) {
        MCT_StateNode* stateNode = dynamic_cast<MCT_StateNode*>(node);
        append(stateNode);
    } else if (nodeType == MCT_NodeType::ACTION) {
        MCT_ActionNode* actionNode = dynamic_cast<MCT_ActionNode*>(node);
        append(actionNode);
    } else {
        //Throw bad argument exception
        throw(invalid_argument("Invalid node type"));
    }
}

void Path::append(MCT_StateNode* state) {
    appendState(state->getId());
}

void Path::append(MCT_ActionNode* action) {
    appendAction(action->getId());
}

void Path::appendAction(int id) {
    ids.push_back(id);
    state.push_back(false);
}

void Path::appendState(int id) {
    ids.push_back(id);
    state.push_back(true);
}

vector<int> Path::asList() const {
    return ids;
}

void Path::extend(const Path& other) {
    for (unsigned index = 0; index < other.size(); index++) {
        if (other.isState(index)) {
            appendState(other[index]);
        } else {
            appendAction(other[index]);
        }
    }
}

vector<int> Path::getStatePath() {
    vector<int> result;
    for (unsigned index = 0; index < ids.size(); index++) {
        if (state[index]) result.push_back(ids[index]);
    }
    return result;
}

set<Path> Path::getSubPaths(vector<Path>& paths) {
    //Preprocess subPaths so they are a string of best action updates.
    set<Path> subPaths;
    for (Path& path : paths) {
        unsigned lastActionIndex = 0;
        bool done = false;
        while (!done) {
            Path subPath;
            for (unsigned index = 0; index < path.size(); index++) {
                if (index == path.size() - 1) {
                    done = true;
                }
                if (path.isState(index)) {
                    subPath.appendState(path[index]);
                } else if (path.isAction(index)) {
                    subPath.appendAction(path[index]);
                    if (index > lastActionIndex) {
                        lastActionIndex = index;
                        break;
                    }
                }
            }
            subPaths.insert(subPath);
        }
    }
    return subPaths;
}

bool Path::isState(int index) const {
    return state[index];
}

bool Path::isAction(int index) const {
    return !state[index];
}

bool operator<(const Path& p1, const Path& p2) {
    if (p1.ids < p2.ids) return true;
    if (p1.ids > p2.ids) return false;
    return p1.state < p2.state;
}

int Path::operator[](int index) const {
    return ids[index];
}

int Path::pop() {
    int result = ids.back();
    ids.pop_back();
    state.pop_back();
    return result;
}

unsigned Path::size() const {
    return ids.size();
}

string Path::toString() const {
    ostringstream result;
    for (unsigned index = 0; index < ids.size(); index++) {
        if (index > 0) result << ',';

        result << (state[index] ? 's' : 'a');
        result << ids[index];
    }
    return result.str();
}
