#include "MCT_StateNode.hpp"

MCT_StateNode::MCT_StateNode(int id) : MCT_Node(id) {
    visits = 0;
    reset_bests();
}

MCT_StateNode::MCT_StateNode(MCT_Node* parent, int id) : MCT_Node(parent, id) {
    reset_bests();
}

MCT_StateNode::~MCT_StateNode() {
    //Free all children
    for (auto& entry : children) {
        delete entry.second; //map<int, MCT_Node*>
    }
}

void MCT_StateNode::addChild(int id) {
    children[id] = new MCT_ActionNode(this, id);
}

void MCT_StateNode::assign(double value) {
    reset_bests();
    bestValue = value;
    visits += 1;
}

int MCT_StateNode :: getBestAction() {
    return (bestChild == nullptr) ? -1 : bestChild->getId();
}

string MCT_StateNode :: getLabel() { 
    ostringstream result;
    result << 's' << id;
    return result.str();
}

MCT_NodeType MCT_StateNode::getType() {
    return MCT_NodeType::STATE;
}

bool MCT_StateNode :: isTerminal() {
    return id == 1 || id == 2;
}

bool operator<(const MCT_StateNode& s1, const MCT_StateNode& s2) {
    return s1.getId() < s2.getId();
}


void MCT_StateNode :: reset_bests() {
    bestChild = nullptr;
    bestValue = -numeric_limits<double>::infinity();
}

string MCT_StateNode :: toString() {
    ostringstream result;
    result << "StateNode (Visits: " << visits << ")" << endl;
    result << "Path: " << getPath() << endl;
    result << "Action values: ";
    int bestIndex = -1, count = 0;
    for (auto& entry : children) {
        if (count > 0) result << ", ";
        MCT_Node* child = entry.second;
        result << child->value();
        if (bestChild == child) {
            bestIndex = count;
        }
        count++;
    }
    if (count == 0) result << "(None)";
    result << endl;
    result <<  "BestAction: " << bestIndex; 
    result << ", BestValue: " << bestValue;
    return result.str();
}

void MCT_StateNode :: update(MCT_Node* child) {
    //We know bestValue and bestChild.
    double childValue = child->value();    

    if (child == bestChild && childValue >= bestValue) {
        //If modified bestChild and increased its value, increase bestValue.
        bestValue = childValue;
    } else if (bestChild == NULL) {
        //Need to replace simulated value with real one.
        bestChild = child;
        bestValue = childValue;
    } else if (child != bestChild && childValue > bestValue) {
        //If modified nonBest and value is higher than best, replace both.
        bestChild = child;
        bestValue = childValue;
    } else if (child == bestChild && childValue < bestValue) {
        //Another child could be better now, iterate over all children.
        reset_bests(); //TODO: Move below into reset_bests and rename fn.
        for (auto& entry : children) {
            MCT_Node* action = entry.second;
            double childValue = action->value();
            if (childValue > bestValue) {
                bestValue = childValue;
                bestChild = action;
            }
        } 
    }

    visits += 1;
}

double MCT_StateNode :: value() {
    return bestValue;
}
