#ifndef _MCT_STATENODE_HPP_
    #define _MCT_STATENODE_HPP_
    #include <iostream>
    #include <limits>
    #include <memory>
    #include "MCT_Node.hpp"
    #include "MCT_ActionNode.hpp"

    using namespace std;

    class MCT_StateNode : public MCT_Node {
        public:
            MCT_StateNode(int id);
            MCT_StateNode(MCT_Node* parent, int id);
            
            string getLabel();
            void addChild(int id);
            void assign(double value);
            int getBestAction();
            MCT_NodeType getType();
            bool isTerminal();
            string toString();
            void update(MCT_Node* child, double value);
            double value();
        private:
            double bestValue;
            MCT_Node* bestChild;

            void reset_bests();
    };

#endif
