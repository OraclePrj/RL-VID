#ifndef _POLICY_HPP_
    #define _POLICY_HPP_
    #include <cmath>
    #include <iostream>
    #include <memory>
    #include "CLIArgs.hpp"
    #include "Config.hpp"
    #include "QBASE/IntSpace.hpp"
    #include "QBASE/QBASE.hpp"
    #include "Simulator/Random.hpp"
    #include "Tree/MCT_Node.hpp"
    #include "Tree/MCT_StateNode.hpp"

    using namespace std;

    class Policy {
        public:
            Policy(Config& config);
            QBASE& getQBASE();
            IntSpace& getSpace();
            void handle(CLIArgs& args);
            int rollout(int state);
            int tree(shared_ptr<MCT_Node> node, int actionCount);
            int tree(MCT_Node* node, int actionCount);
            int tree(MCT_StateNode* state, int actionCount);
        private:
            PolicyMode mode;
            IntSpace space;
            QBASE qbase;
            Random random;
            int greedy(MCT_StateNode* state);
            int ucb(MCT_StateNode* state, int actionCount);
    };

#endif
