#include <unistd.h>
#include "SharedMemory.hpp"

string key;
int length = 0;
double period = 0.0;

void process_args(int argc, char* argv[]) {
    //Arguments should be: shmview <key>, <length>[, <period>]
    if (argc != 3 && argc != 4) {
        cerr << "Usage: shmview <shmkey>, <shmlen>[, <updateperiod>]" << endl;
        exit(1);
    } else if (argc == 4) {
        period = stod(argv[3]);
    } else {
        period = 0.01;
    }
    key = argv[1];
    length = stoi(argv[2]);
}

int main(int argc, char* argv[]) {
    process_args(argc, argv);
  
    SharedMemory shmem(key);
    while (1) {
        cout << "\n[";
        for (int ii = 0; ii < length; ii++) {
            if (ii > 0) cout << ", ";
            cout << shmem.get(ii);
        }
        cout << "]";
        usleep(1000 * 1000 * period); //Wait period
    } 
}
