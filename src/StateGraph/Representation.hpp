#ifndef _REPRESENTATION_HPP_
    #define _REPRESENTATION_HPP_
    #include <iostream>
    #include <string>
    #include "llvm/IR/LLVMContext.h"
    #include "llvm/IR/Module.h"
    #include "llvm/IRReader/IRReader.h"
    #include "llvm/Support/raw_ostream.h"
    #include "llvm/Support/SourceMgr.h"
    #include "Function.hpp"
    using namespace std;    

    namespace PRGM {
        class Representation {
            public:
                 Representation(string filename);
                ~Representation();

                FunctionIterator begin();
                FunctionIterator end();
                unsigned getBlockCount();
                Function getFunction(string name);
                string toString() const;
            private:
                unsigned blockCount;
                llvm::LLVMContext context;
                unique_ptr<llvm::Module> file;
                unique_ptr<llvm::Module> get(string filename);
        };
    }

    ostream& operator<<(ostream& output, const PRGM::Representation& repr);

#endif
