#ifndef _BLOCKCOUNTER_HPP_
    #define _BLOCKCOUNTER_HPP_

    #include <string>
    #include "Function.hpp"
    #include "Representation.hpp"

    
    using namespace std;    
    using namespace PRGM;

    class BlockCounter {
        public:
            unsigned scan(string filename);
        private:
            //Nothing for now
    };    

#endif
