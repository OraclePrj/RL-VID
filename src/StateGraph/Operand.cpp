#include "Operand.hpp"

Operand::Operand(llvm::Value* operand) {
    this->operand = operand;
}

double Operand::getConstantValue() {
    if(llvm::isa<llvm::ConstantInt>(operand)) {
        llvm::ConstantInt* integer = static_cast<llvm::ConstantInt*>(operand);
        const llvm::APInt& value = integer->getValue();
        return value.getSExtValue();
    } else if (llvm::isa<llvm::ConstantFP>(operand)) {
        llvm::ConstantFP* floPt = static_cast<llvm::ConstantFP*>(operand);
        const llvm::APFloat& value = floPt->getValueAPF();
        return value.convertToDouble();
    }

    return numeric_limits<double>::quiet_NaN();
}

string Operand::getLabel() {
    return operand->getName();
}

bool Operand::hasLabel() {
    return operand->hasName();
}

bool Operand::isConstant() {
    //For our purposes, it's a constant if it's an integral type...
    if(llvm::isa<llvm::ConstantInt>(operand)) {
        return true;
    }

    //Or if a floating point type.
    return llvm::isa<llvm::ConstantFP>(operand);
}

bool Operand::isLocalVar() {
    //Want to know if Value is an Alloca instruction.
    //if (!llvm::isa<llvm::Instruction>(operand)) {
    //    return false;
    //}
    //llvm::Instruction* instruction = static_cast<llvm::Instruction*>(operand);
    return llvm::isa<llvm::AllocaInst>(operand);
}

bool Operand::isPointer() {
    //TODO: Is necessary? And still untested.
    llvm::Type* type = operand->getType();
    return llvm::isa<llvm::PointerType>(type);
}

void Operand::setLabel(string label) {
    llvm::Twine twine(label);
    operand->setName(twine);
}

string Operand::toString() const {
    string result;
    llvm::raw_string_ostream output(result);
    output << *operand;
    return result;
}

ostream& operator<<(ostream& output, const Operand& operand) {
    output << operand.toString();
    return output;
}
