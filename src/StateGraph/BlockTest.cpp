#include <iostream>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/SourceMgr.h"
#include "Block.hpp"

using namespace std;

int main()
{
    string filename = "testcase.bc";
    llvm::LLVMContext Context;
    llvm::SMDiagnostic Err;
    unique_ptr<llvm::Module> Mod = parseIRFile(filename.c_str(), Err, Context);

    if (!Mod) {
        Err.print(filename.c_str(), llvm::errs());
        return 1;
    }
    //Mod->dump();
    //Err.print("====================================", errs());
    //Successfully loaded.
    //Get first function
    for (auto function = Mod->begin();
                        function != Mod->end(); ++function) {
        //function->dump();
        //Get first block.
        for (auto block = function->begin();
                          block != function->end(); ++block) {
             block->dump();
             PRGM::Block thisBlock(block);
             cout << thisBlock << endl;
             break;
        }
        break;
    }
}

