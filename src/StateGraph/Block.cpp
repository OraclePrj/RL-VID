#include "Block.hpp"

PRGM::Block::Block(llvm::BasicBlock* block) {
    this->block = block;
}

PRGM::Block::Block(BlockIterator block) {
    this->block = &(*block);
}

PRGM::Block::~Block() {}

InstructionIterator PRGM::Block::begin() {
    return block->begin();
}

vector<PRGM::Block> PRGM::Block::children() {
    vector<Block> result;
    //1) Get terminator block
    llvm::TerminatorInst* term = block->getTerminator();

    //2) Iterate over successors, placing each in vector
    for (auto child : term->successors()) {
        Block childBlock(child);
        result.push_back(childBlock);
    }

    //3) Return vector.
    return result;
}

InstructionIterator PRGM::Block::end() {
    return block->end();
}

PRGM::Instruction PRGM::Block::first() {
    InstructionIterator front = block->front().getIterator();
    Instruction first(front);
    return first;
}

PRGM::Instruction PRGM::Block::getConditionVariable() {
    //Get last instruction
    llvm::Instruction* back = &(block->back());
    Instruction last(back);

    //Confirm it's a conditional jump
    if (!last.isConditionalBranch()) {
        //Raise exception
        throw invalid_argument("Block: no conditional variable to get!");
    }

    //3) Return its condition operand
    vector<Operand> operands = last.getOperands();
    llvm::Value* conditionValue = operands[0].operand;
    llvm::Instruction* condVarPtr = static_cast<llvm::Instruction*>(conditionValue);
    Instruction conditionVariable(condVarPtr); //TODO: Make Operand inherit from Instruction
    return conditionVariable; //TODO: Check that this is the correct one
}

int PRGM::Block::getId() {
    string name = block->getName().str();
    int id;
    try {
        id = stoi(name);
    } catch (invalid_argument& e) {
        id = -1; 
    }
    return id;
}

vector<PRGM::Block> PRGM::Block::getParents() {
    vector<Block> blocks;
    for (llvm::BasicBlock* parentPtr : predecessors(block)) {
        Block parent(parentPtr);
        blocks.emplace_back(parent);
    }
    return blocks;
}

PRGM::Instruction PRGM::Block::last() {
    InstructionIterator back = block->back().getIterator();
    Instruction last(back);
    return last;
}

void PRGM::Block::setId(int id) {
    ostringstream name;
    name << id;
    llvm::Twine twine(name.str());
    block->setName(twine);
}

unsigned PRGM::Block::size() {
    return block->size();
}

PRGM::Block PRGM::Block::splitFrom(Instruction splitPt) {
    //Splits this instruction & everything after into a new Block.

    //Extract pointer from iterator
    llvm::BasicBlock* newBlock = SplitBlock(block, splitPt.instruction);
    Block result(newBlock->getIterator());
    return result;
}

string PRGM::Block::toString() const {
    string result;
    llvm::raw_string_ostream output(result);
    output << *block;
    return result;
}

bool PRGM::Block::operator==(const PRGM::Block& other) {
    return this->block == other.block;
}

bool PRGM::Block::operator!=(const PRGM::Block& other) {
    return !(*this == other);
}

bool PRGM::Block::operator<(const PRGM::Block& that) const {
    return this->block < that.block;
}

ostream& operator<<(ostream& output, const PRGM::Block& block) {
    output << block.toString();
    return output;
}
