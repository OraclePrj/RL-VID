#include "../Simulator/Process.hpp"
#include "../Simulator/Random.hpp"
#include "SharedMemory.hpp"

//For proper test, should have simple child to get reader functioning.
//Should probably use Process to save rework with fork/exec, piping, etc.
//Child class should use SharedMemory class as well.

int main(int argc, char* argv[]) {
    Random random;

    //Create SharedMemory.
    SharedMemory mem(SHM_ENV_VAR, 2);

    Process view("./shmview");
    vector<string> args = {SHM_ENV_VAR, "2"};
    view.launch(args);
    usleep(100000 * 5);
    
    //Write a random number at beginning of SharedMemory.
    int randomNumber = random.integer(0, 255);
    mem.set(0, randomNumber);
    mem.set(1, random.integer(0, 255));
    usleep(100000 * 5);

    //Launch child that should 
        //read random number
        //XOR it with its own random number
        //write result to shared memory
        //return random number

    Process process("./shm_client");
    process.launch();

    //Wait on child and get exit value.
    int childReturn = process.getReturnValue();

    usleep(100000 * 5);
    view.stop();

    //Read shared memory.
    int currentMemory = mem.get(0);

    //Compare child return value to own random number ^ shared memory value.
    int xorResult = randomNumber ^ currentMemory;

    cout << "===SERVER REPORT===" << endl;
    cout << "Server random number: " << randomNumber << endl;
    cout << "Current shared memory: " << currentMemory << endl;
    cout << "XOR Result with shared memory: " << xorResult << endl;
    cout << "Client return value: " << childReturn << endl;
    cout << "===================" << endl;
}
