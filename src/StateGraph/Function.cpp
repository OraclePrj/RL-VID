#include "Function.hpp"

PRGM::Function::Function(FunctionIterator element) {
     function = &(*element);
}

PRGM::Function::Function(llvm::Function* element) {
     function = element;
}

PRGM::Function::~Function() {}

unsigned PRGM::Function::getBlockCount() {
    return function->size();
}

string PRGM::Function::getName() {
    return function->getName().str();
}

BlockIterator PRGM::Function::begin() {
    return function->begin();
}

BlockIterator PRGM::Function::end() {
    return function->end();
}

string PRGM::Function::toString() const {
    string result;
    llvm::raw_string_ostream output(result);
    output << *function;
    return result;
}

ostream& operator <<(ostream& output, const PRGM::Function& function) {
    output << function.toString();
    return output;
}
