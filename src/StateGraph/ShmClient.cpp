#include <iostream>
#include "../Simulator/Random.hpp"
#include "SharedMemory.hpp"

int main(int argc, char* argv[]) {
    Random random;
    SharedMemory shm(SHM_ENV_VAR); //Read

    //Generate own random number
    int own = random.integer(0, 255);

    //read random number
    int other = shm.get(0);

    //XOR it with its own random number
    int encrypted = own ^ other;
    
    //write result to shared memory
    shm.set(0, encrypted);

    cout << "===Client Report===" << endl;
    cout << "Own random number: " << own << endl;
    cout << "Number read from shared memory: " << other << endl;
    cout << "XORed result: " << encrypted << endl;
    cout << "Returning: " << own << endl;
    cout << "===================" << endl;    

    //return own random number
    return own;
}
