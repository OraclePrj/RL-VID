#include "SharedMemory.hpp"

/* 
 * This file is heavily influenced by American Fuzzy Lop source code.
 * See https://github.com/mirrorer/afl - I mostly just tried to simplify/adapt to my style.
 */

SharedMemory::SharedMemory(string key) { //Read
    baseAddress = NULL;
    shmId = 0;
    this->length = 0;

    char* keyContents = getenv(key.c_str());
    if (keyContents == NULL) {
        int errorCode = errno;
        cerr << "Error: getenv() failed!" << endl;

        exit(errorCode);
    } else {
        shmId = stoi(keyContents);
    }

    //Find suitable (unused) address to attach shared memory to
    baseAddress = static_cast<int*>(shmat(shmId, NULL, 0));
    if (baseAddress == (int*)-1)  {
        //shmat() failed.
        int errorCode = errno;
        cerr << "Error: shmat() failed!" << endl;
        if (errorCode == EACCES) {
            cerr << "Don't have required permissions/capability";
        } else if (errorCode == EIDRM) {
            cerr << "shmid points to a removed identifier";
        } else if (errorCode == EINVAL) {
            cerr << "Invalid argument (see documentation for possibilities)";
        } else if (errorCode == ENOMEM) {
            cerr << "Out of memory.";
        } else {
            cerr << "Unknown error code: " << errorCode;
        }
        cerr << endl;
        exit(errorCode);
    }
}

SharedMemory::SharedMemory(string key, unsigned length) {
    //Initialize with supplied/dummy values
    baseAddress = NULL;
    shmId = 0;
    this->length = length;

    //Create new shared memory big enough to fit length bytes.
    size_t bytes = length * sizeof(int); //TODO: make template for different objects
    shmId = shmget(IPC_PRIVATE, bytes, IPC_CREAT | IPC_EXCL | getPrivs());
    if (shmId == -1) {
        int errorCode = errno;
        cerr << "Error: shmget() failed!" << endl;
        exit(errorCode);
    }

    // Set environment variable so programs can find it
    ostringstream toString;
    toString << shmId;
    setenv(key.c_str(), toString.str().c_str(), 1); //TODO: be able to have mult vars?

    //Find suitable (unused) address to attach shared memory to
    baseAddress = static_cast<int*>(shmat(shmId, NULL, 0));
    if (baseAddress == (int*)-1)  {
        //shmmat() failed.
        int errorCode = errno;
        cerr << "Error: shmat() failed!" << endl;
        exit(errorCode);
    }
    //cout << "SharedMemory loaded." << endl;
}

SharedMemory :: ~SharedMemory() {
    int returned = shmctl(shmId, IPC_RMID, NULL);
    if (returned == -1) {
        //shmctl() failed.
        int errorCode = errno;
        cerr << "Warning: shmctl() failed! Shared memory will not free" << endl;
        exit(errorCode);
    }
}

void SharedMemory :: clear() {
  /* After this memset, trace_bits[] are effectively volatile, so we
     must prevent any earlier operations from venturing into that
     territory. */
  memset(baseAddress, 0, length * sizeof(int)); //TODO: Template
}

int SharedMemory :: getPrivs() {
    int READ_BIT  = 1 << 2;
    int WRITE_BIT = 1 << 1;
    //int EXEC_BIT  = 1 << 0; // Uncomment this line if needed
    int USER_BITS  = 2 * 3;
    int GROUP_BITS = 1 * 3;
    int OTHER_BITS = 0 * 3;

    //Assign owner privileges for reading/writing to shared memory.
    int userPrivs  = (READ_BIT | WRITE_BIT) << USER_BITS;  //Read/write
    int groupPrivs =           0            << GROUP_BITS; //None
    int otherPrivs =           0            << OTHER_BITS; //None
    return userPrivs | groupPrivs | otherPrivs;
}

//TODO: Make template to support other types.
vector<int> SharedMemory :: asVector() {
    vector<int> result(length);
    for (unsigned index = 0; index < length; index++) {
        result[index] = get(index);
    }
    return result;
}

//TODO: Make template to support other types.
int SharedMemory:: get(int index) {
    return *(baseAddress + index);
}

//TODO: Make template to support other types.
void SharedMemory::set(int index, int value) {
    *(baseAddress + index) = value;
}

unsigned SharedMemory :: size() {
    return length;
}
