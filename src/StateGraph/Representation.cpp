#include "Representation.hpp"

PRGM::Representation::Representation(string filename) : 
                                                    file(move(get(filename))) {
    //cout << "Loading Representation" << endl;
    if (!file) {
        string errorMsg = "Could not load from file: '";
        errorMsg.append(filename + "'.");
        throw invalid_argument(errorMsg);
    }

    //Count blocks
    blockCount = 0;
    for (auto fnPointer = begin(); fnPointer != end(); ++fnPointer) {
        Function fn(fnPointer);
        for (auto block = fn.begin(); block != fn.end(); ++block) {
            blockCount += 1;
        }
    }

    //cout << "Representation loaded" << endl;
}

PRGM::Representation::~Representation() {}

FunctionIterator PRGM::Representation::begin() {
    return file->begin();
}

FunctionIterator PRGM::Representation::end() {
    return file->end();
}

unsigned PRGM::Representation::getBlockCount() {
    return blockCount;
}

string PRGM::Representation::toString() const {
    string result;
    llvm::raw_string_ostream output(result);
    output << *file;
    return result;
}



PRGM::Function PRGM::Representation::getFunction(string name) {
    Function function(file->getFunction(name));
    return function;
}

unique_ptr<llvm::Module> PRGM::Representation::get(string filename) {
    llvm::SMDiagnostic error;
    return parseIRFile(filename.c_str(), error, context);
}


ostream& operator<<(ostream& output, const PRGM::Representation& repr) {
    output << repr.toString();
    return output;
}
