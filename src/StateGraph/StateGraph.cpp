#include "StateGraph.hpp"

StateGraph::StateGraph(string filename) {
    vector<int> start = {1, 2};
    vector<int> end = {0};
    
    adjacencyList.push_back(start); //s0: Prgm start
    adjacencyList.push_back(end);   //s1: Prgm end
    adjacencyList.push_back(end);   //s2: Prgm end

    Representation program(filename);

    //Split input blocks off separately and saving into states array as created
    vector<Block> states;
    splitInputs(program, states);

    //Number all state blocks; start from 3 since s0-s2 are taken
    int index = 3;
    for (auto& state : states) {
        state.setId(index++);
    }

    //3) Populate adjacency list accordingly
    for (auto& state : states) {
        vector<int> nextStates = {1, 2};
        findNextStates(state, nextStates);
        adjacencyList.push_back(nextStates);
    }

    //TODO: Check assumption that 3 is always first input state
    if (adjacencyList.size() > 3) {
        //Need a special Block for beginning of main.
        Function function = program.getFunction("main");
        Block firstBlock(function.begin());
        findNextStates(firstBlock, adjacencyList[0]);

        //adjacencyList[0].push_back(3); //Start -> First state
    }
    
}

StateGraph::~StateGraph() {}

bool StateGraph::isState(Block& block) {
    //State is one input fn call followed by a branch or unconditional return.
    if (block.size() != 2) return false;
    if (!block.first().isInput()) return false;
    Instruction last = block.last();
    return last.isUnconditionalBranch() || last.isReturn();
}

void StateGraph::findNextStates(Block& start, vector<int>& children) {
    findNextStates(start, start, children);
}

void StateGraph::findNextStates(Block& start, Block& current, 
                                                    vector<int>& children) {
    //Recursively append all children
    bool seen = false;
    for (int child : children) {
        if (child == current.getId()) {
            seen = true;
            break;
        }
    }

    if (isState(current) && !seen && current != start) {
        children.push_back(current.getId());
    } else {
        for (auto& child : current.children()) {
            findNextStates(start, child, children);
        }
    }
}

void StateGraph::splitInputs(Representation& repr, vector<Block>& states) {
    for (auto currentFn = repr.begin();
              currentFn != repr.end(); ++currentFn) {
        Function function(currentFn);
        for (auto currentBlock = function.begin();
                  currentBlock != function.end(); ++currentBlock) {
            Block block(currentBlock);
            //Is block in states so far?
            bool seen = false;
            for (unsigned index = 0; index < states.size(); index++) {
                if (block == states[index]) {
                    seen = true;
                    break;
                }
            }

            if (!seen) {
                splitBlock(block, states);
            }
        }
    } 
}

void StateGraph::splitBlock(Block& block, vector<Block>& states) {
    //Minimally, a block should have an action and a branch to next block.
    if (isState(block)) {
        states.push_back(block);
    } else if (block.size() > 2) {
        int index = 0;
        for (auto currentInst = block.begin();
                  currentInst != block.end(); ++currentInst) {
            Instruction instruction(currentInst);
            if (instruction.isInput()) {
                if (index == 0) {
                    //.splitFrom requires instruction not to be first
                    Instruction nextInst(++currentInst);
                    Block next = block.splitFrom(nextInst);
                    splitBlock(next, states); //TODO: Refactor: ++ inside if
                } else {
                    Block next = block.splitFrom(instruction);
                    splitBlock(next, states);
                }
                break;
            }
            index++;
        }
    }
}

unsigned StateGraph::size() {
    return adjacencyList.size();
}

string StateGraph::toString() const {
    ostringstream result;
    int neighbor = 0;
    result << '{';
    for (auto& neighbors : adjacencyList) {
        if (neighbor != 0) {
            result << ", ";
        }
        result << neighbor << " => [";
        for (unsigned count = 0; count < neighbors.size(); count++) {
            if (count > 0) {
                result << ", ";
            }
            result << neighbors[count];
        }
        result << ']';
        neighbor++;
    }
    result << '}';
    return result.str();
}

ostream& operator<<(ostream& output, const StateGraph& graph) {
    output << graph.toString();
    return output;
}
