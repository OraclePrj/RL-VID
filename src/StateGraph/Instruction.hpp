#ifndef _INSTRUCTION_HPP_
    #define _INSTRUCTION_HPP_
    #include <iostream>
    #include <set>
    #include <string>
    #include "stdio.h"
    #include "llvm/IR/BasicBlock.h"
    #include "llvm/IR/Instruction.h"
    #include "llvm/IR/Instructions.h"
    #include "llvm/Support/Casting.h"
    #include "llvm/Support/raw_ostream.h"
    #include "Operand.hpp"

    using namespace std;

    typedef llvm::BasicBlock::iterator InstructionIterator;
    //typedef llvm::ilist_iterator<llvm::Instruction> InstructionIterator;

    namespace PRGM {
        class Instruction {
            public:
                llvm::Instruction* instruction;

                Instruction(InstructionIterator element);
                Instruction(llvm::Instruction* element);
               ~Instruction();
                bool evaluatesToPointer();
                bool isAdd();
                bool isAlloc();
                bool isArithmetic();
                bool isCast();
                bool isConditionalBranch();
                bool isDangerous();
                bool isDereference();
                bool isFnCall();
                string getCalledFnName();
                bool isGetElemPtr();
                bool isInline();
                bool isInput();
                bool isLoad();
                bool isReturn();
                bool isStore();
                bool isSubtract();
                bool isUnconditionalBranch();
                llvm::BasicBlock* getBlock();
                string getLabel() const;
                vector<Operand> getOperands();
                void setLabel(string label);
                string toString() const;
                bool operator< (const Instruction& that) const;
            private:
                static const set<string> inputFns;
                static const set<string> dangerousFns;

        };
    }
    ostream& operator<<(ostream& output, const PRGM::Instruction& instruction);

#endif
