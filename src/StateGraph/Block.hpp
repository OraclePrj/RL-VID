#ifndef _BLOCK_HPP_
    #define _BLOCK_HPP_
    #include <sstream>
    #include <string>
    #include <vector>
    #include "llvm/ADT/Twine.h"
    #include "llvm/IR/BasicBlock.h"
    #include "llvm/IR/CFG.h"
    #include "llvm/IR/Function.h"
    #include "llvm/Support/raw_ostream.h"
    #include "llvm/Transforms/Utils/BasicBlockUtils.h"
    #include "Instruction.hpp"
    using namespace std;

    typedef llvm::Function::iterator BlockIterator;
    //typedef llvm::ilist_iterator<llvm::BasicBlock> BlockIterator;
    namespace PRGM {
        class Block {
            public:
                llvm::BasicBlock* block; //TODO: Make private again somehow.

                Block(llvm::BasicBlock* block);
                Block(BlockIterator block);
               ~Block();
                InstructionIterator begin();
                vector<Block> children();
                InstructionIterator end();
                Instruction first();
                Instruction getConditionVariable();
                int getId();
                vector<Block> getParents();
                Instruction last();
                void setId(int name);
                unsigned size();
                Block splitFrom(Instruction instruction);
                string toString() const;
                bool operator==(const Block& other);
                bool operator!=(const Block& other);
                bool operator< (const Block& that) const;
            private:
                //Nothing for now.
        };
    }
    ostream& operator<<(ostream& output, const PRGM::Block& block);

#endif
