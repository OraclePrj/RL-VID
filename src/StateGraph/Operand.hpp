#ifndef _OPERAND_HPP_
    #define _OPERAND_HPP_
   
    #include <iostream>
    #include <limits>
    #include <string>
    #include "llvm/ADT/APFloat.h"
    #include "llvm/ADT/APInt.h"
    #include "llvm/ADT/Twine.h"
    #include "llvm/IR/Constants.h"
    #include "llvm/IR/Instructions.h"
    #include "llvm/IR/Value.h"
    #include "llvm/Support/raw_ostream.h"

    using namespace std;
    class Operand {
        public:
            llvm::Value* operand; //TODO: Make private somehow.

            Operand(llvm::Value*);
            double getConstantValue();
            string getLabel();
            bool hasLabel();
            bool isConstant();
            bool isLocalVar();
            bool isPointer();
            void setLabel(string label);
            string toString() const;
        private:
            //Nothing for now.
    };

    ostream& operator<<(ostream& output, const Operand& operand);

#endif
