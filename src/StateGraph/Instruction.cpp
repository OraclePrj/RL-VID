#include "Instruction.hpp"

const set<string> PRGM::Instruction::inputFns = {"scanf", "fscanf",
                                                 "__isoc99_scanf"};

//Src: https://www.owasp.org/index.php/Buffer_Overruns_and_Overflows
const set<string> PRGM::Instruction::dangerousFns = {"fscanf",
                                                     "gets",
                                                     "scanf",
                                                     "sprintf",
                                                     "sscanf",
                                                     "strcat",
                                                     "strcpy",
                                                     "vscanf",
                                                     "vsprintf",
                                                     "vsscanf",
                                                     "__isoc99_fscanf",    
                                                       "__isoc99_gets",
                                                      "__isoc99_scanf", 
                                                    "__isoc99_sprintf",
                                                     "__isoc99_sscanf",  
                                                     "__isoc99_strcat",
                                                     "__isoc99_strcpy",  
                                                     "__isoc99_vscanf",
                                                   "__isoc99_vsprintf", 
                                                    "__isoc99_vsscanf"};

PRGM::Instruction :: Instruction(InstructionIterator element) {
    instruction = &(*element);
}

PRGM::Instruction :: Instruction(llvm::Instruction* element) {
    instruction = element;
}

PRGM::Instruction :: ~Instruction() {}


bool PRGM::Instruction :: evaluatesToPointer() {
    //TODO: Needs to be tested.
    //Instruction cast to value is its return type.
    llvm::Value* asValue = dynamic_cast<llvm::Value*>(instruction);
    llvm::Type* type = asValue->getType();
    return llvm::isa<llvm::PointerType>(type);
}

bool PRGM::Instruction :: isUnconditionalBranch() {
    //TODO: Change to use dynamic cast instead
    bool isBranch = llvm::isa<llvm::BranchInst>(instruction);
    bool unconditional = false;
    if (isBranch) {
	llvm::BranchInst* branch = llvm::cast<llvm::BranchInst>(instruction);
	unconditional = branch->isUnconditional();
    }

    return isBranch && unconditional;
}

bool PRGM::Instruction :: isConditionalBranch() {
    //TODO: Change to use dynamic cast instead
    bool isBranch = llvm::isa<llvm::BranchInst>(instruction);
    bool unconditional = true;
    if (isBranch) {
        llvm::BranchInst* branch = llvm::cast<llvm::BranchInst>(instruction);
        unconditional = branch->isUnconditional();
    }

    return isBranch && !unconditional;
}


bool PRGM::Instruction :: isAlloc() {
    return llvm::isa<llvm::AllocaInst>(instruction);
}

bool PRGM::Instruction :: isAdd() {
    if (!instruction->isBinaryOp()) {
        return false;
    }
    
    unsigned opcode = instruction->getOpcode();
    return  ((opcode == llvm::Instruction::Add) ||
             (opcode == llvm::Instruction::FAdd));
}

bool PRGM::Instruction :: isArithmetic() {
    if (instruction->isBinaryOp()) {
        switch(instruction->getOpcode()) {
            case llvm::Instruction::Add:
            case llvm::Instruction::FAdd:
            case llvm::Instruction::Sub:
            case llvm::Instruction::FSub:
            case llvm::Instruction::Mul:
            case llvm::Instruction::FMul:
            case llvm::Instruction::UDiv:
            case llvm::Instruction::SDiv:
            case llvm::Instruction::FDiv:
            case llvm::Instruction::URem:
            case llvm::Instruction::SRem:
            case llvm::Instruction::FRem:
            case llvm::Instruction::Shl:
            case llvm::Instruction::LShr:
            case llvm::Instruction::AShr:
                return true;
            default:
                return false;
        }
    }
    
    return false;
}

bool PRGM::Instruction :: isCast() {
    return llvm::isa<llvm::CastInst>(instruction);
}

bool PRGM::Instruction :: isDangerous() {
    if (!isFnCall()) {
        return false; 
    }
    string calledName = getCalledFnName();
    
    //Return if this name is in names of dangerousFns.
    return dangerousFns.find(calledName) != dangerousFns.end();
}

bool PRGM::Instruction :: isDereference() {
    //Is instruction a load or a store instruction?
    if (llvm::isa<llvm::LoadInst>(instruction)) {
        return true;
    } else {
        return (llvm::isa<llvm::StoreInst>(instruction));
    }
}

bool PRGM::Instruction :: isFnCall() {
    if (!llvm::isa<llvm::CallInst>(instruction)) {
        return false;
    } 
    //May also be an internal function...
    llvm::CallInst* call = llvm::cast<llvm::CallInst>(instruction);
    return call->getCalledFunction() != NULL;
}

bool PRGM::Instruction :: isGetElemPtr() {
    return llvm::isa<llvm::GetElementPtrInst>(instruction);
}

bool PRGM::Instruction :: isInline() {
    return false; //TODO
}

bool PRGM::Instruction :: isInput() { 
    if (!isFnCall()) {
        return false; 
    }
    string calledName = getCalledFnName();
    //cout << "DEBUG: Fn call of name \"" << calledName << "\"" << endl;
    
    //Return if this name is in names of inputFns.
    return inputFns.find(calledName) != inputFns.end();
}

bool PRGM::Instruction :: isLoad() {
    return llvm::isa<llvm::LoadInst>(instruction);
}

bool PRGM::Instruction :: isReturn() {
    return llvm::isa<llvm::ReturnInst>(instruction);
}

bool PRGM::Instruction :: isStore() {
    return llvm::isa<llvm::StoreInst>(instruction);
}

llvm::BasicBlock* PRGM::Instruction::getBlock() {
    return instruction->getParent();
}

string PRGM::Instruction::getCalledFnName() {
    //Precondition: only called if is a FnCall.
    llvm::CallInst* call = llvm::cast<llvm::CallInst>(instruction);
    llvm::Function* function = call->getCalledFunction();
    llvm::StringRef name = function->getName();
    return name.str();
}

string PRGM::Instruction :: getLabel() const {
    return instruction->getName();
}

vector<Operand> PRGM::Instruction :: getOperands() {
    vector<Operand> result;
    //Need to know how many operands it has.
    unsigned operandCount = instruction->getNumOperands();
    //cout << "DEBUG/Instruction: Operand count: " << operandCount << endl;
    for (unsigned ii = 0; ii < operandCount; ii++) {
        Operand operand(instruction->getOperand(ii));
            result.push_back(operand);
    }
    return result;
}

bool PRGM::Instruction :: isSubtract() {
    if (!instruction->isBinaryOp()) {
        return false;
    }
    unsigned opcode = instruction->getOpcode();
    
    return ((opcode == llvm::Instruction::Sub) ||
            (opcode == llvm::Instruction::FSub));
}

void PRGM::Instruction :: setLabel(string label) {
    llvm::Twine twine(label);
    instruction->setName(twine);
}

string PRGM::Instruction :: toString() const {
    string result;
    llvm::raw_string_ostream output(result);
    output << *instruction;
    return result;
}

bool PRGM::Instruction::operator< (const PRGM::Instruction& that) const {
    return this->instruction < that.instruction;
}

ostream& operator<<(ostream& output, const PRGM::Instruction& instruction) {
    output << instruction.toString();
    return output;
}

