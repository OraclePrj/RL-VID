#include "stdio.h"
#include "BlockCounter.hpp"

int main(int argc, char* argv[]) {
    BlockCounter counter;
    if (argc > 1) {
        unsigned blockCount = counter.scan(argv[1]);
        printf("Blocks counted: %d\n", blockCount);
    } else {
        printf("Usage: %s <filename>\n", argv[0]);
    }
    return 0;
}
