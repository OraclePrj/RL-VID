#ifndef _SHAREDMEMORY_HPP_
    #define _SHAREDMEMORY_HPP_
    #include <cstring> //memset
    #include <iostream>
    #include <sstream>
    #include <sys/ipc.h>   // shmctl, shmget
    #include <sys/types.h> // shmat, shmdt
    #include <sys/shm.h>   // shmctl, shmat, shmdt, shmget
    #include <vector>

    using namespace std;

    #define SHM_ENV_VAR "__BOP_SHM_ID"

    class SharedMemory {
        public:
            SharedMemory(string key);
            SharedMemory(string key, unsigned length);
           ~SharedMemory();
            vector<int> asVector();
            void clear();
            int get(int index);
            void set(int index, int value);
            unsigned size();
        private:
            int* baseAddress;
            unsigned length;
            int shmId;

            int getPrivs();
    };

#endif
