#include <iostream>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/SourceMgr.h"
#include "Function.hpp"

using namespace std;

int main()
{
    string filename = "testcase.bc";
    llvm::LLVMContext Context;
    llvm::SMDiagnostic Err;
    unique_ptr<llvm::Module> Mod = parseIRFile(filename.c_str(), Err, Context);

    if (!Mod) {
        Err.print(filename.c_str(), llvm::errs());
        return 1;
    }
    //Mod->dump();
    //Err.print("====================================", errs());
    //Successfully loaded.
    //Get first function
    for (auto function = Mod->begin();
                        function != Mod->end(); ++function) {
        function->dump();
        PRGM::Function newFunction(function);
        cout << "===========================================" << endl;
        cout << newFunction << endl;
        
    }
}

