#include <iostream>
#include "Representation.hpp"

int main()
{
    string filename = "testcase.bc";
    PRGM::Representation representation(filename);
    cout << representation << endl;
}
