#include "BlockCounter.hpp"

unsigned BlockCounter::scan(string filename) {
    unsigned count = 0;
    Representation prgm(filename);
    for (auto functionPtr = prgm.begin(); functionPtr != prgm.end() ; ++functionPtr) {
        Function function(functionPtr);
        for (auto block = function.begin(); block != function.end(); ++block) {
            count++;
        }
    }
    return count;
}
