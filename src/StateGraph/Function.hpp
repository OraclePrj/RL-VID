#ifndef _FUNCTION_HPP_
    #define _FUNCTION_HPP_
    #include <string>
    #include "llvm/IR/Function.h"
    #include "llvm/IR/Module.h"
    #include "llvm/Support/raw_ostream.h"
    #include "Block.hpp"

    using namespace std;    
    typedef llvm::Module::iterator FunctionIterator;
    //typedef llvm::ilist_iterator<llvm::Function> FunctionIterator;
    
    namespace PRGM {
        class Function {
            public:
                llvm::Function* function; //TODO: Make private somehow

                Function(FunctionIterator element);
                Function(llvm::Function* function);
               ~Function();
                unsigned getBlockCount();
                string getName();
                BlockIterator begin();
                BlockIterator end();
                string toString() const;
            private:
                //Nothing for now.
        };
    }

    ostream& operator <<(ostream& output, const PRGM::Function& function);

#endif
