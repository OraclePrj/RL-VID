#ifndef _STATEGRAPH_HPP_
    #define _STATEGRAPH_HPP_
    #include <sstream>
    #include <string>
    #include <vector>
    #include "Block.hpp"
    #include "Representation.hpp"
    using namespace std;
    using namespace PRGM;

    class StateGraph {
        public:
            StateGraph(string filename);
           ~StateGraph();

            unsigned size();
            string toString() const;
        private:
            vector<vector<int>> adjacencyList; //Formerly public.
            vector<Block> blocks; //NEW
            vector<Block> states; //NEW
            vector<vector<Block>> paths;

            bool isState(Block& block);
            void findNextStates(Block& start, vector<int>& children);
            void findNextStates(Block& start, Block& current, 
                                              vector<int>& children);
            void splitInputs(Representation& repr, vector<Block>& states);
            void splitBlock(Block& block, vector<Block>& states);
    };

    ostream& operator<<(ostream& output, const StateGraph& graph);

#endif
