#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE QBASE
#include <boost/test/unit_test.hpp>

#include "../Simulator/Random.hpp"
#include "IntSpace.hpp"

unsigned TRIALS = 10000;

BOOST_AUTO_TEST_CASE( IntSpace_00 )
{
    Random random;
    for (unsigned trial = 0; trial < TRIALS; trial++) {
        //Randomly generate lower, upper
        int lower = random.integer();
        int upper = random.integer();

        //If lower > upper, swap
        if (lower > upper) {
            int temp = upper;
            upper = lower;
            lower = temp;
        }

        //Create IntSpace
        IntSpace space(lower, upper);
    
        //Get lower
        BOOST_CHECK_EQUAL(lower, space.getLower());

        //Get upper
        BOOST_CHECK_EQUAL(upper, space.getUpper());

        //Get type
        BOOST_CHECK_EQUAL(INT, space.getType());
    }
}

