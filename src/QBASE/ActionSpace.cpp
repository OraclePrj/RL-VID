#include "ActionSpace.hpp"

ActionSpace::ActionSpace(ActionSpaceType type) {
    this->type = type;
}

ActionSpaceType ActionSpace::getType() {
    return type;
}

bool ActionSpace::isType(ActionSpaceType type) {
    return this->type == type;
}
