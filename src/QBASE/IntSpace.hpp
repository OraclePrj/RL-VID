#ifndef _INTSPACE_HPP_
#define _INTSPACE_HPP_

    #include <limits>

    #include "ActionSpace.hpp"

    using namespace std;

    enum Granularity { DISCRETE, CONTINUOUS };

    class IntSpace : public ActionSpace {
        public:
            IntSpace();
            IntSpace(Granularity granularity, int lower, int upper);
            Granularity getGranularity();
            int getLower();
            int getUpper();
        private:
            Granularity granularity;
            int lower, upper;
    };

#endif
