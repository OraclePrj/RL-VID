#include "ActionSampler.hpp"

ActionSampler::ActionSampler() {}

ActionSampler::ActionSampler(IntSpace& givenSpace) : space(givenSpace) {}

void ActionSampler::sampleSet(vector<int>& batch, unsigned batch_size) {
     while (batch.size() < batch_size) {
         int candidate = random.integer(space.getLower(), space.getUpper());
         
         //TODO: Make this linear search less ugly - and maybe more efficient
         if (find(batch.begin(), batch.end(), candidate) == batch.end()) {
             batch.push_back(candidate);
         }
     }
}

void ActionSampler::setSpace(IntSpace& givenSpace) { 
     space = givenSpace;
}
