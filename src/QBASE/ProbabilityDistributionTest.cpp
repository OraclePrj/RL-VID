#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ProbabilityDistribution
#include <boost/test/unit_test.hpp>

#include <map>
#include <sstream>
#include <vector>
#include "ProbabilityDistribution.hpp"
#include "../Simulator/Random.hpp"

double roundOffTolerance = 1e-10;

BOOST_AUTO_TEST_CASE( ProbabilityDistribution_00 )
{
    //Checks state of Distribution with uniform constructor.
    unsigned samples = 10000;
    unsigned size = 5;
    Random random;
    ProbabilityDistribution distro(random, size);

    map<unsigned, unsigned> counts;

    for (unsigned sampleNum = 0; sampleNum < samples; sampleNum++) {
        unsigned index = distro.sample();
        if (counts.count(index) == 0) counts[index] = 0;
        counts[index] += 1;
    }

    //TO CHECK: 1) counts.size() == size
    BOOST_CHECK_EQUAL(counts.size(), size);

    unsigned sumOfValues = 0;
    for (unsigned index = 0; index < size; index++) {
        unsigned seen = counts.count(index);
        if (seen) {
            sumOfValues += counts[index];
        } else {
            ostringstream builder;
            builder << "Not seen: " << index;
            //TO CHECK: 2) all in [0, size) are in counts.
            BOOST_FAIL(builder.str());
        }
    }
    //TO CHECK: 3) sum of values in counts == samples.
    BOOST_CHECK_EQUAL(sumOfValues, samples);

    vector<double> probs = distro.getProbs();
    //TO CHECK: 4) Weights of distribution size() = size
    BOOST_CHECK_EQUAL(probs.size(), size);

    double prevValue = probs[0];
    double sumOfProbs = prevValue;
    for (unsigned index = 1; index < size; index++) {
        double prob = probs[index];
	sumOfProbs += prob;
        //TO CHECK: 5) Probs of distribution are all equal.
        BOOST_CHECK_CLOSE(prevValue, prob, roundOffTolerance);
        prevValue = prob;
    }
    //TO CHECK: 6) Probs of distribution all sum to 1.
    BOOST_CHECK_EQUAL(sumOfProbs, 1.0);

    //TO CHECK: 7) All Probs are statistical noise (hardest)
}

BOOST_AUTO_TEST_CASE( ProbabilityDistribution_01 )
{
    //Checks state of Distribution with custom weights constructor.
    unsigned samples = 10000;
    vector<double> given = {0.0, 0.1, 0.2, 0.3, 0.2, 0.1, 0.1, 0.0};
    Random random;
    ProbabilityDistribution distro(random, given);
    unsigned size = given.size();

    map<unsigned, unsigned> counts;
    for (unsigned sampleNum = 0; sampleNum < samples; sampleNum++) {
        unsigned index = distro.sample();
        if (counts.count(index) == 0) counts[index] = 0;
        counts[index] += 1;
    }

    //TO CHECK: 1) counts.size() == # of non-zero entries in weights.
    unsigned nonzero = 0;
    vector<unsigned> nonzeroIndices; 
    for (unsigned index = 0; index < given.size(); index++) {
        if (given[index] < roundOffTolerance) continue;
        nonzero++;
        nonzeroIndices.push_back(index);
    }
    BOOST_CHECK_EQUAL(counts.size(), nonzero);

    //TO CHECK: 2) all non-zero entries' indices are in counts.
    for (unsigned index : nonzeroIndices) {
        BOOST_CHECK_EQUAL(counts.count(index), 1);
    }

    //TO CHECK: 3) sum of values in counts == samples.
    unsigned totalCounts = 0;
    for (unsigned index = 0; index < size; index++) {
        unsigned sum = (counts.count(index) == 0) ? 0 : counts[index];
        totalCounts += sum;
    }
    BOOST_CHECK_EQUAL(totalCounts, samples);


    vector<double> probs = distro.getProbs();

    //TO CHECK: 4) Probs of distribution size() = size.
    BOOST_CHECK_EQUAL(probs.size(), size);

    //TO CHECK: 5) Probs of distribution are equal to weights given.
    BOOST_CHECK_EQUAL_COLLECTIONS(given.begin(), given.end(),
                                  probs.begin(), probs.end());

    //TO CHECK: 6) Probs of distribution all sum to 1.
    double cumProb = 0.0;
    for (unsigned index = 0; index < size; index++) {
        cumProb += probs[index];
    }
    BOOST_CHECK_EQUAL(cumProb, 1.0);

    //TO CHECK: 7) All counts are statistical noise from given distro (hardest)
    //TODO: Fix this.
    //TODO: Pick constant with a statistical basis.
    //vector<double> expected;
    //for (unsigned index = 0; index < size; index++) {
    //    BOOST_CHECK_CLOSE(counts[index], samples * given[index], 150);
    //}
}
