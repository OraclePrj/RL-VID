#ifndef _ACTIONSPACE_HPP_
#define _ACTIONSPACE_HPP_

    enum ActionSpaceType {INT};
    class ActionSpace {
        public:
            //None for now
            ActionSpace(ActionSpaceType type);
            ActionSpaceType getType();
            bool isType(ActionSpaceType type);
        private:
            //None for now
            ActionSpaceType type;
    };


#endif
