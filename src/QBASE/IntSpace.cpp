#include "IntSpace.hpp"

IntSpace::IntSpace() : ActionSpace(INT) {
    granularity = DISCRETE;
    lower = numeric_limits<int>::min();
    upper = numeric_limits<int>::max();
}

IntSpace::IntSpace(Granularity granularity, int lower, int upper) 
                                                        : ActionSpace(INT) {
    this->granularity = granularity;
    this->lower = lower;
    this->upper = upper;
}

Granularity IntSpace::getGranularity() {
    return granularity;
}

int IntSpace::getLower() {
    return lower;
}

int IntSpace::getUpper() {
    return upper;
}
