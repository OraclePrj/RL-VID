#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE QBASE
#include <boost/test/unit_test.hpp>

#include <set>
#include "../Tree/MCT_Tree.hpp"
#include "../Tree/MCT_ActionNode.hpp"
#include "../Tree/MCT_StateNode.hpp"
#include "QBASE.hpp"

/* TODO: Refactor. Also test having two different States. */

BOOST_AUTO_TEST_CASE( QBASE_00 )
{
    //Make sure sampling working correctly ignoring updates
    MCT_Tree tree;
    MCT_Node* root = tree.getRoot();
    MCT_StateNode* state = dynamic_cast<MCT_StateNode*>(root);
    unsigned updatePeriod = 10000;
    QBASE qbase;
    qbase.setUpdatePeriod(updatePeriod);
    map<int, unsigned> counts;
    for (unsigned sampleNo = 0; sampleNo < updatePeriod; sampleNo++) {
       int action = qbase.sample(state);
       if (counts.count(action) == 0) counts[action] = 0;
       counts[action] += 1;
       state->recordVisit();
    }
    //Make sure only considering up to <batch size> actions at once.
    BOOST_CHECK_EQUAL(counts.size(), qbase.getBatchSize());

    int expectedCount = updatePeriod / qbase.getBatchSize();
    int allowedError = 125; //TODO: Make this more scientific

    //Make sure sampling distribution is uniform.
    for (auto& entry : counts) {
        int count = entry.second;
        int error = abs(count - expectedCount);
        BOOST_CHECK(error <= allowedError);
    }
}

BOOST_AUTO_TEST_CASE( QBASE_01 )
{
    //Test updating under uniform rewards.
    MCT_Tree tree;
    MCT_Node* root = tree.getRoot();
    MCT_StateNode* state = dynamic_cast<MCT_StateNode*>(root);
    unsigned updatePeriod = 10000;
    double reward = 1.0;
    QBASE qbase;
    qbase.setUpdatePeriod(updatePeriod);
    map<int, unsigned> counts;
    for (unsigned sampleNo = 0; sampleNo < 2 * updatePeriod; sampleNo++) {
       int action = qbase.sample(state);
       if (counts.count(action) == 0) counts[action] = 0;
       counts[action] += 1;
       MCT_Node* child = state->getChild(action);
       MCT_ActionNode* actionNode = dynamic_cast<MCT_ActionNode*>(child);
       MCT_Node* grandChild = actionNode->getChild(1);
       MCT_StateNode* nextState = dynamic_cast<MCT_StateNode*>(grandChild);
       nextState->assign(reward);
       actionNode->update(reward, nextState);
       state->update(actionNode);
    }
    unsigned batchSize = qbase.getBatchSize();
    unsigned bestSize = qbase.getBestSize();
    //After an update, should only replace <batch size> - <best size> items.
    unsigned expected = batchSize + (batchSize - bestSize);
    BOOST_CHECK_EQUAL(counts.size(), expected);

    //Make sure sampling distribution is uniform.
    int perRoundExpected = updatePeriod / batchSize;
    set<int> oneRounds, twoRounds, weird;
    int allowableError = 175;
    for (auto& entry : counts) {
        int action = entry.first;
        int count = entry.second;
        //Classify each count as appearing to have gone through 1 or 2 rounds
        if (abs(count - 2 * perRoundExpected) <= allowableError) {
            twoRounds.insert(action);
        } else if (abs(count - perRoundExpected) <= allowableError) {
            oneRounds.insert(action);
        } else {
            weird.insert(action);
            cout << "Action: " << action << ", Count: " << count << endl;
            cout << "Allowable error: " << allowableError << endl;
            cout << "2 round: " << abs(count - 2 * perRoundExpected) << endl;
            cout << "1 round: " << abs(count - perRoundExpected) << endl;
        }
    }
    //Expect best_size to have gone through 2 rounds
    //Expect rejects (batchSize - bestSize) from each round to go through 1.
    BOOST_CHECK_EQUAL(twoRounds.size(), bestSize);
    BOOST_CHECK_EQUAL(oneRounds.size(), 2 * (batchSize - bestSize));
    BOOST_CHECK_EQUAL(weird.size(), 0);
}

BOOST_AUTO_TEST_CASE( QBASE_02 )
{
    //Test updating once under non-uniform rewards.
    MCT_Tree tree;
    MCT_Node* root = tree.getRoot();
    MCT_StateNode* state = dynamic_cast<MCT_StateNode*>(root);
    unsigned updatePeriod = 10000;
    double normalReward = 1.0, bestReward = 42.0;
    int bestAction = -1;
    QBASE qbase;
    qbase.setUpdatePeriod(updatePeriod);
    map<int, unsigned> counts;
    for (unsigned sampleNo = 0; sampleNo < 2 * updatePeriod; sampleNo++) {
       int action = qbase.sample(state);
       if (sampleNo == 0) bestAction = action;
       if (counts.count(action) == 0) counts[action] = 0;
       counts[action] += 1;
       MCT_Node* child = state->getChild(action);
       MCT_ActionNode* actionNode = dynamic_cast<MCT_ActionNode*>(child);
       MCT_Node* grandChild = actionNode->getChild(1);
       MCT_StateNode* nextState = dynamic_cast<MCT_StateNode*>(grandChild);
       double reward = (action == bestAction) ? bestReward : normalReward;
       nextState->assign(reward);
       actionNode->update(reward, nextState);
       state->update(actionNode);
    }
    unsigned batchSize = qbase.getBatchSize();
    unsigned bestSize = qbase.getBestSize();
    //After an update, should only replace <batch size> - <best size> items.
    unsigned expected = batchSize + (batchSize - bestSize);
    BOOST_CHECK_EQUAL(counts.size(), expected);

    //Make sure sampling distribution is as expected.
    int firstRoundExpected = updatePeriod / batchSize;
    double newProb = 1.0 / (batchSize - bestSize + batchSize);
    double bestProb = batchSize * newProb;
    int secondRoundExpectedBest = updatePeriod * bestProb;
    int expectedBest = firstRoundExpected + secondRoundExpectedBest;
    int secondRoundExpectedRest = updatePeriod * newProb;
    set<int> best, firstRoundRest, secondRoundRest, weird;
    int allowableError = 150;
    for (auto& entry : counts) {
        int action = entry.first;
        int count = entry.second;
        //Classify each count as appearing to have gone through 1 or 2 rounds
        if (abs(count - expectedBest) <= allowableError) {
            best.insert(action);
        } else if (abs(count - firstRoundExpected) <= allowableError) {
            firstRoundRest.insert(action);
        } else if (abs(count - secondRoundExpectedRest) <= allowableError) {
            secondRoundRest.insert(action);
        } else {
          weird.insert(action);
          cout << "Action: " << action << ", Count: " << count << endl;
          cout << "Allowable error: " << allowableError << endl;
          cout << "Best: " << abs(count - expectedBest) << endl;
          cout << "1st round: " << abs(count - firstRoundExpected) << endl;
          cout << "2nd round: " << abs(count - secondRoundExpectedRest) << endl;
        }
    }
    //Expect only one absolute best
    BOOST_CHECK_EQUAL(best.size(), 1);

    //Expect batchSize - 1 first rounders
    BOOST_CHECK_EQUAL(firstRoundRest.size(), batchSize - 1);

    //Expect batchSize - bestSize in second round.
    BOOST_CHECK_EQUAL(secondRoundRest.size(), batchSize - bestSize);

    //No weird exceptions
    BOOST_CHECK_EQUAL(weird.size(), 0);
}

BOOST_AUTO_TEST_CASE( QBASE_03 )
{
    //Test updating twice under non-uniform rewards.
    MCT_Tree tree;
    MCT_Node* root = tree.getRoot();
    MCT_StateNode* state = dynamic_cast<MCT_StateNode*>(root);
    unsigned updatePeriod = 10000;
    double normalReward = 1.0, bestReward = 42.0;
    int bestAction = -1;
    QBASE qbase;
    qbase.setUpdatePeriod(updatePeriod);
    map<int, unsigned> counts;
    for (unsigned sampleNo = 0; sampleNo < 3 * updatePeriod; sampleNo++) {
       int action = qbase.sample(state);
       if (sampleNo == 0) bestAction = action;
       if (counts.count(action) == 0) counts[action] = 0;
       counts[action] += 1;
       MCT_Node* child = state->getChild(action);
       MCT_ActionNode* actionNode = dynamic_cast<MCT_ActionNode*>(child);
       MCT_Node* grandChild = actionNode->getChild(1);
       MCT_StateNode* nextState = dynamic_cast<MCT_StateNode*>(grandChild);
       double reward = (action == bestAction) ? bestReward : normalReward;
       nextState->assign(reward);
       actionNode->update(reward, nextState);
       state->update(actionNode);
    }
    unsigned batchSize = qbase.getBatchSize();
    unsigned bestSize = qbase.getBestSize();
    //After each update, should only replace <batch size> - <best size> items.
    unsigned expected = batchSize + 2 * (batchSize - bestSize);
    BOOST_CHECK_EQUAL(counts.size(), expected);

    //Make sure sampling distribution is as expected.
    int firstRoundExpected = updatePeriod / batchSize;
    double round2RestProb = 1.0 / (batchSize - bestSize + batchSize);
    double round2BestProb = batchSize * round2RestProb;
    double round3RestProb = 1.0 / (batchSize + 2 * (batchSize - bestSize));
    double round3BestProb = (2 * batchSize - bestSize) * round3RestProb;
    int round2ExpectedBest = updatePeriod * round2BestProb;
    int round3ExpectedBest = updatePeriod * round3BestProb;
    int expectedBest = firstRoundExpected + round2ExpectedBest + 
                                            round3ExpectedBest;
    int round2ExpectedRest = updatePeriod * round2RestProb;
    int round3ExpectedRest = updatePeriod * round3RestProb;
    set<int> best, firstRoundRest, secondRoundRest, thirdRoundRest, weird;
    int allowableError = 150;
    for (auto& entry : counts) {
        int action = entry.first;
        int count = entry.second;
        //Classify each count as appearing to have gone through 1 or 2 rounds
        if (abs(count - expectedBest) <= allowableError) {
            best.insert(action);
        } else if (abs(count - firstRoundExpected) <= allowableError) {
            firstRoundRest.insert(action);
        } else if (abs(count - round2ExpectedRest) <= allowableError) {
            secondRoundRest.insert(action);
        } else if (abs(count - round3ExpectedRest) <= allowableError) {
            thirdRoundRest.insert(action);
        } else {
          weird.insert(action);
          cout << "Action: " << action << ", Count: " << count << endl;
          cout << "Allowable error: " << allowableError << endl;
          cout << "Best: " << abs(count - expectedBest) << endl;
          cout << "1st round: " << abs(count - firstRoundExpected) << endl;
          cout << "2nd round: " << abs(count - round2ExpectedRest) << endl;
          cout << "3rd round: " << abs(count - round3ExpectedRest) << endl;
        }
    }
    //Expect only one absolute best
    BOOST_CHECK_EQUAL(best.size(), 1);

    //Expect batchSize - 1 first rounders
    BOOST_CHECK_EQUAL(firstRoundRest.size(), batchSize - 1);

    //Expect batchSize - bestSize in second round.
    BOOST_CHECK_EQUAL(secondRoundRest.size(), batchSize - bestSize);

    //Expect another batchSize - bestSize in third round.
    BOOST_CHECK_EQUAL(thirdRoundRest.size(), batchSize - bestSize);

    //No weird exceptions
    BOOST_CHECK_EQUAL(weird.size(), 0);
}

BOOST_AUTO_TEST_CASE( QBASE_04 )
{
    //Test exhausting the action space.
    MCT_Tree tree;
    MCT_Node* root = tree.getRoot();
    MCT_StateNode* state = dynamic_cast<MCT_StateNode*>(root);
    unsigned updatePeriod = 10000;
    //Independent of actual action, Qs will always be increasing as assigned
    double increment = 100;
    double nextReward = increment;
    map<int, double> actionRewards;
    map<int, unsigned> counts;
    map<double, int> getAction;

    //Create a random action space just big enough for one batch.
    Random random;
    QBASE throwaway;
    int length = throwaway.getBatchSize();
    int start = random.integer();
    while (start > numeric_limits<int>::max() - length) {
        start = random.integer();
    }
    IntSpace space(start, start + length - 1);
    QBASE qbase(space);
    qbase.setUpdatePeriod(updatePeriod);

    for (unsigned sampleNo = 0; sampleNo < 9 * updatePeriod; sampleNo++) {
        if (sampleNo % updatePeriod == 0) {
            vector<int> batch = qbase.getBatch(state);
            map<double, unsigned> rewardCounts;
            for (auto& entry : actionRewards) {
                int action = entry.first;
                double reward = entry.second;
                rewardCounts[reward] = counts[action];
            }
            unsigned batchSize = qbase.getBatchSize();
            if (sampleNo / updatePeriod == 1) {
                // Just before 1st update.
                // Should have 1 reward value for each of:
                // increment, 2 * increment, ..., batchSize * increment.
                BOOST_CHECK_EQUAL(rewardCounts.size(), batchSize);
                double checkingSize = increment;
                for (unsigned index = 0; index < batchSize; index++) {
                    BOOST_CHECK_EQUAL(rewardCounts.count(checkingSize), 1);
                    checkingSize += increment;
                }

                //Counts should be approx uniform.
                unsigned expectedCounts = updatePeriod / batchSize;
                int allowedError = 150;
                for (int action : batch) {
                    int error = abs(expectedCounts - counts[action]);
                    BOOST_CHECK(error <= allowedError);
                }
            } else if (sampleNo / updatePeriod >= 2) {
                // After first update.
                // First confirm still correct batchSize values.
                BOOST_CHECK_EQUAL(rewardCounts.size(), batchSize);
                double checkingSize = increment;
                for (unsigned index = 0; index < batchSize; index++) {
                    BOOST_CHECK_EQUAL(rewardCounts.count(checkingSize), 1);
                    checkingSize += increment;
                }

                // Counts should NOT be uniform, but have to calculate.
                vector<double> expectedCounts;
                for (unsigned index = 0; index < batchSize; index++) {
                    //Start with uniform distribution from first period.
                    expectedCounts.push_back(updatePeriod / batchSize);
                }
                // Calculate expected proportions after 1 update
                vector<double> Qs;
                for (unsigned factor = 1; factor <= batchSize; factor++) {
                    Qs.push_back(increment * factor);
                }
                double Qmin = increment;
                double Qmax = batchSize * increment;
                double range = Qmax - Qmin;

                double totalWeights = 0.0;
                vector<double> weights;
                for (double Qcurrent : Qs) {
                    double weight = (Qcurrent - Qmin) / range;
                    totalWeights += weight;
                    weights.push_back(weight);
                }

                for (unsigned index = 0; index < batchSize; index++) {
                    double weight = weights[index];
                    double prob = weight / totalWeights;
                    //Find number of iterations since first (uniform) update
                    unsigned iterations = sampleNo - updatePeriod;

                    //Add expected # of visits for this value to counts.
                    expectedCounts[index] += prob * iterations;
                }

                //Confirm within allowable error.
                int allowableError = sampleNo / 100;
                for (unsigned index = 0; index < batchSize; index++) {
                    double reward = Qs[index];
                    int expectedCount = expectedCounts[index];
                    int observedCount = rewardCounts[reward];

                    int error = abs(expectedCount - observedCount);
                   
                    BOOST_CHECK(error <= allowableError);
                    if (error > allowableError) {
                        cout << "Failed. Error: " << error << endl;
                        cout << "Reward: " << reward << endl;
                        cout << "Expected count: " << expectedCount << endl;
                        cout << "Observed count: " << observedCount << endl;
                    }
                }
            }
        }
        int action = qbase.sample(state);
        if (counts.count(action) == 0) { 
           counts[action] = 0;
           actionRewards[action] = nextReward;
           getAction[nextReward] = action;
           nextReward += increment;
        }
        counts[action] += 1;
        MCT_Node* child = state->getChild(action);
        MCT_ActionNode* actionNode = dynamic_cast<MCT_ActionNode*>(child);
        MCT_Node* grandChild = actionNode->getChild(1);
        MCT_StateNode* nextState = dynamic_cast<MCT_StateNode*>(grandChild);
        double reward = actionRewards[action];
        nextState->assign(reward);
        actionNode->update(reward, nextState);
        state->update(actionNode);
    }
}
