#ifndef _QBASE_HPP_
    #define _QBASE_HPP_

    #include <algorithm>
    #include <iostream>
    #include <map>
    #include <string>
    #include <vector>

    #include "../Simulator/Random.hpp"
    #include "../Tree/MCT_ActionNode.hpp"
    #include "../Tree/MCT_Node.hpp"
    #include "../Tree/MCT_StateNode.hpp"
    #include "ActionSampler.hpp"
    #include "ProbabilityDistribution.hpp"

    using namespace std;

    class QBASE {
        public:
            QBASE(unsigned updatePeriod);
            //vector<double> getBatchProbs(MCT_StateNode* state);
            unsigned getBatchSize();
            unsigned getBestSize();
            int sample(MCT_StateNode* state); //Change int to string
        private:
            unsigned batchSize;
            unsigned bestSize;
            double beta;
            double unvisitedProb;
            unsigned updatePeriod;

            ActionSampler sampler;
            Random random;
            map<MCT_StateNode*, vector<int>> batches; //Change int to string
            map<MCT_StateNode*, ProbabilityDistribution> distribution;
            map<MCT_StateNode*, vector<int>> unvisited; //Change int to string

            static bool byValue(MCT_Node* action1, MCT_Node* action2);
            double doubt(MCT_Node* action); //alpha_b
            void saveChanges(MCT_StateNode* state, vector<int>& batch,
                        vector<double>& batchProbs, vector<int>& newActions);
            void update(MCT_StateNode* state);
            void updateUnvisited(MCT_StateNode* state, vector<int>& batch,
                                 vector<double>& batchProbs,
                                 map<int, MCT_Node*>& visited,
                                 vector<int>& newActions); //int->string
            void updateVisited(MCT_StateNode* state, vector<int>& batch,
                               vector<double>& batchProbs,
                               map<int, MCT_Node*>& visited); //int->str
    };

#endif
