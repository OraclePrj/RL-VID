#include "QBASE.hpp"

QBASE::QBASE() { //unsigned updatePeriod) {
    /* batchSize: the size of batch to sample from
     * bestSize: top <bestSize> actions are kept in batch after updates
     * beta: constant for doubt/alpha_b calculation
     * maxActions: a cap on # of actions to try in a given state
     * updatePeriod: Resample batch every <updatePeriod> visits */
    batchSize = 5;
    bestSize = 3;
    beta = 5.0; //Erli recommends 5-10
    unvisitedProb = 1.0 / 10000; //I think I may not need this.
    updatePeriod = 1000;
}

QBASE::QBASE(IntSpace& space) : sampler(space) { //unsigned updatePeriod, space
    /* batchSize: the size of batch to sample from
     * bestSize: top <bestSize> actions are kept in batch after updates
     * beta: constant for doubt/alpha_b calculation
     * maxActions: a cap on # of actions to try in a given state
     * updatePeriod: Resample batch every <updatePeriod> visits */
    batchSize = 5;
    bestSize = 3;
    beta = 5.0; //Erli recommends 5-10
    unvisitedProb = 1.0 / 10000; //I think I may not need this.
    updatePeriod = 1000;
}

vector<int> QBASE::getBatch(MCT_StateNode* state) { return batches[state]; }

unsigned QBASE::getBatchSize() { return batchSize; }
unsigned QBASE::getBestSize() { return bestSize; }
unsigned QBASE::getUpdatePeriod() { return updatePeriod; }

void QBASE::setBatchSize(unsigned newSize) { 
    batchSize = newSize; 
}

void QBASE::setBestSize(unsigned newSize) { 
    bestSize = newSize; 
}

void QBASE::setSpace(IntSpace& space) {
    sampler.setSpace(space);
}

void QBASE::setUpdatePeriod(unsigned newPeriod) { 
    updatePeriod = newPeriod; 
}

int QBASE::sample(MCT_StateNode* state) {
    //Perform update at regular frequency
    auto distroEntry = distribution.find(state);
    bool noDistro = distroEntry == distribution.end();
    if (noDistro || state->getVisits() % updatePeriod == 0) {
        //This also initializes if first visit
        update(state);
        distroEntry = distribution.find(state);
    }

    //Choose action
    int chosen = -1;
    /*vector<int>& unvisitedActions = unvisited[state];
    if (unvisitedActions.size() > 0) {
        //This forces unvisited actions to be tried first
        chosen = unvisitedActions.back();
        unvisitedActions.pop_back();
    } else {*/
        //Get distribution associated with state
        vector<int> batch = batches[state];
        ProbabilityDistribution& distro = distroEntry->second;
        unsigned index = distro.sample();
        chosen = batch[index];
    //}

    return chosen;
}

bool QBASE::byValue(MCT_Node* action1, MCT_Node* action2) {
    return action1->value() > action2->value();
}

double QBASE::doubt(MCT_Node* action) {
    //Called alpha_b in paper.
    unsigned visits = action->getVisits();
    return visits / (visits + beta);
}

void QBASE::update(MCT_StateNode* state) {
    /* batch: Set of values to choose from next time.
     * batchProbs: Sampling probabilities for each value.
     * visited: All values we've tried before (and ref to their tree nodes) */
    vector<int> batch;
    map<int, MCT_Node*> visited;
    map<int, double> probs;
        
    //Populates above variables with info about visited actions
    updateVisited(state, batch, probs, visited);

    //unvisited: a list of the values we haven't tried before.
    vector<int> newActions;

    //Updates all above variables to add unvisited part of batch
    updateUnvisited(state, batch, probs, visited, newActions);
    saveChanges(state, batch, probs, newActions);
}

void QBASE::saveChanges(MCT_StateNode* state, vector<int>& batch,
                        map<int, double>& probs, vector<int>& newActions) {
    batches[state] = batch;
    if (distribution.find(state) != distribution.end()) {
        distribution.erase(state);
    }
    vector<double> batchProbs;
    for (int action : batch) {
        batchProbs.push_back(probs[action]);
    }

    ProbabilityDistribution samplingDistro(random, batchProbs);
    distribution.insert(make_pair(state, samplingDistro));
    unvisited[state] = newActions;
}

void QBASE::updateUnvisited(MCT_StateNode* state, vector<int>& batch,
                            map<int, double>& probs,
                            map<int, MCT_Node*>& visited,
                            vector<int>& newActions) {
    //Append to (possibly empty) batch until reaches batchSize
    sampler.sampleSet(batch, batchSize);

    //batchProbs has a probability for each VISITED action
    //Still need to add probabilities for unvisited ones
    for (unsigned index = 0; index < batchSize; index++) {
        int action = batch[index];
        if (visited.count(action) == 0) {
            //This action value hasn't been tried before: add new prob
            newActions.push_back(action);
            probs[action] = unvisitedProb;
        }
    }
}

void QBASE::updateVisited(MCT_StateNode* state, vector<int>& batch,
                          map<int, double>& probs,
                          map<int, MCT_Node*>& visited) {
    vector<MCT_Node*> actions;
    for (auto& idNodePair : state->getChildren()) {
        actions.push_back(idNodePair.second);
    }
    unsigned visitCount = actions.size();
    if (visitCount > 0) {
        //Sort ActionNodes by the value of taking that action
        sort(actions.begin(), actions.end(), byValue);

        //Calculating action values might be expensive, so cache? (May remove)
        vector<double> Qs;
        for (MCT_Node* action : actions) {
            Qs.push_back(action->value());
        }

        //Can now instantly get Qmin/Qmax since Qs is sorted
        double Qmax = Qs[0];
        double Qmin = Qs[visitCount - 1];
        double range = Qmax - Qmin;
        unsigned bestCount = min(bestSize, visitCount);
        //Special case: if range == 0, bad things happen
        if (range == 0) {
            //All Qs are equal, so pick any best_size, it doesn't matter.
            for (unsigned index = 0; index < bestCount; index++) {
                MCT_Node* action = actions[index];
                int id = action->getId();
                batch.push_back(id);
                probs[id] = unvisitedProb;
                visited[id] = action;
            }
            return;
        }

        //Pass over all actions to calculate weights (and remember as visited)
        double totalWeights = 0.0;
        vector<double> weights;
        for (unsigned index = 0; index < visitCount; index++) {
            //Mark action value as visited
            MCT_Node* action = actions[index];
            visited[action->getId()] = action;

            //Calculate weight for this action and add to running total
            double weight = doubt(action) * (Qs[index] - Qmin) / range;
            weights.push_back(weight);
            totalWeights += weight;
        }

        //scale: product of normalize term: 1 / sum(weights)
        //        & fraction of prob. mass: (actions seen) / maxActions 
        double scale = unvisitedProb * visitCount / totalWeights; //TODO: split

        //Save first (up to) bestSize actions and their probabilities
        for (unsigned index = 0; index < visitCount; index++) {
            int id = actions[index]->getId();
            if (index < bestCount) {
                batch.push_back(id);
            }
            probs[id] = scale * weights[index];
        }
    }
}
