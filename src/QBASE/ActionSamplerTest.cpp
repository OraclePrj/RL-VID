#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE QBASE
#include <boost/test/unit_test.hpp>

#include <set>

#include "ActionSampler.hpp"

unsigned TRIALS = 10000;

void checkDuplicates(vector<int>& sampled) {
    set<int> seen;
    for (unsigned index = 0; index < sampled.size(); index++) {
        int currentAction = sampled[index];
        //Should not have seen current action before
        BOOST_CHECK_EQUAL(seen.count(currentAction), 0);
        seen.insert(currentAction);
    }
}

void checkInvariant(vector<int>& sampled, unsigned target_size) {
    //Check set is proper size
    BOOST_CHECK_EQUAL(sampled.size(), target_size);
    checkDuplicates(sampled);
}

BOOST_AUTO_TEST_CASE( ActionSampler_00 )
{
    //Check to ensure ActionSampler fills empty set
    //to proper size with no duplicates.
    ActionSampler sampler;
    unsigned batch_size = 20;

    for (unsigned trial = 0; trial < TRIALS; trial++) {
        vector<int> empty;
        sampler.sampleSet(empty, batch_size);
        checkInvariant(empty, batch_size);
    }
}

BOOST_AUTO_TEST_CASE( ActionSampler_01 )
{
    //Check to ensure ActionSampler fills partial set
    //to proper size with no duplicates.
    ActionSampler sampler;
    unsigned batch_size = 20;

    for (unsigned trial = 0; trial < TRIALS; trial++) {
        vector<int> partial = {1, 2, 3};
        sampler.sampleSet(partial, batch_size);
        checkInvariant(partial, batch_size);
    }
}

BOOST_AUTO_TEST_CASE( ActionSampler_02 )
{
    //Check to ensure ActionSampler does nothing to full set
    ActionSampler sampler;
    unsigned batch_size = 3;
    vector<int> original = {1, 2, 3};

    vector<int> full;
    for (int element : original) {
        full.push_back(element);
    }
    sampler.sampleSet(full, batch_size);
    checkInvariant(full, batch_size);
    BOOST_CHECK_EQUAL_COLLECTIONS(original.begin(), original.end(),
                                      full.begin(),     full.end());
}

BOOST_AUTO_TEST_CASE( ActionSampler_03 )
{
    //Check to ensure ActionSampler does nothing to set that's too large
    ActionSampler sampler;
    unsigned batch_size = 3;
    vector<int> original = {1, 2, 3, 4, 5};

    vector<int> full;
    for (int element : original) {
        full.push_back(element);
    }
    sampler.sampleSet(full, batch_size);
    checkDuplicates(full);
    BOOST_CHECK_EQUAL_COLLECTIONS(original.begin(), original.end(),
                                      full.begin(),     full.end());
}
