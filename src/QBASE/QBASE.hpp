#ifndef _QBASE_HPP_
    #define _QBASE_HPP_

    #include <algorithm>
    #include <iostream>
    #include <map>
    #include <string>
    #include <vector>

    #include "../Simulator/Random.hpp"
    #include "../Tree/MCT_ActionNode.hpp"
    #include "../Tree/MCT_Node.hpp"
    #include "../Tree/MCT_StateNode.hpp"
    #include "ActionSampler.hpp"
    #include "IntSpace.hpp"
    #include "ProbabilityDistribution.hpp"

    using namespace std;

    class QBASE {
        public:
            QBASE();
            QBASE(IntSpace& space); //Change Int to Str
            vector<int> getBatch(MCT_StateNode* state); //Change int to string
            unsigned getBatchSize();
            unsigned getBestSize();
            unsigned getUpdatePeriod();
            int sample(MCT_StateNode* state); //Change int to string
            void setBatchSize(unsigned newSize);
            void setBestSize(unsigned newSize);
            void setSpace(IntSpace& space);
            void setUpdatePeriod(unsigned newPeriod);
            
        private:
            unsigned batchSize;
            unsigned bestSize;
            double beta;
            double unvisitedProb;
            unsigned updatePeriod;

            ActionSampler sampler;
            Random random;
            map<MCT_StateNode*, vector<int>> batches; //Change int to string
            map<MCT_StateNode*, ProbabilityDistribution> distribution;
            map<MCT_StateNode*, vector<int>> unvisited; //Change int to string

            static bool byValue(MCT_Node* action1, MCT_Node* action2);
            double doubt(MCT_Node* action); //alpha_b
            void saveChanges(MCT_StateNode* state, vector<int>& batch,
                        map<int, double>& probs, vector<int>& newActions);
            void update(MCT_StateNode* state);
            void updateUnvisited(MCT_StateNode* state, vector<int>& batch,
                                 map<int, double>& probs,
                                 map<int, MCT_Node*>& visited,
                                 vector<int>& newActions); //int->string
            void updateVisited(MCT_StateNode* state, vector<int>& batch,
                               map<int, double>& probs,
                               map<int, MCT_Node*>& visited); //int->str
    };

#endif
