#ifndef _PROBABILITYDISTRIBUTION_HPP_
    #define _PROBABILITYDISTRIBUTION_HPP_

    #include <iostream>
    #include <sstream>
    #include <string>
    #include <vector>
    #include "../Simulator/Random.hpp"
    #include "../Utils/StrOps.hpp"

    using namespace std;

    class ProbabilityDistribution {
        public:
/*
            ProbabilityDistribution(const ProbabilityDistribution& other);
            ProbabilityDistribution(ProbabilityDistribution&& other);
*/
            ProbabilityDistribution(Random& random, unsigned size);
            ProbabilityDistribution(Random& random, vector<double>& weights);
            vector<double> getProbs() const;
            vector<double> normalize(vector<double>& weights);
/*
            ProbabilityDistribution& operator=(const ProbabilityDistribution& other);
            ProbabilityDistribution& operator=(ProbabilityDistribution&& other);
*/
            unsigned sample();
            void setWeights(vector<double>& weights);
            string toString() const;
        private:
            Random* random;
            vector<double> distribution;
    };

#endif
