#ifndef _ACTIONSAMPLER_HPP_
     #define _ACTIONSAMPLER_HPP_

     #include <algorithm>
     #include <vector>
     #include "../Simulator/Random.hpp"
     #include "IntSpace.hpp"

     using namespace std;

     class ActionSampler {
          public:
              ActionSampler();
              ActionSampler(IntSpace& space);
              void sampleSet(vector<int>& batch, unsigned batch_size);
              void setSpace(IntSpace& space);
          private:
              IntSpace space;
              Random random;
     };

#endif
