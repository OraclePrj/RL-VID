#include "ProbabilityDistribution.hpp"

/*
ProbabilityDistribution::ProbabilityDistribution(const ProbabilityDistribution& other) { //Copy constructor
    this->random = other.random;
    this->distribution = other.distribution;
}

ProbabilityDistribution::ProbabilityDistribution(ProbabilityDistribution&& other) { //move constructor
    this->random = other.random;
    this->distribution = other.distribution;
}

ProbabilityDistribution& ProbabilityDistribution::operator=(ProbabilityDistribution&& other) { //move assignment
    this->random = other.random;
    this->distribution = other.distribution;
    return *this;
}

ProbabilityDistribution& ProbabilityDistribution::operator=(const ProbabilityDistribution& other) { //copy assignment
    this->random = other.random;
    this->distribution = other.distribution;
    return *this;
}
*/

ProbabilityDistribution::ProbabilityDistribution(Random& random, 
                                                 unsigned size) {
    //Constructs with uniform distribution of size "size"
    this->random = &random;

    //Uniform distribution: all indices have same weight.
    vector<double> weights;
    for (unsigned index = 0; index < size; index++) {
        weights.push_back(1);
    }

    setWeights(weights);
}

ProbabilityDistribution::ProbabilityDistribution(Random& random, 
                                                 vector<double>& weights) {
    //Constructs with distribution based on given weights.
    this->random = &random;
    setWeights(weights);
}

vector<double> ProbabilityDistribution::getProbs() const {
    vector<double> copy;
    for (double prob : distribution) {
        copy.push_back(prob);
    }
    return copy;
}

vector<double> ProbabilityDistribution::normalize(vector<double>& weights) {
    //Efficiently normalizes weights to make a probability distribution
    unsigned weightCount = weights.size();

    //First pass: forwards, summing
    double weightSum = 0.0;
    for (unsigned index = 0; index < weightCount; index++) {
        weightSum += weights[index];
    }
        
    //Second pass: reverse over all but first entry, normalizing by sum.
    double allButFirstSum = 0.0;
    for (unsigned index = weightCount - 1; 0 < index; index--) {
        weights[index] /= weightSum;
        allButFirstSum += weights[index];
    }

    //Deliberately skipped first element: it is 1 - sum of all the rest.
    weights[0] = 1 - allButFirstSum;
    return weights;
}

unsigned ProbabilityDistribution::sample() {
    //Returns an index representing the item to be sampled.

    //Sample cdf value
    double probability = 0.0;
    do {
        probability = random->real(0.0, 1.0);
    } while (probability == 0.0); //prob of exactly 0: unlikely but breaks this

    //Find index corresponding to cdf value
    unsigned index = 0;
    double cumulative = distribution[index];
    while (cumulative < probability) {
        cumulative += distribution[++index];
    }

    return index;
}

void ProbabilityDistribution::setWeights(vector<double>& weights) {
    //Imports weights to replace current distribution.
    vector<double> newDistro = normalize(weights);

    distribution.clear();
    for (double prob : newDistro) {
        distribution.push_back(prob);
    }
}

string ProbabilityDistribution::toString() const {
    ostringstream builder;
    builder << distribution;
    return builder.str();
}
