#!/bin/bash

# Define number of times to run each parameter setting for
RUN_COUNT=30

# Define parameters to iterate over
BATCH=(1 2 3 4 5 6 7 8 9 10 11)
BEST=(1 2 3 4 5 6 7 8 9 10 11)
DISCOUNT=(1 2 3 4 5 6 7 8 9 10 11)
UPDATE=(1 2 3 4 5 6 7 8 9 10 11)

# Calculate sizes
BATCH_COUNT=${#BATCH[@]}
BEST_COUNT=${#BEST[@]}
DISCOUNT_COUNT=${#DISCOUNT[@]}
UPDATE_COUNT=${#UPDATE[@]}

# Calculate number of parameter ids needed for above parameters
PARAM_COUNT=$(( $BATCH_COUNT * $BEST_COUNT * $DISCOUNT_COUNT * $UPDATE_COUNT ))
echo "Total parameters: $PARAM_COUNT"

# Determine amount of processes user is running (subtract 1: table heading).
RUNNING=$(( $(ps -u $(whoami) | wc -l) - 1 ))
PROCESS_LIMIT=$(ulimit -u)

# Assuming launched processes spawn off some multiple of subprocesses,
# find the number of processes to safely launch while remaining within limit
SAFETY_FACTOR=8
CAN_LAUNCH=$(( ($PROCESS_LIMIT - $RUNNING) / $SAFETY_FACTOR ))


# Start a new process for each parameter id
for ((ID = 0; ID < PARAM_COUNT; ID++))
do
    # Assign a index to this parameter
    PROCESS_INDEX=$(( ID % CAN_LAUNCH ))

    # Calculate appropriate parameter indices for this id
    BATCH_INDEX=$((ID % BATCH_COUNT))
    let LEFTOVER=$((ID / BATCH_COUNT))
    BEST_INDEX=$((LEFTOVER % BEST_COUNT))
    let LEFTOVER=$((LEFTOVER / BEST_COUNT))
    DISCOUNT_INDEX=$((LEFTOVER % DISCOUNT_COUNT))
    UPDATE_INDEX=$((LEFTOVER / DISCOUNT_COUNT))

    # Dereference each array by appropriate index
    BATCH_SIZE=$(( ${BATCH[$BATCH_INDEX]} ))
    BEST_SIZE=$(( ${BEST[$BEST_INDEX]} ))
    DISCOUNT_SIZE=$(( ${DISCOUNT[$DISCOUNT_INDEX]} ))
    UPDATE_SIZE=$(( ${UPDATE[$UPDATE_INDEX]} ))

    # If we have launched most we can launch
    if [[ $ID -gt $CAN_LAUNCH ]]
    then
        OLD_PID=${PROCESSES[$PROCESS_INDEX]}
        wait $OLD_PID # Wait for 1 child process to end to free up process slot
    fi

    # Launch test program in background with parameter settings
    ./test_parameter.sh -i $ID --batch $BATCH_SIZE --best $BEST_SIZE -d $DISCOUNT_SIZE -r $RUN_COUNT -u $UPDATE_SIZE &
    
    # Record the launched PID in process array.
    PROCESSES[$PROCESS_INDEX]=$!

done
