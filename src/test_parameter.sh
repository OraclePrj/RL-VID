#!/bin/bash

#Get command line arguments.
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        --batch)
        BATCH="$2"
        shift # past argument
        shift # past value
        ;;
        --best)
        BEST="$2"
        shift # past argument
        shift # past value
        ;;
        -d|--discount)
        DISCOUNT="$2"
        shift # past argument
        shift # past value
        ;;
        -i|--id)
        ID="$2"
        shift # past argument
        shift # past value
        ;;
        -r|--runs)
        RUN_COUNT="$2"
        shift # past argument
        shift # past value
        ;;
        -u|--update)
        UPDATE="$2"
        shift # past argument
        shift # past value
        ;;
    esac
done

# Ensure all variables have been defined.
# TODO: add discount
if [[ -v BATCH && -v BEST && -v ID && -v RUN_COUNT && -v UPDATE ]]
then
	# Success!
        for ((RUN_ID = 0; RUN_ID < RUN_COUNT; RUN_ID++))
        do
            # Execute MCTS
             ./main --batch $BATCH --best $BEST --id $ID --update $UPDATE 1>out$ID.log 2>err$ID.log

            # Summarize run info
	     python3 analyze.py $ID $RUN_ID
            
            echo Completed run $(($RUN_ID + 1)) of $RUN_COUNT for parameter $ID.
        done
        
        # Clean up logs
        cp log$ID.log log.log
        rm log$ID.log
        rm out$ID.log
        rm err$ID.log
else
	# Failure. :(
        echo "Failure. Missing command-line arguments."
fi
