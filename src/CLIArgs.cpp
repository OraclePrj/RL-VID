#include "CLIArgs.hpp"

CLIArgs::CLIArgs(int argc, char** argv) {
    unsigned dummy = numeric_limits<unsigned>::max();
    batchSize = dummy;
    bestSize = dummy;
    parameterId = dummy;
    updatePeriod = dummy;

    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
        ("batch", po::value<unsigned>(), "# of actions in batch")
        ("best", po::value<unsigned>(),  "# of best Qs to maintain in batch")
        ("id", po::value<unsigned>(), "ID of parameter set being executed")
        ("update", po::value<unsigned>(), "# of visits before each update")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    unsigned seen = 0;

    if (vm.count("batch")) {
        batchSize = vm["batch"].as<unsigned>();
        seen++;
    }
    if (vm.count("best")) {
        bestSize = vm["best"].as<unsigned>();
        seen++;
    }
    if (vm.count("id")) {
        parameterId = vm["id"].as<unsigned>();
        seen++;
    }
    if (vm.count("update")) {
        updatePeriod = vm["update"].as<unsigned>();
        seen++;
    }

    if (seen == 0) {
        success = false;
    } else if (seen == 4) {
        success = true;
    } else {
        cerr << "Invalid arguments." << endl;    
        exit(1);
    }
}

bool CLIArgs::exist() { return success; }
unsigned CLIArgs::getBatchSize() { return batchSize; }
unsigned CLIArgs::getBestSize() { return bestSize; }
unsigned CLIArgs::getParameterId() { return parameterId; }
unsigned CLIArgs::getUpdatePeriod() { return updatePeriod; }
