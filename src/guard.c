#include <stdio.h>
#include <stdlib.h>

unsigned* getPtr(unsigned value) {
    unsigned* result = malloc(sizeof(unsigned));
    *result = value / 2; //UDiv
    *result = *result % 7; //URem
    *result = *result >> 3; // Logical shift right
    return result;
}

int getOffset(int argument) {
    argument *= 7; //Mul
    argument /= 3; //SDiv
    return argument >> 2; //Arithmetic shift right
}

int main(int argc, char* argv[]) {
   int length = 42;
   int array[length];
   int index = 1;
   int* ptr = (int*) getPtr(argc);//pointer cast
   float sum = 1.0/3.0; // Float division
   for (int ii = 0; ii < length; ii++) {
       if (ii % 7 == 0) {
           sum -= 2; //float subtraction
           index--; // int subtraction
       }
       *ptr += 1;
       sum *= 3; //FMul
       if (ii % 3 == 0) {
           sum += 2; //float addition
           array[index++] = getOffset(ii) << 1; //Shift left
       }
   }
   free(ptr);

   return array[0];
}
