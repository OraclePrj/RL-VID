from .TreeNode import TreeNode

class Tree:
    def __init__(self, time_step):
        #Preprocess best paths so we know which nodes to select
        selected = set()
        for path in time_step.selected.paths:
            for last in range(len(path)):
                selected.add(",".join(path[:last+1]))

        self.root = None
        created = {}
        for node in time_step.tree:
            parent, node_id = node.parent, node.id
            if node_id in selected:
                node.select()
            child_node = TreeNode(node)
            created[node_id] = child_node      
            if parent:
                created[parent].add_child(child_node)
            elif self.root is None:
                self.root = child_node
            else:
                raise Exception("Multiple roots in tree!")

