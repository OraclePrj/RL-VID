from .ActionNode import ActionNode

class TreeNode:
    def __init__(self, node):
        self.best = node.selected
        self.children = []
        self.id = node.label
        self.parent = None
        self.type = "Action" if isinstance(node, ActionNode) else "State"
        self.value = node.value
        self.visits = node.visits

    def add_child(self, other):
        if other.parent is None:
            other.parent = self
            self.children.append(other)
        else:
            msg = "Cannot add {} as parent of {}! Parent exists!" 
            raise Exception(msg.format(self.id, other.id))

