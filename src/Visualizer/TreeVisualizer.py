from .Tree    import Tree
from .LogFile import LogFile

class NoStepException(Exception):
    pass

class TreeVisualizer:
    def __init__(self, filename):
        log = LogFile(filename)
        self.steps = {}
        first, last = None, None
        for step in log.steps:
            tree = Tree(step)
            step_no = step.no
            if first is None or step_no < first:
                first = step_no
            if last is None or last < step_no:
                last = step_no
            self.steps[step_no] = tree
        self.range = first, last

    def get_range(self):
        return self.range
