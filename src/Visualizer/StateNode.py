class StateNode:
    def __init__(self, stream):
        #Format: Path,from,root,to,node;visits;value
        line = stream.readline()
        tokens = [token.strip() for token in line.split(';')]
        path, visits, value = tokens
        nodes = [node.strip() for node in path.split(',')]
        self.id = ','.join(nodes)
        self.parent, self.label = ','.join(nodes[:-1]), nodes[-1]
        if not self.parent:
            self.parent = None
        if self.label == "s1":
            self.label = "sCrashed"
        elif self.label == "s2":
            self.label = "sExited"
        self.visits = int(visits)
        self.value = float(value)
        self.selected = False

    def is_in(stream):
        line = stream.peek_line()
        try:
            path, line = line.split(';', 1)
        except ValueError:
            return False
        
        path = path.strip()
        nodes = [node.strip() for node in path.split(',')]
        return nodes[-1].startswith('s')

    def get_html(self):
        table = '<<table border="{}" cellborder="1" cellspacing="0">{}</table>>'
        row = '<tr>{}</tr>'
        col = '<td{}>{}</td>'
        row1 = row.format(col.format(' colspan="2"', self.label))
        row2 = row.format(col.format('', "Visits") + 
                          col.format('', "Value"))
        row3 = row.format(col.format('', self.visits) + 
                          col.format('', self.value))
        border = 2 if self.selected else 0
        return table.format(border, row1 + row2 + row3)

    def select(self):
        self.selected = True
