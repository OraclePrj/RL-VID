class LogStream:
    def __init__(self, filename):
        self._stream = open(filename)

    def at_end(self):
        return self.peek_line() is None

    def close(self):
        return self._stream.close()

    def peek_line(self):
        start_position = self._stream.tell()
        line = self._stream.readline()
        self._stream.seek(start_position)
        return None if line == "" else line

    def readline(self):
        return self._stream.readline()

    def read_value(self):
        """Reads a value from the stream of the form 'label value'. """
        line = self.readline()
        line = line.strip()
        __, value = line.split()
        return value

    def skip_to_next(self, cls):
        self.skip_to_token(cls.START_TOKEN)

    def skip_to_token(self, token):
        line = self.peek_line()
        while line:
            if line.startswith(token):
                #Advanced to correct line
                break
            #Skip line
            self.readline()    

            line = self.peek_line()

