from graphviz import Digraph

class Graph:
    def __init__(self, time_step):
        #Converts timeStep into a linked-together tree
        step_label = "step{}".format(time_step.no)
        self.graph = Digraph(step_label, node_attr = {'shape':'record'})

        #Preprocess best paths so we know which nodes to select
        selected = set()
        for path in time_step.selected.paths:
            for last in range(len(path)):
                selected.add(",".join(path[:last+1]))

        edges = []
        for node in time_step.tree:
            if node.id in selected:
                node.select()
            self.graph.node(node.id, node.get_html())
            if node.parent:
                edges.append((node.parent, node.id))

        self.graph.edges(edges)

    def view(self):
        self.graph.view()
"""
Graphviz format:

Add node: unique identifier, label
Add edge: 


"""
