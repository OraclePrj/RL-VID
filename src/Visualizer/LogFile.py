from .LogStream import LogStream
from .TimeStep  import  TimeStep

class LogFile:
    def __init__(self, filename):
        stream = LogStream(filename)
        self.steps = []
        while not stream.at_end():
            self.steps.append(TimeStep(stream))
        stream.close()

