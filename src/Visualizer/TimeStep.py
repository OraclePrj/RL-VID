from  .StateNode import  StateNode
from .ActionNode import ActionNode

class TimeStep:
    START_TOKEN = "Step"
    def __init__(self, stream):
        self._read_step_no(stream)

        self.tree = []
        #Read TreeNode lines until hitting BestPaths.
        while not stream.at_end():
            next_line = stream.peek_line().strip()
            if next_line.startswith(BestPaths.START_TOKEN):
                break
            self._read_tree_node(stream)
        self.selected = BestPaths(stream)

    def _read_step_no(self, stream):
        line = stream.readline()
        tokens = [token.strip() for token in line.split()]
        if len(tokens) != 2 or tokens[0] != "Step":
            raise Exception("Invalid step line: " + line)
        try:
            self.no = int(tokens[1])
        except ValueError:
            raise Exception("Invalid step value: " + tokens[1])

    def _read_tree_node(self, stream):
        if StateNode.is_in(stream):
            self.tree.append(StateNode(stream))
        elif ActionNode.is_in(stream):
            self.tree.append(ActionNode(stream))
        elif stream.peek_line().strip() == "":
            stream.readline()
        else:
            raise Exception("Invalid tree node: " + stream.peek_line())

from .BestPaths import BestPaths
