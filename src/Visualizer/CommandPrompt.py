from .Commands import CommandParser

class CommandPrompt:
    def __init__(self, prompt_string):
        self.prompt_string = prompt_string

    def handle_error(self, error):
        raise NotImplementedError

    def process_command(self, command):
        raise NotImplementedError

    def start(self):
        self.looping = True
        self._main_loop()

    def stop(self):
        self.looping = False

    def _main_loop(self):
        while self.looping:
            raw_text = input(self.prompt_string)
            try:
                command = CommandParser.parse(raw_text)
                self.process_command(command)
            except Exception as error:
                self.handle_error(error)
