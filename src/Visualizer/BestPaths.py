from .TimeStep import TimeStep

class BestPaths:
    START_TOKEN = "Best Paths:"

    def __init__(self, stream):
        self.paths = []
        line = stream.readline().strip()
        if line != "Best Paths:":
            raise Exception("Invalid BestPaths line: " + line)
        while not stream.at_end():
            next_line = stream.peek_line().strip()
            if next_line.startswith(TimeStep.START_TOKEN):
                break
            line = stream.readline()
            path = [node.strip() for node in line.split(',')]
            self.paths.append(tuple(path))
