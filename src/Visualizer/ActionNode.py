class ActionNode:
    def __init__(self, stream):
        #Format: Path,from,root,to,node;[min,max];visits;value
        line = stream.readline()
        tokens = [token.strip() for token in line.split(';')]
        path, minMax, visits, value = tokens
        nodes = [node.strip() for node in path.split(',')]
        self.id = ','.join(nodes)
        self.parent, self.label = ','.join(nodes[:-1]), nodes[-1]
        if not self.parent:
            self.parent = None

        minMax = minMax[1:-1] #Remove []s
        self.range = [int(val) for val in minMax.split(',')]
        if self.range[0] == self.range[1]:
            self.range = self.range[0] #Interval of size 1 == int
        self.visits = int(visits)
        self.value = float(value)
        self.selected = False

    def is_in(stream):
        line = stream.peek_line()
        try:
            path, line = line.split(';', 1)
        except ValueError:
            return False
        
        path = path.strip()
        nodes = [node.strip() for node in path.split(',')]
        return nodes[-1].startswith('a')

    def get_html(self):
        table = '<<table border="{}" cellborder="1" cellspacing="0">{}</table>>'
        row = '<tr>{}</tr>'
        col = '<td{}>{}</td>'
        rows = []
        label = "{}: {}".format(self.label, str(self.range))

        rows.append(row.format(col.format(' colspan="2"', label)))
        rows.append(row.format(col.format('', "Visits") + 
                          col.format('', "Value")))
        rows.append(row.format(col.format('', self.visits) + 
                          col.format('', self.value)))
        border = 2 if self.selected else 0
        return table.format(border, "".join(rows))

    def select(self):
        self.selected = True
