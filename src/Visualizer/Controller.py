from .CommandPrompt import CommandPrompt
from .Commands import *

class Controller(CommandPrompt):
    def __init__(self, model, view):
        super().__init__(view.get_prompt_string())
        self.executed = []
        self.model = model
        self.view = view
        self.start()

    def handle_error(self, error):
        self.view.display_error(error)

    def process_command(self, command):
        command.do(self)

    def push_command_stack(self, command):
        self.executed.append(command)

    def pop_command_stack(self):
        if len(self.executed) > 0:
            return self.executed.pop()
    
    def start(self):
        self.view.display_startup_message()
        super().start()
