from .TreeVisualizer import TreeVisualizer

class Model:
    def __init__(self):
        self.step = 0
        self.tree = TreeVisualizer("log.log")
        self.node = self.tree.steps[self.step].root

    def get_best_child(self, node):
        for child in node.children:
            if child.best:
                #Return first marked best
                return child
        if len(node.children) > 0:
            # None marked best, but one must be at least tied
            best_value = max((child.value for child in node.children))
            for child in node.children:
                if child.value == best_value:
                    #Return first tied for best
                    return child


    def get_child_index(self, child_id):
        #Finds index associated with child_id and returns it
        for index, child in enumerate(self.node.children):
            if child.id == child_id:
                return index

    def get_current_node(self):
        return self.node

    def get_current_node_id(self):
        #Returns current node unique identifier.
        return self.node.id

    def get_step(self):
        #Returns current timestep.
        return self.step

    def get_step_range(self):
        #Returns tuple which represents first and last valid indices of steps
        return self.tree.get_range()

    def set_child(self, child_index):
        #Sets the current node to be the child of current node at given index
        child_count = len(self.node.children)
        if 0 <= child_index < child_count:
            self.node = self.node.children[child_index]
        else:
            raise Exception("Invalid child index: {}".format(child_index))

    def set_parent(self):
        #Returns False if at root, selects parents and returns True otherwise
        has_parent = self.node.parent
        if has_parent:
            self.node = self.node.parent
        return has_parent

    def set_step(self, step_no):
        #Sets current step to be step_no.
        low, high = self.get_step_range()
        if low <= step_no <= high:
            self.step = step_no
        else:
            raise Exception("Invalid step no: {}".format(step_no))

