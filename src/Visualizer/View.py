class View:
    def __init__(self, model):
        self.model = model

    def display_current_node_info(self):
        #Toplevel format: Type, id, value, visits, child count.
        node = self.model.get_current_node()

        child = self.model.get_best_child(node)
        if not child:
             best_child = "N/A"
        else:
             index = self.model.get_child_index(child.id)
             best_child = str(index)
        child_count = len(node.children)
        
        lines = ["{} '{}'".format(node.type, node.id), 
                 "Val {} / Vis {}".format(node.value, node.visits), 
                 "{} Child{}, Best: {}".format(child_count, 
                                             "ren" if child_count != 1 else "",
                                               best_child)]

        for line in lines:
            print(line)

    def display_children(self):
        print("Children:")
        parent = self.model.get_current_node()
        child_count = len(parent.children)
        # If child # passes some threshold, print in pages of size threshold
        threshold = 20
        for index, child in enumerate(parent.children):
            msg = "{}: '{}', Value: {}, Visits: {}, Child count: {}"
            if child.best:
                msg = '[' + msg + ']'
            print(msg.format(index, child.id, child.value, child.visits,
                             len(child.children)))
            if index > 0 and index % threshold == 0:
                msg = "{} of {} printed. Continue? (Y/n) "
                response = input(msg.format(index, child_count))
                if response == 'n':
                    break
        

    def display_error(self, error):
        print("Error occurred: {}".format(str(error)))

    def display_help(self):
        print("==========HELP==========")
        print(" ? - prints current node info")
        print(" b - back, go back to previous node from which you came")
        print(" c - list children and associated info, highlighting best.")
        print("c# - go to child # (index)")
        print(" h - help (print this message)")
        print(" p - go to parent")
        print(" q - quit")
        print(" s - list valid range of timesteps")
        print("s# - set current timestep to #")

    def display_startup_message(self):
        print("Welcome to MCT_Tree Visualizer.")
        self.display_current_node_info()
        print("Type 'h' for help, or enter your first command.")
	
    def display_timestep_range(self):
        first, last = self.model.get_step_range()
        print("Valid timesteps are in range [{}, {}].".format(first, last))

    def get_prompt_string(self):
        return ">>> "
