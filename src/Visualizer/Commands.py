class BackCommand:
    def do(self, controller):
        command = controller.pop_command_stack()
        if command is None:
            raise Exception("Cannot go back further!")
        command.undo(controller)

class ChildCommand:
    def __init__(self, index):
        self.index = index

    def do(self, controller):
        model, view = controller.model, controller.view
        model.set_child(self.index)
        view.display_current_node_info()
        controller.push_command_stack(self)

    def undo(self, controller):
        model, view = controller.model, controller.view
        model.set_parent()
        view.display_current_node_info()

class HelpCommand:
    def do(self, controller):
        model, view = controller.model, controller.view
        view.display_help()

class InfoCommand:
    def do(self, controller):
        model, view = controller.model, controller.view
        view.display_current_node_info()

class ListCommand:
    def do(self, controller):
        model, view = controller.model, controller.view
        view.display_children()

class NoOpCommand:
    def do(self, controller):
        pass

class ParentCommand:
    def __init__(self):
        self.id = None

    def do(self, controller):
        model, view = controller.model, controller.view
        self.id = model.get_current_node_id()
        if model.set_parent():
            view.display_current_node_info()
            controller.push_command_stack(self)
        else:
            raise Exception("No parent exists!")

    def undo(self, controller):
        model, view = controller.model, controller.view
        index = model.get_child_index(self.id)
        model.set_child(index)
        view.display_current_node_info()

class QuitCommand:
    def do(self, controller):
        controller.stop()

class StepCommand:
    def __init__(self, step_no):
        self.step_no = step_no

    def do(self, controller):
        model, view = controller.model, controller.view
        old_step_no = model.get_step()
        model.set_step(self.step_no)
        view.display_current_node_info()
        self.step_no = old_step_no
        controller.push_command_stack(self)

    def undo(self, controller):
        model, view = controller.model, controller.view
        model.set_step(self.step_no)
        view.display_current_node_info()

class StepRangeCommand:
    def do(self, controller):
        model, view = controller.model, controller.view
        view.display_timestep_range()        

class CommandParser:
    SIMPLE_COMMANDS = { '': NoOpCommand,
                       '?': InfoCommand, 
                       'b': BackCommand, 
                       'c': ListCommand, 
                       'h': HelpCommand, 
                       'p': ParentCommand, 
                       'q': QuitCommand,
                       "quit": QuitCommand,
                       's': StepRangeCommand}

    def parse(raw_text):
        normalized = CommandParser.validate(raw_text)
        if normalized in CommandParser.SIMPLE_COMMANDS:
            return CommandParser.SIMPLE_COMMANDS[normalized]()
        else:
            cls, args = CommandParser.process_advanced(normalized)
            return cls(*args)
            
    def process_advanced(normalized_text):
        cls, arg = normalized_text[0], normalized_text[1:]
        classes = {'c': ChildCommand, 's': StepCommand}
        cls, args = classes[cls], [int(arg)]
        return cls, args

    def validate(raw_text):
        normalized = raw_text.strip()

        # See if normalized text matches pattern for a valid command
        good = False
        if normalized in CommandParser.SIMPLE_COMMANDS:
            good = True
        elif len(normalized) == 0:
            good = True
        elif normalized[0] == 'c' or normalized[0] == 's':
            try:
                int(normalized[1:])
                good = True
            except ValueError:
                pass

        # Return normalized text if valid, otherwise raise exception
        if good:
            return normalized
        else:
            raise Exception("Bad command: {}".format(raw_text))

