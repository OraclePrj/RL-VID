Installation instructions (written for Ubuntu 16.04):
1) Ensure git is installed to clone repository. 
    From a Terminal: sudo apt-get install git

2) Navigate to desired containing folder and clone the repository there:
    In the terminal: git clone git@gitlab.com:OraclePrj/RL-VID.git

    You may see a warning that reads:
        The authenticity of host 'gitlab.com (#.#.#.#)' can't be established.
        ECDSA key fingerprint is SHA256:(base-64 text).
        Are you sure you want to continue connecting (yes/no)?
    If so, you must type "yes" to continue.
    If you only hit enter, you will fail out with the following error:
        Host key verification failed.
        fatal: Could not read from remote repository.

        Please make sure you have the correct access rights
        and the repository exists.    
    
    You also must have set up a public SSH key for your device on Gitlab.
    If you haven't, you will get an error like this:
        Permission denied (publickey).
        fatal: Could not read from remote repository.

        Please make sure you have the correct access rights
        and the repository exists.

    Please see Gitlab's website for current instructions on how to do so.
        At time of writing, you could find instructions this way:
            Login to gitlab.com and go to User Settings.
            Select the SSH Keys entry on the left sidebar.
            Follow the instructions under "Add an SSH key".
        After adding your SSH key, try cloning again, it should work now.
            (This of course assumes you have access to the repository.)

    After completing the above cloning, you should have an RL-VID folder now.

    Before building the main program, some dependencies are needed.

    Dependency 1: LLVM. We need specifically LLVM 6.0. 
    Install using this command:
        sudo apt-get install llvm-6.0-dev

    Dependency 2: Boost. 
    Install using this command:
        sudo apt-get install libboost-all-dev
    
    These are the only dependencies needed for the main search executable.
    To get into the main folder, execute the following command:
        cd RL-VID/src
    In this folder, you'll need to type "make" to build the main program.
        make


Running the software:
    In RL-VID/src, type "make" to build the file "main".

    You can then run the program in several ways.
        ./main
            Good for a sanity check that the program is not frozen.
            (This often indicates the program isn't prompting correctly)
            Otherwise this can cause information overload.    
        ./main 2>err.log
            Intended single-run usage of program.
            Displays progress bar.
            Also notifies on first crash detection and gives elapsed time.
            If using QBASE, default parameters are used if using no flags.

        ./main 1>out.log 2>err.log (optional flags)
            This format is what analyze.py uses. No output will be displayed.
            analyze.py is not currently documented.
            This is used internally to test_parameter.sh.
            Optional flags are the same as used for test_parameter.
               The only difference is that --runs is not needed.
               (This handles one run.)

        Either of the above options produces a log file for visualization.
        ./test_parameter.sh (followed by mandatory flags)
            Helpful for running multiple trials without supervision.
            Used to perform 100 run checks in reported results in July.
            Requires the following flags:
              --best  <int>: 
                  QBASE keeps this many "best" actions it sees
              --batch <int>: 
                  For each update, QBASE chooses this many actions as options
              --id <int>:
                  A unique identifier that is used to save the results
              --runs <int>:
                  The number of independent times to run this parameter.
              --update <int>:
                  The period with which that QBASE considers the same batch
            Most of these options are ignored by UCB.
            (This approach can be used to test UCB for multiple runs as well)
            Note that reported July results used:
                --best 25 --batch 128 --id <arbitrary> --runs 100 --update 200
        The above option outputs a file named param<int>.data.
            You can quickly count the number of runs with a crash found:
                grep -v "None" param<int>.data | wc -l

    Always ensure that the program itself is:
        1) Has been instrumented with the provided AFL compiler.
        2) Is in the same directory as main
        3) Is named as specified in Config.cpp ("testcase" by default)
        4) Has its bitcode (LLVM IR) in the same directory as main.
        5) Its bc is also named as in Config.cpp ("testcase.bc" by default)
    The versions of testcase/testcase.bc are already set up to meet these.
    Note for convenience we provide two compiled testcases: 
        testcase_bind2[.bc]: the bind test case with only inputs 5 and 6
        testcase_bind4[.bc]: the bind test case with inputs 3-6
    The two input case is what we used in July and is already set up.
    To use a certain test case, either change Config.cpp to point to files, or
        cp testcase_bind[2/4] testcase
        cp testcase_bind[2/4].bc testcase.bc

Visualizing:
    visualize.py has a flag at the beginning that lets you choose between
    2 visualization styles: a graphical tree diagram and a text interface.
    The text interface is helpful for when trees get enormous.
    To visualize a given run, you should be sure it created a log.log file.

    For supporting Python code, you'll need pip3 to install one more.
    If necessary, install pip3 using this command:
        sudo apt-get install python3-pip
    To use the graphical visualizer, Graphviz will need to be installed. 
        To install, type the following two commands:
            For the underlying software: sudo apt-get install graphviz
            And for the Python hooks: pip3 install graphviz

    Then type the following command in RL-VID/src:
        python3 visualize.py
    Note that a "step" is one search tree generated during the run.
    "0" will be the first 1000 iterations, "1", the second 1000, etc.

    For large search trees, the default Ubuntu PDF viewer may not cope well.
    We've had more success opening these visualizer PDFs with FoxIt Reader.
    At the time of writing, this software could be downloaded at:
        https://www.foxitsoftware.com/pdf-reader/

Switching modes between UCB and QBASE:
    To switch between QBASE (currently selected) and UCB:
        From the RL-VID/src folder, edit "Config.cpp".
        At the top, change the action space arguments and policy mode.
            QBASE: DISCRETE/QBASE_MODE
            UCB: CONTINUOUS/UCB_MODE.
        Other pairings are not currently tested/supported.         
        actionCount (only used in UCB) changes are also unsupported.
        Note however that you can change the interval of the action space.
        Then type "make" from the RL-VID/src folder to apply changes.

To recompile test cases from source:
    Make sure since installation you have done the following extra steps:
    1) Ensure clang and llvm-3.8 are installed.
        sudo apt-get install clang
        sudo apt-get install llvm-3.8
    2) From RL-VID/afl directory, type:
        make afl-showmap
    3) Navigate to RL-VID/afl/llvm_mode and type:
        make
    Then navigate to the RL-VID/testcases/bind directory, typing "make" there.
    The files produced should be copied to the RL-VID/src directory to run.

DowserScores:
    Although the main program has the potential to use program
    instrumentation and some of the features from the Dowser paper to
    score basic blocks, the incorporation of this information is still under 
    development and was omitted from the UCB/QBASE test to prevent clouding 
    the data. (See line 72, Rewards::value() in Rewards.cpp.)

